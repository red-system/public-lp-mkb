module.exports = function(grunt) {

  var default_js = [
	  "assets/js/vendor/modernizr-3.6.0.min.js",
	  "assets/js/vendor/jquery-1.12.4.min.js",
	  "assets/js/bootstrap.min.js",
	  "assets/js/popper.min.js",
	  "assets/js/jquery.easing.min.js",
	  "assets/js/scrolling-nav.js",
	  "assets/js/slick.min.js",
	  "assets/js/select2.full.min.js",
	  "node_modules/sweetalert2/dist/sweetalert2.all.min.js",
	  "assets/js/jquery.magnific-popup.min.js",
      "node_modules/masonry-layout/dist/masonry.pkgd.min.js",
	  "assets/js/main.js",
  ];

  var default_css = [
	  "assets/css/bootstrap.min.css",
	  "assets/css/font-awesome.min.css",
	  "assets/css/magnific-popup.css",
	  "assets/css/slick.css",
	  "assets/css/default.css",
	  "assets/css/select2.min.css",
	  "assets/css/select2-bootstrap4.css",
	  "assets/css/style.css",
	  "assets/css/custom.css",
  ];

  var allFiles = default_js.concat(default_css); // merge js & css files directory

  grunt.initConfig({
    jsDistDir: 'assets/js/',
    cssDistDir: 'assets/css/',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: default_js,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: default_css,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
        }
      }
    },
    cssmin: {
      add_banner: {
        options: {
          banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
      'concat',
      'uglify',
      'cssmin'
  ]);

};
