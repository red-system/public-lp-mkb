$(document).ready(function () {

    var base_url = $('#base_url').html();
    var site_url = $('#site_url').html();

    'use strict';

    $('.select2').select2({
        theme: 'bootstrap4',
    });

    // $('.sitemap').masonry({
    //     itemSelector: '.sitemap-item',
    //     percentPosition: true,
    // });

	// $('.select2bs4').select2({
	// 	theme: 'bootstrap4'
	// });

    if ($('.accrodion-grp').length) {
        var accrodionGrp = $('.accrodion-grp');
        accrodionGrp.each(function () {
            var accrodionName = $(this).data('grp-name');
            var Self = $(this);
            var accordion = Self.find('.accrodion');
            Self.addClass(accrodionName);
            Self.find('.accrodion .accrodion-content').hide();
            Self.find('.accrodion.active').find('.accrodion-content').show();
            accordion.each(function () {
                $(this).find('.accrodion-title').on('click', function () {
                    if ($(this).parent().parent().hasClass('active') === false) {
                        $('.accrodion-grp.' + accrodionName).find('.accrodion').removeClass('active');
                        $('.accrodion-grp.' + accrodionName).find('.accrodion').find('.accrodion-content').slideUp();
                        $(this).parent().parent().addClass('active');
                        $(this).parent().parent().find('.accrodion-content').slideDown();
                    };


                });
            });
        });
    };


    $(window).on('scroll', function (event) {
        if ($(this).scrollTop() > 600) {
            $('.back-to-top').fadeIn(200)
        } else {
            $('.back-to-top').fadeOut(200)
        }
    });


    $('.back-to-top').on('click', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0,
        }, 1500);
    });

    $('#filter-agent-province').select2({
        ajax: {
            url: base_url + 'agen-find/provinces',
            type: 'post',
            dataType: 'json',
            async: true,
            data: function(params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response, params) {
                var data = $.map(response.data, function (obj) {
                    obj.id = obj.id || obj.province_id;
                    obj.text = obj.text || obj.province;

                    return obj;
                });

                params.page = params.page || 1;

                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            escapeMarkup: function(province) {
                return province;
            },
            templateResult: function (result) {
                if (result.loading) { return 'Loading...'; }
                return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        },
    });

    $('#pre-define-province').ready(function () {

        var $pre_define_province = $('#pre-define-province').val(),
            $pre_define_city = $('#pre-define-city').val();

        if (!isEmpty($pre_define_province)) {
            $.ajax({
                url: base_url + 'agen-find/search/provinces',
                type: 'post',
                dataType: 'json',
                data: {
                    province_name: special_slug($pre_define_province),
                },
            }).then(function (response) {
                var data = $.map(response.data, function (obj) {
                    obj.id = obj.id || obj.province_id;
                    obj.text = obj.text || obj.province;

                    return obj;
                });

                var option = new Option(response.data.province, response.data.province_id, true, true);

                $('#filter-agent-province').append(option).trigger('change');

                $('#filter-agent-province').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            })

            if (isEmpty($pre_define_city)) {
                $('#pre-define-province').val('');
            }
        }
    });

    $('.filter-agen').on('change', '#filter-agent-province', function (e) {
        
        var $province_val = $(this).val(),
            $predefine_subdistrict = $('#pre-define-subdistrict').val(),
            $predefine_city = $('#pre-define-city').val();

        if (!isEmpty($province_val)) {
            $('.filter-agen').find('#filter-agent-city').parent().removeClass('hide');
                loading_start();
            if (isEmpty($predefine_city)) {
                list_agent($province_val, null, null);
            }

            setTimeout(function() {
                loading_finish();
            }, 1000);
            $('#filter-agent-city').select2({
                ajax: {
                    url: base_url + 'agen-find/citys',
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    data: function (params) {
                        return {
                            province_id: $province_val,
                            search: params.term,
                        };
                    },
                    processResults: function (response, params) {
                        var data = $.map(response.data, function (obj) {
                            obj.id = obj.id || obj.city_id;
                            obj.text = obj.text || obj.city_name;

                            return obj;
                        });

                        params.page = params.page || 1;

                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    escapeMarkup: function (province) {
                        return province;
                    },
                    templateResult: function (result) {
                        if (result.loading) {
                            return 'Loading...';
                        }
                        return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
            });

            $('#filter-agent-city').val('').trigger('change');
            $('#filter-agent-area').parent().addClass('hide');

            if (!isEmpty($predefine_city) && !isEmpty($province_val)) {
                $.ajax({
                    url: base_url + 'agen-find/search/citys',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        city_name: special_slug($predefine_city),
                        province_id: $province_val
                    },
                }).then(function (response) {
                    var data = $.map(response.data, function (obj) {
                        obj.id = obj.id || obj.city_id;
                        obj.text = obj.text || obj.city_name;

                        return obj;
                    });

                    var option = new Option(response.data.city_name, response.data.city_id, true, true);

                    $('#filter-agent-city').append(option).trigger('change');

                    $('#filter-agent-city').trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                })

                if (isEmpty($predefine_subdistrict)) {
                    $('#pre-define-city').val('');
                    $('#pre-define-province').val('');
                }
            }
        }
        return false;
    });

    $('.filter-agen').on('change', '#filter-agent-city', function () {
        var $city_id = $(this).val(),
            $province_id = $('#filter-agent-province').val(),
            $predefine_subdistrict = $('#pre-define-subdistrict').val();

        if (!isEmpty($city_id)) {
            $('.filter-agen').find('#filter-agent-area').parent().removeClass('hide');
            loading_start()
            if (isEmpty($predefine_subdistrict)) {
                list_agent($province_id, $city_id, null);
            }

            setTimeout(function() {
                loading_finish();
            }, 1000);

            $('#filter-agent-area').select2({
                ajax: {
                    url: base_url + 'agen-find/subdistricts',
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    data: function(params) {
                        return {
                            province_id: $province_id,
                            city_id: $city_id,
                            search: params.term,
                        };
                    },
                    processResults: function (response, params) {
                        var data = $.map(response.data, function (obj) {
                            obj.id = obj.id || obj.subdistrict_id;
                            obj.text = obj.text || obj.subdistrict_name;

                            return obj;
                        });

                        params.page = params.page || 1;

                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    escapeMarkup: function(province) {
                        return province;
                    },
                    templateResult: function (result) {
                        if (result.loading) { return 'Loading...'; }
                        return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
            });

            $('#filter-agent-area').val('').trigger('change');

            if (!isEmpty($predefine_subdistrict) && !isEmpty($city_id) && !isEmpty($province_id)) {
                $.ajax({
                    url: base_url + 'agen-find/search/subdistricts',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        subdistrict_name: special_slug($predefine_subdistrict),
                        province_id: $province_id,
                        city_id: $city_id
                    },
                }).then(function (response) {
                    $.map(response.data, function () {
                            response.data.id = response.data.id || response.data.subdistrict_id;
                            response.data.text = response.data.text || response.data.subdistrict_name;

                        return response.data;
                    });

                    var option = new Option(response.data.subdistrict_name, response.data.subdistrict_id, true, true);

                    $('#filter-agent-area').append(option).trigger('change');

                    $('#filter-agent-area').trigger({
                        type: 'select2:select',
                        params: {
                            data: response.data
                        }
                    });
                })
                $('#pre-define-province').val('');
                $('#pre-define-city').val('');
                $('#pre-define-subdistrict').val('');
            }
        }
    });

    $('.filter-agen').on('change', '#filter-agent-area', function () {
        var val = $(this).val();
        var province_id = $('#filter-agent-province').val();
        var city_id = $('#filter-agent-city').val();
        loading_start();
        if (!isEmpty(val)) {
            
            setTimeout(function() {
                loading_finish();
            }, 1000);

            list_agent(province_id, city_id, val);
        }
    });

    $('.form-send').submit(function (e) {
        e.preventDefault();

        $('.preloader').hide().removeClass('hide').fadeIn('fast').removeClass('loaded');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (json) {
                $('.preloader').fadeOut('fast').addClass('hide').addClass('loaded');
                var data = JSON.parse(json);
                $('.form-group').removeClass('validated');
                $('.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                if (data.status === 'error') {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: data.message,
                        confirmButtonColor: '#0B6749'
                    });

                    $.each(data.errors, function (field, message) {
                        if (message) {
                            $('[name=' + field + ']').parents('.form-group').addClass('validated');
                            $('[name=' + field + ']').after('<div class="invalid-feedback">' + message + '</div>');
                            $('[name=' + field + ']').addClass('is-invalid');
                        }
                    });
                } else if (data.status == 'success') {
                    if (!isEmpty(data.no_swal) && data.no_swal === 'true' && !isEmpty(data.redirect)) {
                        window.location = site_url + data.redirect;
                    } else if(!isEmpty(data.no_swal) && data.no_swal === 'false' && isEmpty(data.redirect)) {
                        window.location.reload();
                    } else if (!isEmpty(data.reloadPage)) {
                        swal.fire({
                            title: data.title,
                            text: data.message,
                            icon: 'success',
                            confirmButtonColor: '#0B6749'
                        }).then(function () {
                            window.location.reload();
                        });
                    } else if (isEmpty(data.redirect)){
                        Swal.fire({
                            title: data.title,
                            text: data.message,
                            icon: data.status,
                            confirmButtonColor: '#0B6749'
                        });
                    } else {
                        Swal.fire({
                            title: data.title,
                            text: data.message,
                            icon: data.status,
                            confirmButtonColor: '#0B6749'
                        })
                        .then(function () {
                            window.location = site_url + data.redirect;
                        });
                    }
                } else {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Something Wrong!',
                        confirmButtonColor: '#0B6749'
                    });
                }
            }
        });

        return false;
    });

    function list_agent(province, city, subdistrict)
    {
        $('.faq-accordion').find('div[class*="clone-agent-"]').remove();
        $('#zero-data').addClass('hide');
        $('.preloader').hide().removeClass('hide').fadeIn('fast').removeClass('loaded');
        $.ajax ({
            url: base_url + 'agen-find/lists-agent',
            type: 'POST',
            dataType: 'json',
            data: {
                    province_id: province,
                    city_id: city,
                    subdistrict_id: subdistrict
            },
            success: function (response) {
                $('.preloader').fadeOut('fast').addClass('hide').addClass('loaded');
                var length = response.data.length;

                if (length !== 0) {
                    clone_template_agent(response.data);
                } else {
                    $('#zero-data').removeClass('hide');
                }
            }
        });
    }


    // SUPER RESELLER

    $('#filter-agent-province-super').select2({
        ajax: {
            url: base_url + 'agen-find/provinces',
            type: 'post',
            dataType: 'json',
            async: true,
            data: function(params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response, params) {
                var data = $.map(response.data, function (obj) {
                    obj.id = obj.id || obj.province_id;
                    obj.text = obj.text || obj.province;

                    return obj;
                });

                params.page = params.page || 1;

                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            escapeMarkup: function(province) {
                return province;
            },
            templateResult: function (result) {
                if (result.loading) { return 'Loading...'; }
                return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        },
    });

    $('#pre-define-province-super').ready(function () {

        var $pre_define_province = $('#pre-define-province-super').val(),
            $pre_define_city = $('#pre-define-city-super').val();

        if (!isEmpty($pre_define_province)) {
            $.ajax({
                url: base_url + 'agen-find/search/provinces',
                type: 'post',
                dataType: 'json',
                data: {
                    province_name: special_slug($pre_define_province),
                },
            }).then(function (response) {
                var data = $.map(response.data, function (obj) {
                    obj.id = obj.id || obj.province_id;
                    obj.text = obj.text || obj.province;

                    return obj;
                });

                var option = new Option(response.data.province, response.data.province_id, true, true);

                $('#filter-agent-province-super').append(option).trigger('change');

                $('#filter-agent-province-super').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            })

            if (isEmpty($pre_define_city)) {
                $('#pre-define-province-super').val('');
            }
        }
    });

    $('.filter-agen').on('change', '#filter-agent-province-super', function (e) {
        
        var $province_val = $(this).val(),
            $predefine_subdistrict = $('#pre-define-subdistrict-super').val(),
            $predefine_city = $('#pre-define-city-super').val();

        if (!isEmpty($province_val)) {
            $('.filter-agen').find('#filter-agent-city-super').parent().removeClass('hide');
                loading_start();
            if (isEmpty($predefine_city)) {
                list_super_agent($province_val, null, null);
            }

            setTimeout(function() {
                loading_finish();
            }, 1000);
            $('#filter-agent-city-super').select2({
                ajax: {
                    url: base_url + 'agen-find/citys',
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    data: function (params) {
                        return {
                            province_id: $province_val,
                            search: params.term,
                        };
                    },
                    processResults: function (response, params) {
                        var data = $.map(response.data, function (obj) {
                            obj.id = obj.id || obj.city_id;
                            obj.text = obj.text || obj.city_name;

                            return obj;
                        });

                        params.page = params.page || 1;

                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    escapeMarkup: function (province) {
                        return province;
                    },
                    templateResult: function (result) {
                        if (result.loading) {
                            return 'Loading...';
                        }
                        return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
            });

            $('#filter-agent-city-super').val('').trigger('change');
            $('#filter-agent-area-super').parent().addClass('hide');

            if (!isEmpty($predefine_city) && !isEmpty($province_val)) {
                $.ajax({
                    url: base_url + 'agen-find/search/citys',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        city_name: special_slug($predefine_city),
                        province_id: $province_val
                    },
                }).then(function (response) {
                    var data = $.map(response.data, function (obj) {
                        obj.id = obj.id || obj.city_id;
                        obj.text = obj.text || obj.city_name;

                        return obj;
                    });

                    var option = new Option(response.data.city_name, response.data.city_id, true, true);

                    $('#filter-agent-city-super').append(option).trigger('change');

                    $('#filter-agent-city-super').trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                })

                if (isEmpty($predefine_subdistrict)) {
                    $('#pre-define-city-super').val('');
                    $('#pre-define-province-super').val('');
                }
            }
        }
        return false;
    });

    $('.filter-agen').on('change', '#filter-agent-city-super', function () {
        var $city_id = $(this).val(),
            $province_id = $('#filter-agent-province-super').val(),
            $predefine_subdistrict = $('#pre-define-subdistrict-super').val();

        if (!isEmpty($city_id)) {
            $('.filter-agen').find('#filter-agent-area-super').parent().removeClass('hide');
            loading_start()
            if (isEmpty($predefine_subdistrict)) {
                list_super_agent($province_id, $city_id, null);
            }

            setTimeout(function() {
                loading_finish();
            }, 1000);

            $('#filter-agent-area-super').select2({
                ajax: {
                    url: base_url + 'agen-find/subdistricts',
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    data: function(params) {
                        return {
                            province_id: $province_id,
                            city_id: $city_id,
                            search: params.term,
                        };
                    },
                    processResults: function (response, params) {
                        var data = $.map(response.data, function (obj) {
                            obj.id = obj.id || obj.subdistrict_id;
                            obj.text = obj.text || obj.subdistrict_name;

                            return obj;
                        });

                        params.page = params.page || 1;

                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    escapeMarkup: function(province) {
                        return province;
                    },
                    templateResult: function (result) {
                        if (result.loading) { return 'Loading...'; }
                        return result.text + '<h6>' + result.make + ' ' + result.category + '</h6>';
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
            });

            $('#filter-agent-area-super').val('').trigger('change');

            if (!isEmpty($predefine_subdistrict) && !isEmpty($city_id) && !isEmpty($province_id)) {
                $.ajax({
                    url: base_url + 'agen-find/search/subdistricts',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        subdistrict_name: special_slug($predefine_subdistrict),
                        province_id: $province_id,
                        city_id: $city_id
                    },
                }).then(function (response) {
                    $.map(response.data, function () {
                            response.data.id = response.data.id || response.data.subdistrict_id;
                            response.data.text = response.data.text || response.data.subdistrict_name;

                        return response.data;
                    });

                    var option = new Option(response.data.subdistrict_name, response.data.subdistrict_id, true, true);

                    $('#filter-agent-area-super').append(option).trigger('change');

                    $('#filter-agent-area-super').trigger({
                        type: 'select2:select',
                        params: {
                            data: response.data
                        }
                    });
                })
                $('#pre-define-province-super').val('');
                $('#pre-define-city-super').val('');
                $('#pre-define-subdistrict-super').val('');
            }
        }
    });

    $('.filter-agen').on('change', '#filter-agent-area-super', function () {
        var val = $(this).val();
        var province_id = $('#filter-agent-province-super').val();
        var city_id = $('#filter-agent-city-super').val();
        loading_start();
        if (!isEmpty(val)) {
            
            setTimeout(function() {
                loading_finish();
            }, 1000);

            list_super_agent(province_id, city_id, val);
        }
    });

    function list_super_agent(province, city, subdistrict)
    {
        $('.faq-accordion').find('div[class*="clone-agent-"]').remove();
        $('#zero-data').addClass('hide');
        $('#pre-filter').addClass('hide');
        $('.preloader').hide().removeClass('hide').fadeIn('fast').removeClass('loaded');
        $.ajax ({
            url: base_url + 'agen-find/lists-super-agent',
            type: 'POST',
            dataType: 'json',
            data: {
                    province_id: province,
                    city_id: city,
                    subdistrict_id: subdistrict
            },
            success: function (response) {
                $('.preloader').fadeOut('fast').addClass('hide').addClass('loaded');
                var length = response.data.length;

                if (length !== 0) {
                    clone_template_agent(response.data);
                } else {
                    $('#zero-data').removeClass('hide');
                }
            }
        });
    }


    // =====

    function clone_template_agent(data) {
        $.each(data, function (index, value) {
            $('#template-agent-nonactive').clone().appendTo('.faq-accordion').removeAttr('id').removeClass('hide').addClass('clone-agent-' + index).children('.accrodion').removeClass('active');

            if (!isEmpty(value.nama)) {
                $('.clone-agent-' + index).find('.agent-name').html(value.nama + ' (' + value.city_name + ', ' + value.subdistrict_name + ')');
                var names = value.nama.split(' '),
                    initials = names[0].substring(0, 1).toUpperCase();
                
                if (names.length > 1) {
                    initials += names[1].substring(0, 1).toUpperCase();
                }

                $('.clone-agent-' + index).find('.agent-ava').html(initials);

            } else {
                $('.clone-agent-' + index).find('.agent-name').addClass('hide');
                $('.clone-agent-' + index).find('.agent-ava').addClass('hide');
            }

            if (!isEmpty(value.outlet_alamat)) {
                $('.clone-agent-' + index).find('.agent-address').html(value.outlet_alamat);
            } else {
                $('.clone-agent-' + index).find('.agent-address').addClass('hide');
            }

            if (!isEmpty(value.maps)) {
                $('.clone-agent-' + index).find('.agent-maps').attr('href', value.maps);
            } else {
                $('.clone-agent-' + index).find('.agent-maps').addClass('hide');
            }

            if (!isEmpty(value.phone)) {
                $('.clone-agent-' + index).find('.agent-phone').attr('href', 'tel:' + trim_first_char(value.phone)).html(value.phone);
                $('.clone-agent-' + index).find('.agent-whatsapp').attr('href', 'https://wa.me/:'+trim_first_char(value.phone)).html(value.phone);
                $('.clone-agent-' + index).find('.btn-wa').attr('href', 'https://api.whatsapp.com/send?phone='+trim_first_char(value.phone)+'&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB').attr('data-res', value.nama);
                $('.clone-agent-' + index).find('.btn-super-res').attr('href', 'https://api.whatsapp.com/send?phone='+trim_first_char(value.phone)+'&text=Hai,%20Saya%20ingin%20bergabung%20jadi%20Reseller%20MKB').html('Gabung Sekarang');
            } else {
                $('.clone-agent-' + index).find('.agent-phone').addClass('hide');
                $('.clone-agent-' + index).find('.agent-whatsapp').addClass('hide');
                $('.clone-agent-' + index).find('.btn-wa').addClass('hide');
                $('.clone-agent-' + index).find('.btn-super-res').addClass('hide');
            }

            if (!isEmpty(value.shopee)) {
                $('.clone-agent-' + index).find('.btn-shopee').attr('href', value.shopee);
            } else {
                $('.clone-agent-' + index).find('.btn-shopee').addClass('hide');
            }

            if (!isEmpty(value.tokopedia)) {
                $('.clone-agent-' + index).find('.btn-tokped').attr('href', value.tokopedia);
            } else {
                $('.clone-agent-' + index).find('.btn-tokped').addClass('hide');
            }

            if (!isEmpty(value.email)) {
                $('.clone-agent-' + index).find('.agent-email').attr('href', 'mailto:' + value.email).html(value.email);
            } else {
                $('.clone-agent-' + index).find('.agent-email').addClass('hide');
            }

            if (!isEmpty(value.tokopedia)) {
                $('.clone-agent-' + index).find('.agent-tokopedia').attr('href', value.tokopedia);
            } else {
                $('.clone-agent-' + index).find('.agent-tokopedia').addClass('hide');
            }

            if (!isEmpty(value.shopee)) {
                $('.clone-agent-' + index).find('.agent-shopee').attr('href', value.shopee);
            } else {
                $('.clone-agent-' + index).find('.agent-shopee').addClass('hide');
            }

            // $('.clone-agent-' + index).find('.agent-detail').attr('href', base_url +  'agen/' + slug(value.province) + '/' + slug(value.city_name) + '/' + slug(value.subdistrict_name) + '/' + slug(value.username));
        });
        
        recheck_accrodion();
    }

});

function slug(str) {
    return str
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

function remove_slug(str) {
    str = str.replace(/-/g, ' ');
    return str
}

function special_slug(str) {
    str = str.replace(/-/g, '+');
    return str
}

function trim_first_char(value) {
    var trimmed  = '';
    if (value.substr(0,2) === '08') {
        trimmed = '62'+value.substring(1);
    } else if (value.substr(0,2) === '62') {
        trimmed = ''+value;
    } else {
        trimmed = value;
    }
    return trimmed;
}

function recheck_accrodion() {
    if ($('.accrodion-grp').length) {
        var accrodionGrp = $('.accrodion-grp');
        accrodionGrp.each(function () {
            var accrodionName = $(this).data('grp-name');
            var Self = $(this);
            var accordion = Self.find('.accrodion');
            Self.addClass(accrodionName);
            Self.find('.accrodion .accrodion-content').hide();
            Self.find('.accrodion.active').find('.accrodion-content').show();
            accordion.each(function () {
                $(this).find('.accrodion-title').on('click', function () {
                    if ($(this).parent().parent().hasClass('active') === false) {
                        $('.accrodion-grp.' + accrodionName).find('.accrodion').removeClass('active');
                        $('.accrodion-grp.' + accrodionName).find('.accrodion').find('.accrodion-content').slideUp();
                        $(this).parent().parent().addClass('active');
                        $(this).parent().parent().find('.accrodion-content').slideDown();
                    }
                });
            });
        });
    }
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
        {
            return false;
        }
    }
    return true;
}

$('#contact-form').submit(function(e) {
    e.preventDefault();
    loading_start();
    $.post($('#contact-form').attr('action'), $(this).serialize(), function(data){
        loading_finish();
        if(data.status === 'error') {
            Object.keys(data.errors).forEach(function(key) {
                $('#' + key + '-form .invalid-feedback').html(data.errors[key]);
            });
        } else {
            Swal.fire({
                icon: 'success',
                title : 'Sukses !',
                text: 'Pesan berhasil terkirim',
                });
            $('input[type="text"]').val('');
            $('input[type="tel"]').val('');
            $('input[type="email"]').val('');
            $('textarea').val('');
            $('.invalid-feedback').html('');
        }
    });
    return false;
});

function loading_start() {
    $('.container-loading').removeClass('d-none').fadeIn('fast');
}

function loading_finish() {
    $('.container-loading').fadeOut('fast').addClass('d-none');
}