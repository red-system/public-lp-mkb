$(document).ready(function () {
    var base_url = $("#base_url").val();
    $("#inputForProvince").on("change",function () {
        var value = $(this).val();
        loading_start();
        if(value!=0){
            $.ajax({
                url:base_url+'form-bisnis/kabupaten',
                data:{'id':value},
                type:"post",
                success:function (response) {
                    var data = JSON.parse(response)
                    $("#inputForKabupaten").html('<option >Pilih Kabupaten/Kota</option>');
                    $.each(data,function (index,value) {
                        $("#inputForKabupaten").append('<option value="'+value.city_id+'">'+value.city_name+'</option>')
                    })

                    loading_finish()
                },
                error:function () {
                    loading_finish()
                }
            })
        }
    })
    $("#inputForKabupaten").on("change",function () {
        var value = $(this).val();
        loading_start();
        if(value!=0){
            $.ajax({
                url:base_url+'form-bisnis/kecamatan',
                data:{'id':value},
                type:"post",
                success:function (response) {
                    var data = JSON.parse(response)
                    $("#inputForKecamatan").html('<option >Pilih Kecamatan</option>');
                    $.each(data,function (index,value) {
                        $("#inputForKecamatan").append('<option value="'+value.subdistrict_id+'">'+value.subdistrict_name+'</option>')
                    })

                    loading_finish()
                },
                error:function () {
                    loading_finish()
                }
            })
        }
    })
    $("#btn-submit").on("click",function (e) {
        e.preventDefault();
        var form = $(this).closest("form");
        form.valid();
        if (!form.valid()) {
            return;
        }
        $(".error").remove();
        loading_start()
        $("label .error").remove();
        form.ajaxSubmit({
            url:form.attr("action"),

            success:function (respone) {
                var data = JSON.parse(respone)
                loading_finish()
                if(data.success){
                    swal.fire({
                        type: 'success',
                        title:'Data Tersimpan',
                        text:data.message,
                    }).then(function () {
                        window.location.reload()
                    })
                }else{
                    $.each(data.errors,function (index,value) {
                        if(value!='') {
                            $('[name=' + index + ']').parent().append('<label id="error-' + index + '" class="error">' + value + '</label>')
                        }
                    })
                }
            },
            error:function () {
                loading_finish()
            }
        })
    })
    function loading_start() {
        $('.container-loading').removeClass('d-none').fadeIn('fast');
    }

    function loading_finish() {
        $('.container-loading').fadeOut('fast').addClass('d-none');
    }
})