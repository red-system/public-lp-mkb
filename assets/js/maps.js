'use strict';

$(function () {
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});

$('#province').on('change', function () {
    var province_id = $(this).val();
    loading_start();
    $.ajax({
        type: "POST",
        url: base_url + "mapping/province",
        data: {
            province_id: province_id,
        },
        success: function (result) {

            setTimeout(function() {
                loading_finish();
            }, 1000);

            var hasil = JSON.parse(result);
            console.log(hasil);

            $('#lat_val').val(hasil.latlong.latitude);
            $('#long_val').val(hasil.latlong.longitude);
            $('#zoom_val').val(hasil.latlong.zoom);

            $("#city").html('');
            $("#city").html(hasil.data);

            $("#subdistrict").html('<option value="">Pilih Kecamatan</option>');
            initMap();
        }
    });
});

function getProvinceLatLong() {
    var province_id = $("#province").val();
    loading_start();
    $.ajax({
        type: "POST",
        url: base_url + "mapping/province",
        data: {
            province_id: province_id,
        },
        success: function (result) {

            if (!isEmpty(result)) {
            
                setTimeout(function() {
                    loading_finish();
                }, 1000);
    
                var hasil = JSON.parse(result);
                console.log(hasil);

                $('#lat_val').val(hasil.latlong.latitude);
                $('#long_val').val(hasil.latlong.longitude);
                $('#zoom_val').val(hasil.latlong.zoom);

                $("#city").html('');
                $("#city").html(hasil.data);

                $("#subdistrict").html('<option value="">Pilih Kecamatan</option>');
                initMap();
            }
        }
    });
}

$('#city').on('change', function () {
    var city_id = $(this).val();
    loading_start();
    $.ajax({
        type: "POST",
        url: base_url + "mapping/city",
        data: {
            city_id: city_id,
        },
        success: function (result) {

            setTimeout(function() {
                loading_finish();
            }, 1000);

            var hasil = JSON.parse(result);
            console.log(hasil);

            $('#lat_val').val(hasil.latlong.latitude);
            $('#long_val').val(hasil.latlong.longitude);
            $('#zoom_val').val(hasil.latlong.zoom);

            $("#subdistrict").html('');
            $("#subdistrict").html(hasil.data);
            initMap();
        }
    });
});

function getCityLatLong() {
    var city_id = $("#city").val();

    $.ajax({
        type: "POST",
        url: base_url + "mapping/city",
        data: {
            city_id: city_id,
        },
        success: function (result) {
            var hasil = JSON.parse(result);


            console.log(hasil);

            $('#lat_val').val(hasil.latlong.latitude);
            $('#long_val').val(hasil.latlong.longitude);
            $('#zoom_val').val(hasil.latlong.zoom);

            $("#subdistrict").html('');
            $("#subdistrict").html(hasil.data);

            initMap();
        }
    });
}

$('#subdistrict').on('change', function () {
    var subdistrict_id = $(this).val();
    loading_start();
    $.ajax({
        type: "POST",
        url: base_url + "mapping/subdistrict",
        data: {
            subdistrict_id: subdistrict_id,
        },
        success: function (result) {

            setTimeout(function() {
                loading_finish();
            }, 1000);

            var hasil = JSON.parse(result);
            console.log(hasil);

            $('#lat_val').val(hasil.latlong.latitude);
            $('#long_val').val(hasil.latlong.longitude);
            $('#zoom_val').val(hasil.latlong.zoom);
            
            initMap();
        }
    });
});

function getSubdistrictLatLong() {
    var subdistrict_id = $("#subdistrict").val();

    $.ajax({
        type: "POST",
        url: base_url + "mapping/subdistrict",
        data: {
            subdistrict_id: subdistrict_id,
        },
        success: function (result) {
            var hasil = JSON.parse(result);


            console.log(hasil);

            $('#lat_val').val(hasil.latlong.latitude);
            $('#long_val').val(hasil.latlong.longitude);
            $('#zoom_val').val(hasil.latlong.zoom);

            initMap();
        }
    });
}

function loading_start() {
    $('.container-loading').removeClass('d-none').fadeIn('fast');
}

function loading_finish() {
    $('.container-loading').fadeOut('fast').addClass('d-none');
}