<?php

class M_gallery extends CI_Model
{

	protected $table = 'gallery';

	public function get_data()
	{
        $this->db->where('type','gallery');
		return $this->db->get($this->table);
	}
    public function get_data_front($id=null)
    {
        $this->db->where('type','gallery');
        if($id!=null){
            $this->db->where('id <',$id);
        }
        $this->db->order_by('id','desc');
        return $this->db->get($this->table)->result();
    }
	public function input_data($data)
	{
		$this->db->insert($this->table, $data);
	}

	public function row_data($where)
	{
		return $this->db->where($where)->get($this->table)->row();
	}

	public function delete_data($where)
	{
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	function update_data($where, $data)
	{
		$this->db->where($where);
		$this->db->update($this->table, $data);
	}
}
