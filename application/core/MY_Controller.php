<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    var $css = array();
    var $js = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
        $this->load->helper('string');

    }
    public function index()
    {


    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
