<!-- introBlock -->
<section class="introBlock position-relative">
	<div class="slick-fade">
		<div>
			<div class="align w-100 d-flex align-items-center bgCover">
				<!-- holder -->
				<div class="container position-relative holder pt-xl-10 pt-15">
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
							<div class="txtwrap pr-lg-10">
								<h1 class="fwEbold position-relative pb-lg-8 pb-4 mb-xl-7 mb-lg-6">
									<?php echo $home_sesi_1->title_menu ?><span
										class="text-break d-block"><?php echo $home_sesi_1->title ?></span></h1>
								<p class="mb-xl-15 mb-lg-10">Lorem ipsum is simply dummy text of the printing and
									typesetting industry <br>has been the industry's standard.</p>

								<a href="<?php echo site_url('agen');?>"
									title="<?= $dict_btn_to_agen ? $dict_btn_to_agen : 'tombol list reseller' ?>"
									class="btn btnTheme btnShop fwEbold text-white md-round py-md-3 px-md-4 py-1 px-1">
									<?php echo $dict_btn_to_agen ?>
									<i class="fas fa-arrow-right ml-2"></i>
								</a>

								<a href="http://app.support88.id/register" target="_blank"
									title="<?= $dict_btn_register_agen ? $dict_btn_register_agen : 'tombol register agen' ?>"
									class="btn btnTheme fwEbold text-white md-round py-md-3 px-md-4 py-sm-1 px-sm-1 py-1 px-1"
									style="background-color:#fbc02d">
									<?php echo $dict_btn_register_agen ?>
									<i class="fas fa-arrow-right ml-2"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="imgHolder">
						<img src="<?php echo base_url();?>assets/images/1.jpg" alt="image description"
							class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="discoverSecHolder container pt-xl-19 pb-xl-12 pt-lg-20 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-5">
	<div class="row rightAlign">
		<div class="col-12 col-lg-6 mb-lg-0 mb-4">
			<div class="imgHolder position-relative">
				<img src="<?php echo base_url();?>upload/images/<?php echo $home_sesi_2->thumbnail ?>"
					alt="image description" class="img-fluid w-100">
				<img src="<?php echo base_url();?>assets/images/b-bdr.png" alt="image description"
					class="img-fluid bdr position-absolute">
			</div>
		</div>
		<div class="col-12 col-lg-6 pt-xl-14 pt-lg-5">
			<span class="title position-relative d-block pl-7 mb-4">Sejarah</span>
			<h2 class="fwEbold headingIV playfair mb-6"><?php echo $home_sesi_2->title ?></h2>
			<p class="mb-lg-10 mb-md-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
				incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
		</div>
	</div>
</section>

<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-12 pb-xl-17 pt-lg-10 pb-lg-4 pt-md-8 pb-md-4 pt-5">
	<div class="row">
		<!-- mainHeader -->
		<header class="col-12 mainHeader mb-4 text-center">
			<h1 class="headingIV playfair fwEblod mb-6"><?php echo $home_sesi_3->title ?></h1>
			<span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
					alt="Header Border" class="img-fluid img-bdr"></span>
			<p>There are many variations of passages of lorem ipsum available </p>
		</header>
	</div>
	<div class="row">
		<?php foreach ($list_event as $event) { 
			$permalink = $this->main->permalink(array('event', $event->slug));	
		?>
		<div class="col-12 col-sm-6 col-lg-4">
			<div class="newsPostColumn text-center px-2 pb-6 mb-6">
				<div class="imgHolder position-relative mb-6">
					<a href="<?= $permalink ?>">
						<img src="<?php echo $this->main->image_preview_url($event->thumbnail) ?>"
							alt="<?php echo $event->thumbnail_alt ? $event->thumbnail_alt : $event->title; ?>"
							title="<?= $event->thumbnail_alt ? $event->thumbnail_alt : $event->title ?>"
							class="img-fluid">
						<time class="time text-uppercase position-absolute py-2 px-0" datetime="2019-02-03 20:00">
							<span class="fwEbold d-block "><?php echo date('d M Y', $blog->created_at) ?></span>
						</time>
					</a>
				</div>
				<h2 class="headingV fwEbold mb-2"><a href="<?php echo $permalink ?>"
						title="<?= $event->title ? $event->title : 'judul event' ?>"><?php echo $event->title; ?></a>
				</h2>
				<p class="mb-0">
					<?php echo $this->main->short_desc($event->description); ?>
				</p>
			</div>
		</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="d-flex justify-content-center">
				<a href="<?php echo site_url('event');?>" title="link event"
					class="btn btnTheme btnShop fwEbold text-white md-round py-md-3 px-md-4 py-1 px-1">
					Semua Event <i class="fas fa-arrow-right ml-2"></i>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="introSec bg-lightGray pt-xl-12 pb-xl-7 pt-10 pb-10">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 mb-lg-0 mb-6">
				<img src="<?php echo base_url();?>assets/images/img81.png" alt="image description" class="img-fluid">
			</div>
			<div class="col-12 col-lg-6">
				<h2 class="headingII fwEbold playfair position-relative mb-6 pb-5"><?php echo $home_sesi_4->title; ?>
				</h2>

				<div id="accordion" class="accordionList pt-lg-0">

					<?php $data_1 = json_decode($faq->data_1, TRUE); ?>
					<?php foreach($data_1['title'] as $key => $faq_title) { ?>
					<div class="card mb-2">
						<div class="card-header px-xl-5 py-xl-3" id="heading<?= $key ?>">
							<h5 class="mb-0">
								<button class="btn btn-link fwEbold text-uppercase text-left w-100 p-0"
									data-toggle="collapse" data-target="#collapse<?= $key ?>" aria-expanded="true"
									aria-controls="collapse">
									<?php echo $faq_title; ?> <i class="fas fa-sort-down float-right"></i>
								</button>
							</h5>
						</div>
						<div id="collapse<?= $key ?>" class="collapse" aria-labelledby="heading<?= $key ?>"
							data-parent="#accordion">
							<div class="card-body px-xl-5 py-0">
								<p class="mb-7"><?php echo $data_1['description'][$key] ?></p>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- chooseUs-sec -->
<section class="chooseUs-sec container pt-xl-22 pt-lg-20 pt-md-16 pt-10 pb-xl-12 pb-md-7 pb-2">
	<div class="row">
		<div class="col-12 col-lg-6 mb-lg-0 mb-4">
			<img src="<?php echo base_url();?>assets/images/2.jpg" alt="image description" class="img-fluid">
		</div>
		<div class="col-12 col-lg-6 pr-4">
			<h2 class="headingII fwEbold playfair position-relative mb-6 pb-5" style="line-height:1.3">
				<?php echo $home_sesi_5->title; ?></h2>
			<ul class="list-unstyled chooseList container">
				<?php $data_1_service = json_decode($home_sesi_5->data_1, TRUE); ?>
				<?php foreach($data_1_service['title'] as $key => $service_title ) { ?>
				<li class="d-flex justify-content-start mb-xl-7 mb-lg-5 mb-3">
					<!-- <span class="icon icon-plant"></span> -->
					<div class="alignLeft d-flex justify-content-start flex-wrap">
						<h3 class="headingIII fwEbold mb-2"><i
								class="fas fa-<?php echo $data_1_service['icons'][$key]; ?>"></i>
							<?php echo $service_title ?></h3>
						<!-- <p><?php echo $data_1_service['description'][$key] ?></p> -->
					</div>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</section>

<section class="teamSec pt-xl-12 pb-xl-21 pt-lg-10 pb-lg-20 pt-md-8 pb-md-16 pt-0 pb-4">
	<div class="container">
		<div class="row">
			<header class="col-12 mainHeader mb-9 text-center">
				<h1 class="headingIV playfair fwEblod mb-4"><?php echo $home_sesi_6->title; ?></h1>
				<span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
						alt="Header Border" class="img-fluid img-bdr"></span>
			</header>
		</div>
		<div class="row">
			<?php foreach($testimonial as $testi) { ?>
			<div class="col-12 col-sm-6 col-lg-4 mb-lg-0 mb-6">
				<article class="teamBlock overflow-hidden">
					<span class="imgWrap position-relative d-block w-100 mb-4">
						<img class="img-fluid" src="<?php echo base_url();?>assets/images/img83.jpg"
							alt="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>"
							title="<?= $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>">
					</span>
					<div class="textDetail w-100 text-center">
						<h3>
							<strong class="text-uppercase d-block fwEbold name mb-2"><a
									href="javascript:void(0);"><?php echo $testi->title; ?></a></strong>
							<strong class="text-capitalize d-block desination"><?php echo $testi->title_sub; ?></strong>
						</h3>

						<p class="mb-xl-14 mb-lg-10">“<?php echo $testi->description ?>”</p>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-0 pb-xl-0 pt-lg-0 pb-lg-4 pt-md-0 pb-md-2 pt-0">
	<div class="row">
		<!-- mainHeader -->
		<header class="col-12 mainHeader mb-4 text-center">
			<h1 class="headingIV playfair fwEblod mb-4"><?php echo $home_sesi_8->title; ?></h1>
			<span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
					alt="Header Border" class="img-fluid img-bdr"></span>
			<p>There are many variations of passages of lorem ipsum available </p>
		</header>
	</div>
	<div class="row">
		<?php foreach ($list_blog as $blog) { 
			$permalink = $this->main->permalink(array('blog and news', $blog->category_blog, $blog->title));
		?>
		<div class="col-12 col-sm-6 col-lg-4">
			<div class="newsPostColumn text-center px-2 pb-6 mb-6">
				<div class="imgHolder position-relative mb-6">
					<a href="<?php echo $permalink ?>" title="<?= $blog->title ? $blog->title : 'judul blog' ?>">
						<img src="<?php echo $this->main->image_preview_url($blog->thumbnail) ?>"
							alt="<?php echo $blog->thumbnail_alt ? $blog->thumbnail_alt : $blog->title ?>"
							title="<?= $blog->thumbnail_alt ? $blog->thumbnail_alt : $blog->title ?>"
							class="img-fluid w-100">
						<time class="time text-uppercase position-absolute py-2 px-0" datetime="2019-02-03 20:00">
							<span class="fwEbold d-block"><?php echo date('d M Y', $blog->created_at) ?></span></time>
					</a>
				</div>
				<h2 class="headingV fwEbold mb-2"><a href="<?php echo $permalink ?>"
						title="<?= $blog->title ? $blog->title : 'judul blog' ?>"><?php echo $blog->title ?></a></h2>
				<p class="mb-0">
					<?php echo $this->main->short_desc($blog->description); ?>
				</p>
			</div>
		</div>
		<?php } ?>

	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="d-flex justify-content-center">
				<a href="<?php echo site_url('blog-and-news');?>" title="link event"
					class="btn btnTheme btnShop fwEbold text-white md-round py-md-3 px-md-4 py-1 px-1">
					Semua Blog & News <i class="fas fa-arrow-right ml-2"></i>
				</a>
			</div>
		</div>
	</div>
</section>