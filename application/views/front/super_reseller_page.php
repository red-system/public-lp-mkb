<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MKB Health and Beauty | Reseller</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.r.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/select2/css/select2.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/loading.css">
</head>

<body>
    <div id="base_url" class="hide"><?php echo base_url(); ?></div>
    <div id="site_url" class="hide"><?php echo site_url(); ?></div>
    <div class="container p-0">
        <div class="row m-0 justify-content-center">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 bg-white p-0">
                <div class="banner pt-5">
                    <div class="px-4">
                        <div class="row pt-5">
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-6">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <a href="<?php echo base_url() ?>gabung" class="banner-text2"><i
                                                        class="fa fa-angle-left"></i>
                                                    Kembali</a>
                                            </div>
                                        </div>
                                        <div class="banner-text pt-5">
                                            Join MKB bersama <br>Super Reseller Terdekat.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-6">
                                <img class="w-100 banner-produk"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new-crop.png") ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- section search -->
                <!-- <div class="row py-5 px-4">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                        <div class="input-icon-wrap">
                            <span class="input-icon"><span class="fa fa-search"></span></span>
                            <input type="text" class="input-with-icon" id="form-name" placeholder="Cari Reseller">
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <button type="submit" class="btn-search"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div> -->

                <div class="row pt-5 px-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="box-filter pt-3 pl-3 pr-3 pb-1">
                            <span class="text-filter">
                                Cari Super Reseller
                            </span>
                            <form>
                                <div class="row filter-agen">
                                    <input type="text" class="hide" id="pre-define-province-super"
                                        value="<?= $province_name ? $province_name : null; ?>">
                                    <input type="text" class="hide" id="pre-define-city-super"
                                        value="<?= $city_name ? $city_name : null; ?>">
                                    <input type="text" class="hide" id="pre-define-subdistrict-super"
                                        value="<?= $subdistrict_name ? $subdistrict_name : null; ?>">

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group pt-3">
                                            <!-- <label>Provinsi</label> -->
                                            <select name="filter-agent-province" id="filter-agent-province-super"
                                                class="filter-input select2" data-placeholder="Pilih Provinsi"></select>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group hide">
                                            <!-- <label>Kabupaten</label> -->
                                            <select name="filter-agent-city" id="filter-agent-city-super"
                                                class="form-control select2"
                                                data-placeholder="Pilih Kabupaten"></select>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group hide pb-0">
                                            <!-- <label>Kecamatan</label> -->
                                            <select name="filter-agent-area" id="filter-agent-area-super"
                                                class="form-control select2"
                                                data-placeholder="Pilih Kecamatan"></select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row px-4 pt-5">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="faq-accordion agent-accordion">
                            <div id="zero-data" class="hide py-5">
                                <h5 class="text-center">Maaf Reseller tidak ditemukan, silahkan coba kembali.</h5>
                            </div>

                            <div id="pre-filter">
                            <?php foreach($resellers as $reseller){ ?>
                            
                            <div class="accrodion-grp" id="template-agent-nonactive" data-grp-name="faq-accrodion">
                                <div class="accrodion card mb-2">
                                    <div class="accrodion-inner card-header p-0 m-0">
                                        <div class="accrodion-title p-3 d-flex align-items-center"
                                            style="cursor:pointer;">
                                            <div
                                                class="circle-ava d-flex align-items-center justify-content-center mr-3 agent-ava">
                                                <?php
                                                        $names = explode(' ', $reseller->nama);
                                                        $initials = strtoupper(substr($names[0], 0, 1));
                                                        if (count($names) > 1) {
                                                            $initials = $initials."".strtoupper(substr($names[1], 0, 1));
                                                        }
                                                        echo $initials;
                                                    
                                                ?>
                                            </div>
                                            <span class="name-res agent-name"><?= $reseller->nama ?> (<?= $reseller->city_name.", ". $reseller->subdistrict_name ?>)</span>
                                            <i class="fa fa-angle-double-down d-inline float-right p-2 icon-drop"></i>
                                        </div>
                                        <div class="accrodion-content bg-white ml-0">
                                            <div class="card-body p-2">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="row py-2">
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="desc-res">
                                                                <i class="fa fa-map"></i> Alamat Outlet<br>
                                                                <p class="agent-address d-inline">
                                                                    <?= $reseller->outlet_alamat ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row py-2">
                                                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                                                            <div class="desc-res">
                                                                <i class="fa fa-whatsapp"></i> Whatsapp<br>
                                                                <p class="agent-whatsapp d-inline">
                                                                    <?= $reseller->phone ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-7">
                                                            <div class="desc-res">
                                                                <i class="fa fa-envelope"></i> Email<br>
                                                                <p class="agent-email d-inline">
                                                                    <?= $reseller->email ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row my-2 px-2">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <a href="https://api.whatsapp.com/send?phone='.<?= $this->main->trim_first_char_wa($reseller->phone) ?>.'&text=Hai,%20Saya%20ingin%20bergabung%20jadi%20Reseller%20MKB" class="btn btn-super-res" target="_blank">Gabung Sekarang</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>

                            </div>

                            <div class="accrodion-grp hide" id="template-agent-nonactive" data-grp-name="faq-accrodion">
                                <div class="accrodion card mb-2">
                                    <div class="accrodion-inner card-header p-0 m-0">
                                        <div class="accrodion-title p-3 d-flex align-items-center"
                                            style="cursor:pointer;">
                                            <div
                                                class="circle-ava d-flex align-items-center justify-content-center mr-3 agent-ava">
                                            </div>
                                            <span class="name-res agent-name"></span>
                                            <i class="fa fa-angle-double-down d-inline float-right p-2 icon-drop"></i>
                                        </div>
                                        <div class="accrodion-content bg-white ml-0">

                                            <div class="card-body p-2">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="row py-2">
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="desc-res">
                                                                <i class="fa fa-map"></i> Alamat Outlet<br>
                                                                <p class="agent-address d-inline">
                                                                    <?= $dict_agent_list_address ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row py-2">
                                                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                                                            <div class="desc-res">
                                                                <i class="fa fa-whatsapp"></i> Whatsapp<br>
                                                                <p class="agent-whatsapp d-inline">
                                                                    <?= $dict_agent_list_whatsapp ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-7">
                                                            <div class="desc-res">
                                                                <i class="fa fa-envelope"></i> Email<br>
                                                                <p class="agent-email d-inline">
                                                                    <?= $dict_agent_list_email ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row my-2 px-2">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <a href="" class="btn btn-super-res" target="_blank">Gabung Sekarang</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- section reseller -->
                <!-- <div class="row px-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="faq-accordion agent-accordion">
                            <div id="zero-data" class="hide">
                                <h4 class="text-center"><?= $dict_agent_report_none ?></h4>
                            </div>
                            <div class="accordion hide" id="template-agent-nonactive">
                                <div class="row">
                                    <div
                                        class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3 d-flex align-items-center justify-content-center">
                                        <div class="circle-ava d-flex align-items-center justify-content-center">
                                            NR
                                        </div>
                                    </div>
                                    <div
                                        class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-9 d-flex align-items-center pl-0">
                                        <span class="name-res agent-name"></span><br>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="row d-flex justify-content-end">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <p class="desc-res"><i class="fa fa-map"></i> Jalan Melati Jalan
                                                    Melati
                                                    Jalan Melati Jalan Melati Jalan Melati</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                <p class="desc-res"><i class="fa fa-whatsapp"></i> 089123456789</p>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                <p class="desc-res"><i class="fa fa-envelope"></i>
                                                    testemail@gmail.com</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <a href="#" class="btn btn-res">Beli Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="row px-4 mt-5">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <img class="w-100"
                            src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_32.png") ?>">
                    </div>
                </div>

                <!-- section footer -->
                <footer class="d-flex mt-5">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                            <img class="w-100"
                                src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new.png") ?>"
                                style="bottom:0">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-7 pr-0 pl-0 pt-4 footer-description">
                            <img width="110"
                                src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                                alt="Logo">
                            <br>
                            <span class="company-name">CV. AMERTHA SEDANA SARI</span>
                            <div class="row m-0 pr-1 footer-contact">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-0">
                                    <div>
                                        <b>Office</b>
                                        <br>
                                        <a
                                            href="https://www.google.com/maps/place/MKB+Health+and+Beauty/@-8.6404531,115.2283771,15z/data=!4m5!3m4!1s0x0:0x28f52169c1dbb072!8m2!3d-8.6405313!4d115.228046">Jalan
                                            Ratna No.80 Tonja, Denpasar Utara, Kota Denpasar, Bali</a>
                                    </div>
                                    <div class="mt-3">
                                        <b>Office Hour</b>
                                        <br>
                                        <span>08.00 - 17.00 WITA</span>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12 pl-1 pr-0 pb-5">
                                    <a href="https://wa.me/"><i class="fa fa-whatsapp mr-1"></i>+62 813 376
                                        63109</a>
                                    <br>
                                    <a href="https://www.instagram.com/mkb.healthandbeauty"><i
                                            class="fa fa-instagram"></i><span
                                            class="pl-1">mkb.healthandbeauty</span></a>
                                    <br>
                                    <a href="https://www.facebook.com/Mkbhealthandbeauty-101394608499775"><i
                                            class="fa fa-facebook-square"></i><span
                                            class="pl-1">Mkbhealthandbeauty</span></a>
                                    <br>
                                    <a href=""><i class="fa fa-globe mr-0"></i> www.mykindofbeauty.co.id</a>
                                </div>
                                <div class="col-12 pl-0 mt-4 text-white">
                                    <span>
                                        <i class="fa fa-copyright"></i> mkbhealthandbeauty. 2021
                                    </span>
                                    <div class="py-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </div>
    </div>
    </div>

    <div class='container-loading d-none'>
        <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
            <div class='loader--desc'></div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = document.getElementsByClassName("panel");
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    </script>
</body>

</html>