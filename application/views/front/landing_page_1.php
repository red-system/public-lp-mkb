<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MKB Health and Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/support88.min.css">

    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/support88.min.css"> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.d.css">
</head>

<body>
    <div class="container p-0">
        <div class="row m-0 justify-content-center">
            <div class="col-sm-12 col-md-5 col-lg-5 col-12 bg-white p-0">
                <div class="banner ">
                    <header class="header d-flex justify-content-center">
                        <nav class="py-5">
                            <img width="200"
                                src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                                alt="Logo">
                        </nav>
                    </header>
                    <div class="px-5">
                        <!-- <span class="quote"></span> -->
                        <i class="quote fas fa-quote-left"></i>
                        <div class="banner-text">
                            Banyak orang ingin berbisnis namun <span class="d-inline">tidak memiliki <br>waktu
                                luang</span>
                        </div>
                        <a href="" class="banner-cta"><i class="fas fa-angle-down"></i></a>
                    </div>
                    <img width="80" class="banner-accent float-right mr-2"
                        src="<?php echo base_url();?>assets/images/kemiri/landing-page/3.png" alt="Logo">
                </div>

                <div class="row pt-5 pb-3">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-8 ml-5 question-card">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                <img class="question-image"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/car_1.png") ?>" alt="">
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                                <div class="question-description">
                                    Mahasiswa ingin berbisnis untuk mendapatkan <span class="d-inline">bekal
                                        tambahan?</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row pt-5 pb-3 d-flex justify-content-end">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-8 mr-5 question-card">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                <img width="75" class="question-image"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/car_2.png") ?>" alt="">
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                                <div class="question-description">
                                    Ibu-ibu ingin berbisnis, tapi <span class="d-inline">sambil mengurus anak
                                        dirumah?</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row pt-5 pb-5">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-8 ml-5 question-card">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                <img class="question-image"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/car_3-2.png") ?>"
                                    alt="">
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                                <div class="question-description pl-3">
                                    Banyak pegawai dirumahkan jadi <span class="d-inline">banyak waktu untuk
                                        berbisnis?</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex mt-3 tawaran align-items-center justify-content-between">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-4 p-2">
                        <h4>Apa yang bisa kami tawarkan?</h4>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                        <ul>
                            <li>Pekerjaan sambil mengisi waktu luang</li>
                            <li>Fleksibel / bisa dilakukan dimana aja</li>
                            <li>Menjual produk kecantikan dan kesehatan (dimasa pandemi)</li>
                            <li>Menjual produk dengan sistem sederhana</li>
                            <li>Keuntungan yang ideal</li>
                        </ul>
                    </div>
                </div>
                <div class="baca-banner row mt-4 mb-5">
                    <h6 style="font-size: 16px;">
                        Bagi anda yang sedang membaca <br>tulisan ini, selamat...Anda berada di <br>awal bisnis yang
                        tepat.<br>
                        <span class="d-inline">MKB Health & Beauty</span> sebagai distributor utama <br>produk <span
                            class="d-inline">Minyak Kemiri
                            Bakar</span> memberikan peluang kepada Anda untuk bergabung.
                    </h6>
                </div>


                <div class="d-flex join-banner">
                    <!-- <div class="col-4 p-0"> -->
                    <img class="join-image" width="150"
                        src="<?php echo base_url("assets/images/kemiri/landing-page/car_4.png") ?>" alt="">
                    <!-- </div> -->
                    <!-- <div class="col-8 py-3"> -->
                    <h4>
                        Mengapa harus join dengan kami?
                    </h4>
                    <!-- </div> -->
                </div>
                <div class="gimmick-banner">
                    <h5 class="text-center mb-4 pt-5">Kami memberikan support maksimal kepada Anda berupa</h5>
                    <div class="row">
                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/1.png") ?>" alt="">
                                <h6>Digital Marketing</h6>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/2.png") ?>" alt="">
                                <h6>Foto Produk Berkualitas</h6>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/3.png") ?>" alt="">
                                <h6>Konsultasi Gratis</h6>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/4.png") ?>" alt="">
                                <h6>Aplikasi Kasir</h6>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/5.png") ?>" alt="">
                                <h6>Fasilitas Desain</h6>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="gimmick-card">
                                <img width="50"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/icon/6.png") ?>" alt="">
                                <h6>Banyak Bonus</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row pt-2 pb-2 d-flex justify-content-center">
                    <div class="col-lg-11 col-md-11 col-sm-11 col-11 px-2">
                        <img src="<?php echo base_url("assets/images/kemiri/landing-page/group_2.png") ?>" alt="">
                    </div>
                </div>

                <div class="row pt-2 pb-4 d-flex justify-content-center">
                    <div class="col-lg-11 col-md-11 col-sm-11 col-11 px-2">
                        <img src="<?php echo base_url("assets/images/kemiri/landing-page/group_1.png") ?>" alt="">
                    </div>
                </div>

                <!-- <div class="row py-5 d-flex justify-content-center">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-8 px-2 products-card">
                        <div class="row p-0">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-7">
                            <div class="products-description py-3 pl-3">
                                <span class="d-flex justify-content-end">
                                    Produk yang kami tawarkan (harga Reseller)
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-5 p-0">
                            <img class="products-image"
                                src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-product-new.png") ?>"
                                alt="">
                        </div>
                        </div>
                    </div>
                </div> -->

                <div class="mt-4 mb-3 testtimonial px-4">
                    <div class="row justify-content-center">
                        <div class="col-6 text-center">
                            <h6>Apa kata mereka yang sudah bergabung</h6>
                        </div>
                    </div>
                    <div class="row mt-3 px-4 testimonial-card">
                        <div class="col-4 p-2">
                            <img class="rounded-lg w-100"
                                src="<?php echo base_url("assets/images/landing-page/") ?>testimoni-riri.jpg" alt="">
                            <p class="mt-2 text-sm">
                                Sangat membantu terutama di pandemi ini, ketika kita bisa berbisnis dengan produk
                                kesehatan. pasar yang luas dan keuntungan yang ideal.
                            </p>
                        </div>
                        <div class="col-4 p-2">
                            <img class="rounded-lg w-100"
                                src="<?php echo base_url("assets/images/landing-page/") ?>testimoni-ferby.jpg" alt="">
                            <p class="mt-2 text-sm">
                                Pertama kali mencoba produknya, langsung amaze dengan manfaatnya dengan pemakaian rutin.
                                Terus jalanin bisnisnya, wahh lebih amazing lagi. Jadi klo aku udah jalanin bisnisnya,
                                itu bener-bener produk yang aku suka dan bermanfaat.
                            </p>
                        </div>
                        <div class="col-4 p-2">
                            <img class="rounded-lg w-100"
                                src="<?php echo base_url("assets/images/landing-page/") ?>testimoni-shanti.jpg" alt="">
                            <p class="mt-2 text-sm">
                                MKB selain memiliki manfaat yang luar biasa di rambut dan anakku juga pakai lho, 100%
                                aman untuk anak-anak dan bayi! Aku ikut jadi reseller MKB karena selain produknya yang
                                bagus dan harganya yang cocok, bonusnya juga banyak.
                            </p>
                        </div>
                    </div>
                    <div class="row mt-5 px- d-flex justify-content-center">
                        <div class="col-9 p-0 pl-3 chat-slider d-flex justify-content-center">
                            <div>
                                <img class="w-100 d-flex justify-content-center"
                                    src="<?= base_url('assets/images/kemiri/landing-page/chat-3.jpeg') ?>">
                            </div>
                            <div>
                                <img class="w-100 d-flex justify-content-center"
                                    src="<?= base_url('assets/images/kemiri/landing-page/chat-4.jpeg') ?>">
                            </div>
                            <div>
                                <img class="w-100 d-flex justify-content-center"
                                    src="<?= base_url('assets/images/kemiri/landing-page/chat-5.jpeg') ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0 faq mt-4 justify-content-center">
                    <a href="<?= $url ?>" class="btn-faq">Gabung Sekarang <i class="fas fa-chevron-right"></i></a>
                    <div class="col-12 px-5 mt-5 mb-5 faq-list">
                        <ol type="1">
                            <li>
                                <a data-toggle="collapse" href="#faq-1" role="button" aria-expanded="false"
                                    aria-controls="faq-1">
                                    Berapa Modal yang dikeluarkan?
                                    <span class="float-right">
                                        <i class="fa fa-minus"></i>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                            </li>
                            <div class="collapse mb-2" id="faq-1">
                                <div class="card card-body">
                                    Modal yang dikeluarkan untuk menjadi Reseler di MKB Health and Beauty hanya Rp
                                    1.190.000, sudah mendapatkan produk Minyak Kemiri Bakar.
                                </div>
                            </div>
                            <!-- <li>
                                <a data-toggle="collapse" href="#faq-2" role="button" aria-expanded="false"
                                    aria-controls="faq-2">
                                    Produk bisa dikembalikan?
                                    <span class="float-right">
                                        <i class="fa fa-minus"></i>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                            </li>
                            <div class="collapse mb-2" id="faq-2">
                                <div class="card card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident.
                                </div>
                            </div> -->
                            <!-- <li>
                                <a data-toggle="collapse" href="#faq-3" role="button" aria-expanded="false"
                                    aria-controls="faq-3">
                                    Moneyback Guarantee
                                    <span class="float-right">
                                        <i class="fa fa-minus"></i>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                            </li>
                            <div class="collapse mb-2" id="faq-3">
                                <div class="card card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident.
                                </div>
                            </div> -->
                            <li>
                                <a data-toggle="collapse" href="#faq-4" role="button" aria-expanded="false"
                                    aria-controls="faq-4">
                                    Berapa jumlah reseller yang sudah bergabung?
                                    <span class="float-right">
                                        <i class="fa fa-minus"></i>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                            </li>
                            <div class="collapse mb-2" id="faq-4">
                                <div class="card card-body">
                                    <?php echo $count_res ?> Reseller dan <?php echo $count_superres ?> Super Reseller sudah bergabung di MKB Health and Beauty.
                                </div>
                            </div>
                        </ol>

                    </div>

                </div>

                <footer class="d-flex mt-4">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                            <img class="w-100"
                                src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new.png") ?>"
                                style="bottom:0">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-7 pr-0 pl-0 pt-4 footer-description">
                            <img width="110"
                                src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                                alt="Logo">
                            <br>
                            <span class="company-name">CV. AMERTHA SEDANA SARI</span>
                            <div class="row m-0 pr-1 footer-contact">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-0">
                                    <div>
                                        <b>Office</b>
                                        <br>
                                        <a
                                            href="https://www.google.com/maps/place/MKB+Health+and+Beauty/@-8.6404531,115.2283771,15z/data=!4m5!3m4!1s0x0:0x28f52169c1dbb072!8m2!3d-8.6405313!4d115.228046">Jalan
                                            Ratna No.80 Tonja, Denpasar Utara, Kota Denpasar, Bali</a>
                                    </div>
                                    <div class="mt-3">
                                        <b>Office Hour</b>
                                        <br>
                                        <span>08.00 - 17.00 WITA</span>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12 pl-1 pr-0 pb-5">
                                    <a href="https://wa.me/"><i class="fab fa-whatsapp mr-1"></i>+62 813 376
                                        63109</a>
                                    <br>
                                    <a href="https://www.instagram.com/mkb.healthandbeauty"><i
                                            class="fab fa-instagram"></i><span
                                            class="pl-1">mkb.healthandbeauty</span></a>
                                    <br>
                                    <a href="https://www.facebook.com/Mkbhealthandbeauty-101394608499775"><i
                                            class="fab fa-facebook-square"></i><span
                                            class="pl-1">Mkbhealthandbeauty</span></a>
                                    <br>
                                    <a href=""><i class="fas fa-globe mr-0"></i> www.mykindofbeauty.co.id</a>
                                </div>
                                <div class="col-12 pl-0 mt-4 text-white">
                                    <span>
                                        <i class="fas fa-copyright"></i> mkbhealthandbeauty. 2021
                                    </span>
                                    <div class="py-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <!-- <script src="<?= base_url('assets/js/support88.min.js') ?>"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
    <script>
        $('.chat-slider').slick();
    </script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>