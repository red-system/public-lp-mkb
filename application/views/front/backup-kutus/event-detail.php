<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?= $home->title_menu ? $home->title_menu : 'beranda' ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-area single-blog-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blog-thumb">
                    <img src="<?php echo $this->main->image_preview_url($detail_event->thumbnail) ?>" alt="<?php echo $detail_event->thumbnail_alt ? $detail_event->thumbnail_alt : $detail_event->title ?>" title="<?= $detail_event->thumbnail_alt ? $detail_event->thumbnail_alt : $detail_event->title ?>">
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="single-blog-top-content">
                            <h2 class="title"><?php echo $detail_event->title ?></h2>
                            <span><?php echo date('d M Y', strtotime($detail_event->created_at)) ?></span>
                            <p>
                                <?php echo $detail_event->description; ?>
                            </p>
                            <ul class="blog-share-link">
                                <li><a href="<?php echo $this->main->share_link('facebook', $detail_event->title, site_url('event/'.$detail_event->category_slug.'/'.$detail_event->slug)) ?>" title="link share facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php echo $this->main->share_link('twitter', $detail_event->title, site_url('event/'.$detail_event->category_slug.'/'.$detail_event->slug)) ?>" title="link share twitter"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="blog" class="news-area bg_cover news-page-area">
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-lg-6 col-md-9">
                <div class="section-title text-left">
                    <h3 class="title"><?php echo $dict_other_post ?></h3>
                </div>
            </div>
        </div>
        <div class="row news-active">
            <?php foreach ($random_post as $random) { ?>
                <div class="col-lg-4">
                    <div class="news-item mt-30">
                        <div class="news-thumb">
                            <img src="<?php echo $this->main->image_preview_url($random->thumbnail) ?>" alt="<?php echo $random->thumbnail_alt ? $random->thumbnail_alt : $random->title ?>" title="<?= $random->thumbnail_alt ? $random->thumbnail_alt : $random->title ?>">
                        </div>
                        <div class="news-content">
                            <a href="<?php echo site_url('event/'.$random->slug) ?>" title="<?= $random->title ? $random->title : 'link event' ?>">
                                <h4 class="title mb-2"><?php echo $random->title ?></h4>
                            </a>
                            <span class="mb-1"><?php echo date('d M Y', strtotime($random->created_at)) ?></span>
                            <p><?php echo $random->description ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>