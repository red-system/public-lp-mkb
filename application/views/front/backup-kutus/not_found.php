<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-area single-blog-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="single-blog-top-content">
                            <h3 class="title">404</h3>
                            <h5 class="mb-2 mb-sm-3"><?php echo $dict_404_title_1 ?></h5>
                            <p><?php echo $dict_404_title_2 ?></p>
                            <a href="<?php echo site_url() ?>" class="main-btn" title="<?php echo $dict_404_button_1 ?>"><?php echo $dict_404_button_1 ?><i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>