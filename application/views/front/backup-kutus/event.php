<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?= $home->title_menu ? $home->title_menu : 'beranda' ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                        <form action="<?php echo site_url('event') ?>" method="get">
                            <div class="input-box">
                                <input type="text" name="search" placeholder="Search">
                                <button><i class="far fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-area pt-90 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title mb-45">
                    <h3 class="title"><?php echo $page->title ?></h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php if (count($list) == 0) { ?>
                <div class="col-12 text-center">
                    <h4><?= $dict_no_event_page ?></h4>
                </div>
            <?php } ?>
            <?php foreach ($list as $event) { ?>
            <div class="col-lg-4 col-md-6 col-sm-9">
                <div class="news-item mt-30">
                    <div class="news-thumb">
                        <img src="<?php echo $this->main->image_preview_url($event->thumbnail) ?>" alt="<?php echo $event->thumbnail_alt ? $event->thumbnail_alt : $event->title ?>" title="<?= $event->thumbnail_alt ? $event->thumbnail_alt : $event->title ?>">
                    </div>
                    <div class="news-content">
                        <a href="<?php echo site_url('event/'.$event->slug) ?>" title="<?= $event->title ? $event->title : 'link event' ?>">
                            <h4 class="title mb-2"><?php echo $event->title ?></h4>
                        </a>
                        <span class="mb-1"> <?php echo date('d M Y', strtotime($event->created_at)) ?></span>
                        <p><?php echo $event->description ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="col-lg-12">
                <div class="news-btn text-center mt-60">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</section>