<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-area single-blog-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-center agent-content">
                    <div class="col-lg-8">
                        <div class="single-blog-top-content">
                            <div class="container-agent-image">
                                <img src="http://dev-storage.redsystem.id/redpos/<?= $agen->avatar ?>" alt="<?= $agen->nama ?>" title="<?= $agen->nama ?>" class="agent-image">
                                <p class="agent-type text-center"><?= ucwords($agen->type) ?></p>
                                <p class="agent-name text-center"><?= ucwords($agen->nama) ?></p>
                                <div class="row agent-link justify-content-center">
                                    <a class="agent-individual-link text-center" href="<?= $agen->maps ?>" title="google maps"><i class="fa fa-map"></i></a>
                                    <a class="agent-individual-link text-center" href="mailto:<?= $agen->email ?>" title="link email"><i class="fa fa-envelope"></i></a>
                                    <a class="agent-individual-link text-center" href="https://wa.me/:<?= $this->main->trim_first_char($agen->phone) ?>" title="link whatsapp"><i class="fab fa-whatsapp"></i></a>
                                    <a class="agent-individual-link text-center" href="tel:<?= $this->main->trim_first_char($agen->phone) ?>" title="link telephone"><i class="fa fa-phone"></i></a>
                                    <a class="agent-individual-link text-center justify-content-center row" href="<?= $agen->tokopedia ?>" title="link tokopedia"> <div class="tokoped-white"></div> </a>
                                    <a class="agent-individual-link text-center justify-content-center row" href="<?= $agen->shopee ?>" title="link shopee"> <div class="shopee-white"></div> </a>
                                </div>
                            </div>
                            <p class="agent-description">
                                <?= $agen->deskripsi; ?>
                            </p>
                            <div class="col-lg-12 row justify-content-center">
                                <a class="main-btn" href="<?= $agen->referal_url ?>" target="_blank" title="link registrasi referral"><span><?php echo $dict_btn_register_agen ?> </span><i class="fas fa-angle-right"></i></a>
                            </div>
                            <ul class="blog-share-link">
                                <li><a href="<?php echo $this->main->share_link('facebook', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?>" title="link share facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php echo $this->main->share_link('twitter', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?>" title="link share twitter"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>