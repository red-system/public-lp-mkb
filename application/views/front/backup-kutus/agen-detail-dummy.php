<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-area single-blog-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-center agent-content">
                    <div class="col-lg-8">
                        <div class="single-blog-top-content">
                            <div class="container-agent-image">
<!--                                <img src="http://dev-storage.redsystem.id/redpos/--><?//= $agen->avatar ?><!--" alt="--><?//= $agen->nama ?><!--" title="--><?//= $agen->nama ?><!--" class="agent-image">-->
                                <img src="https://via.placeholder.com/200x350" alt="variabel nama agen" title="variabel nama agen" class="agent-image">
<!--                                <p class="agent-type text-center">--><?//= ucwords($agen->type) ?><!--</p>-->
<!--                                <p class="agent-name text-center">--><?//= ucwords($agen->nama) ?><!--</p>-->
                                <p class="agent-type text-center"><?= ucwords('tipe agen') ?></p>
                                <p class="agent-name text-center"><?= ucwords('nama agen') ?></p>
                                <div class="row agent-link justify-content-center">
<!--                                    <a class="agent-individual-link text-center" href="--><?//= $agen->maps ?><!--" title="google maps"><i class="fa fa-map"></i></a>-->
<!--                                    <a class="agent-individual-link text-center" href="mailto:--><?//= $agen->email ?><!--" title="link email"><i class="fa fa-envelope"></i></a>-->
<!--                                    <a class="agent-individual-link text-center" href="https://wa.me/:--><?//= $this->main->trim_first_char($agen->phone) ?><!--" title="link whatsapp"><i class="fab fa-whatsapp"></i></a>-->
<!--                                    <a class="agent-individual-link text-center" href="tel:--><?//= $this->main->trim_first_char($agen->phone) ?><!--" title="link telephone"><i class="fa fa-phone"></i></a>-->
<!--                                    <a class="agent-individual-link text-center justify-content-center row" href="--><?//= $agen->tokopedia ?><!--" title="link tokopedia"> <div class="tokoped-white"></div> </a>-->
<!--                                    <a class="agent-individual-link text-center justify-content-center row" href="--><?//= $agen->shopee ?><!--" title="link shopee"> <div class="shopee-white"></div> </a>-->
                                    <a class="agent-individual-link text-center" href="" title="google maps"><i class="fa fa-map"></i></a>
                                    <a class="agent-individual-link text-center" href="" title="link email"><i class="fa fa-envelope"></i></a>
                                    <a class="agent-individual-link text-center" href="" title="link whatsapp"><i class="fab fa-whatsapp"></i></a>
                                    <a class="agent-individual-link text-center" href="tel:" title="link telephone"><i class="fa fa-phone"></i></a>
                                    <a class="agent-individual-link text-center justify-content-center row" href="" title="link tokopedia"> <div class="tokoped-white"></div> </a>
                                    <a class="agent-individual-link text-center justify-content-center row" href="" title="link shopee"> <div class="shopee-white"></div> </a>
                                </div>
                            </div>
                            <p class="agent-description">
<!--                                --><?//= $agen->deskripsi; ?>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et nisl sapien. Aliquam libero nulla, egestas eu elementum vitae, ornare et massa. Phasellus et diam quis lorem convallis pretium rhoncus et ligula. Nam finibus arcu eu accumsan facilisis. Donec quis nisl ac diam dictum maximus vel et nisl. Fusce vel pretium dui. Praesent id euismod lacus, vitae sollicitudin erat. Donec volutpat dui id libero fermentum lacinia. Praesent viverra nunc vitae odio porttitor, nec dignissim velit laoreet. Nulla tristique enim ut laoreet vestibulum. Curabitur at semper erat. Nulla scelerisque lacus eu feugiat aliquam.

Interdum et malesuada fames ac ante ipsum primis in faucibus. In et nibh volutpat, interdum urna sit amet, tincidunt sem. Vivamus auctor eros odio, sed blandit velit malesuada blandit. Donec a augue at lorem euismod pretium. Mauris et nibh rutrum, malesuada odio placerat, egestas justo. Morbi eu semper metus. Pellentesque lacinia lacus non cursus consectetur. Aenean sit amet fermentum augue. Aliquam erat volutpat. Duis a sem non lacus gravida interdum eget et lacus. Mauris in erat sed erat hendrerit venenatis in at enim. Vivamus nisl ante, iaculis vel odio ut, sollicitudin suscipit arcu. Mauris nunc lorem, fermentum at tempus ac, imperdiet ornare justo.
                            </p>
                            <div class="col-lg-12 row justify-content-center">
<!--                                <a class="main-btn" href="--><?//= $agen->referal_url ?><!--" target="_blank" title="link registrasi referral"><span>--><?php //echo $dict_btn_register_agen ?><!-- </span><i class="fas fa-angle-right"></i></a>-->
                                <a class="main-btn" href="" target="_blank" title="link registrasi referral"><span><?php echo 'register agen' ?> </span><i class="fas fa-angle-right"></i></a>
                            </div>
                            <ul class="blog-share-link">
<!--                                <li><a href="--><?php //echo $this->main->share_link('facebook', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?><!--" title="link share facebook"><i class="fab fa-facebook-f"></i></a></li>-->
                                <li><a href="<?php echo $this->main->share_link('facebook', 'judul', site_url('agen/')) ?>" title="link share facebook"><i class="fab fa-facebook-f"></i></a></li>
<!--                                <li><a href="--><?php //echo $this->main->share_link('twitter', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?><!--" title="link share twitter"><i class="fab fa-twitter"></i></a></li>-->
                                <li><a href="<?php echo $this->main->share_link('twitter', 'judul', site_url('agen/')) ?>" title="link share twitter"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>