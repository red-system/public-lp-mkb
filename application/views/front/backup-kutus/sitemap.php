<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?php echo $home->title_menu ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="intro" class="features-area intro-2">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="features-box-2">
                    <div class="features-title">
                        <h3 class="title"><?php echo $page->title ?></h3>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-xs-12">
                            <div class="sitemap">
                                <div class="sitemap-item">
                                    <h4>About Support88</h4>
                                    <p></p>
                                    <p><a href="<?= site_url('agen') ?>"><?= $agent->title_menu ?></a></p>
                                    <p><a href="<?= site_url('blog') ?>"><?= $blog->title_menu ?></a></p>
                                    <p><a href="<?= site_url('event') ?>"><?= $event->title_menu ?></a></p>
                                    <p><a href="<?= site_url('faq') ?>"><?= $faq->title_menu ?></a></p>
                                    <p><a href="<?= site_url('tentang-kami') ?>"><?= $about_us->title_menu ?></a></p>
                                    <p><a href="<?= site_url('kontak-kami') ?>"><?= $contact_us->title_menu ?></a></p>
                                </div>
                                <div class="sitemap-item">
                                    <h4>Agen Support88</h4>
                                    <p></p>
                                    <?php foreach ($sitemap as $site) { ?>
                                        <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province)) ?>"><?= $site->province ?></a></p>
                                    <?php } ?>
                                </div>
                                <?php foreach ($sitemap as $site) { ?>
                                    <div class="sitemap-item">
                                        <h4>Agen Wilayah <?= $site->province ?></h4>
                                        <p></p>
                                        <?php foreach ($site->kota_nya as $kota) { ?>
                                            <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province).'/'.$this->main->slug_url($kota->city_name)) ?>"><?= $kota->city_name ?></a></p>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <?php foreach ($sitemap as $site) { ?>
                                    <?php foreach ($site->kota_nya as $kota) { ?>
                                        <div class="sitemap-item">
                                            <h4>Agen Wilayah <?= $kota->city_name ?></h4>
                                            <p></p>
                                            <?php foreach ($kota->kecamatannya as $kecamatan) { ?>
                                                <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province).'/'.$this->main->slug_url($kota->city_name).'/'.$this->main->slug_url($kecamatan->subdistrict_name)) ?>"><?= $kecamatan->subdistrict_name ?></a></p>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>