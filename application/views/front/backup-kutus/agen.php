<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?php echo $home->title_menu; ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="faqs" class="faq-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="section-title mb-45">
                    <h3 class="title"><?php echo $page->title; ?></h3>
                </div>
            </div>
        </div>
        <div class="faq-box agent-box">
            <div class="row">
                <div class="col-lg-8 col-md-9 col-sm-10">
                    <div class="filter-agen mb-5 ml-3 mr-3">
                        <h4><?php echo $dict_filter_agen; ?></h4>
                        <form>
                            <div class="form-row">
                                <input type="text" class="hide" id="pre-define-province" value="<?= $province_name ? $province_name : null; ?>">
                                <input type="text" class="hide" id="pre-define-city" value="<?= $city_name ? $city_name : null; ?>">
                                <input type="text" class="hide" id="pre-define-subdistrict" value="<?= $subdistrict_name ? $subdistrict_name : null; ?>">
                                <div class="form-group col-md-4">
                                    <select name="filter-agent-province" id="filter-agent-province" class="form-control form-control-lg select2-input" data-placeholder="Pilih Provinsi"></select>
                                </div>
                                <div class="form-group col-md-4 hide">
                                    <select name="filter-agent-city" id="filter-agent-city" class="form-control form-control-lg select2-input" data-placeholder="Pilih Kabupaten"></select>
                                </div>
                                <div class="form-group col-md-4 hide">
                                    <select name="filter-agent-area" id="filter-agent-area" class="form-control form-control-lg select2-input" data-placeholder="Pilih Kecamatan"></select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="faq-accordion agent-accordion">
                        <div id="zero-data" class="hide">
                            <h4 class="text-center"><?= $dict_agent_report_none ?></h4>
                        </div>
                        <div class="accrodion-grp hide" id="template-agent-nonactive" data-grp-name="faq-accrodion">
                            <div class="accrodion">
                                <div class="accrodion-inner">
                                    <div class="accrodion-title">
                                        <h4 class="agent-name"></h4>
                                    </div>
                                    <div class="accrodion-content">
                                        <div class="inner">
                                            <span><?= $dict_agent_list_address ?> </span><br>
                                            <p class="agent-address"></p><br>
                                            <p><?= $dict_agent_list_whatsapp ?> <a href="" class="agent-whatsapp" title="Whatsapp agent"></a></p>
                                            <p><?= $dict_agent_list_telephone ?> <a href="" class="agent-phone" title="Phone agent"></a></p>
                                            <p><?= $dict_agent_list_email ?> <a href="" class="agent-email" title="Email agent"></a></p>
                                            <br>
                                            <strong><a href="https://goo.gl/maps/yw8U92kcvQ4ER7PEA" target="_blank" class="mr-3 agent-maps bold" title="Link Google Maps">Google Maps</a></strong>
                                            <strong><a href="" target="_blank" class="mr-3 agent-tokopedia" title="Link Tokopedia">Tokopedia</a></strong>
                                            <strong><a href="" target="_blank" class="agent-shopee" title="Link Shopee">Shopee</a></strong>
                                            <strong><a href="" target="_blank" class="agent-detail main-btn main-btn-2 float-xl-right mt-xs-3 mt-sm-3 mt-lg-3 mt-xl-0" title="Link Detail"><?= $dict_agent_list_detail ?> <i class="fas fa-angle-right"></i> </a></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>