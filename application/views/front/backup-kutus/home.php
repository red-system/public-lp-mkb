    <!--====== HERO PART START ======-->

    <section class="hero-area bg_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="hero-content">
                        <h1 class="title"><span><?php echo $home_sesi_1->title_menu ?></span> <?php echo $home_sesi_1->title ?></h1>
                        <ul class="nav">
                            <li><a class="main-btn" href="<?php echo site_url('agen');?>" title="<?= $dict_btn_to_agen ? $dict_btn_to_agen : 'tombol list agen' ?>"><span><?php echo $dict_btn_to_agen ?> </span><i class="fas fa-angle-right"></i></a></li>
                            <li><a class="main-btn main-btn-2" href="http://app.support88.id/register" target="_blank" title="<?= $dict_btn_register_agen ? $dict_btn_register_agen : 'tombol register agen' ?>"><span><?php echo $dict_btn_register_agen ?> </span><i class="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="hero-thumb">
                        <img src="<?php echo base_url() ?>assets/images/products/minyak-balur.png" alt="Logo Kutus-Kutus" title="Logo Kutus-Kutus">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="intro" class="features-area">
        <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="features-box">
                        <div class="features-title">
                            <h3 class="title"><?php echo $home_sesi_2->title ?></h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="features-item mt-30">
                                    <p><?php echo $home_sesi_2->description ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="features-play">
                            <a class="video-popup" href="https://www.youtube.com/watch?time_continue=74&v=S2IBwp6I-Q4" title="Video Kutus-Kutus"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="intro" class="benefits-area">
        <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="features-box" style="background-image: url(<?= $this->main->image_preview_url($home_sesi_10->thumbnail); ?>)">
                        <div class="col-lg-8">
                            <div class="features-title text-center">
                                <h3 class="title"><?php echo $home_sesi_10->title ?></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 row">
                                <?php $data_1 = json_decode($home_sesi_10->data_1, TRUE) ?>
                                <?php foreach ($data_1['title'] as $key => $service_title) { ?>
                                <div class="col-lg-4 text-center">
                                    <div class="features-item mt-30">
                                        <i class="fal fa-<?php echo $data_1['icons'][$key] ?>"></i>
                                        <h4 class="title"><?php echo $data_1['title'][$key] ?></h4>
                                        <p><?php echo $data_1['description'][$key] ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="features" class="intro-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="section-title text-center">
                        <h3 class="title"><?php echo $home_sesi_5->title ?></h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="intro-thumb">
                        <img src="<?php echo base_url() ?>assets/images/intro-thumb.png" alt="Background Product" title="Background Product">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="intro-content">
                        <?php $data_1_service = json_decode($home_sesi_5->data_1, TRUE); ?>
                        <?php foreach($data_1_service['title'] as $key => $service_title ) { ?>
                        <div class="intro-item mb-30">
                            <i class="fal fa-<?php echo $data_1_service['icons'][$key]; ?>"></i>
                            <h4 class="title"><?php echo $service_title ?></h4>
                        </div>
                        <?php } ?>
                    </div>
                    <a class="main-btn main-btn-2 mt-50 trans-x-100" href="<?php echo site_url('agen');?>" title="<?= $dict_btn_to_agen ? $dict_btn_to_agen : 'tombol list agen' ?>"><span><?php echo $dict_btn_to_agen ?> </span><i class="fas fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <?php
    $count_event = count($list_event);
    if ($count_event !== 0) { ?>
        <section class="capture-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="section-title section-title-bondan">
                            <h3 class="title"><?php echo $home_sesi_3->title; ?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-12">
                    <div class="capture-active">
                        <?php foreach ($list_event as $event) { ?>
                        <div class="capture-item mt-30">
                            <div class="capture-thumb">
                                <img src="<?php echo $this->main->image_preview_url($event->thumbnail) ?>" alt="<?php echo $event->thumbnail_alt ? $event->thumbnail_alt : $event->title; ?>" title="<?= $event->thumbnail_alt ? $event->thumbnail_alt : $event->title ?>">
                            </div>
                            <div class="capture-content">
                                <a href="<?php echo site_url('event/'.$event->slug) ?>" title="<?= $event->title ? $event->title : 'judul event' ?>">
                                    <h4 class="title mb-2"><?php echo $event->title; ?></h4>
                                </a>
                                <span class="mb-1"><?php echo date('d M Y', $blog->created_at) ?></span>
                                <p><?php echo $event->description; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-12 text-center mt-5">
                    <a href="<?= site_url('event') ?>" class="btn main-btn main-btn-2" title="<?= $dict_btn_to_event ? $dict_btn_to_event : 'tombol list event' ?>"><?php echo $dict_btn_to_event ?> <i class="fas fa-angle-right"></i></a>
                </div>
            </div>
        </section>
    <?php } ?>

    <section id="faqs" class="faq-area <?php if ($count_event == 0) { ?> remove-before-background <?php } ?>"  style=""  >
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title mb-45">
                        <h3 class="title"><?php echo $home_sesi_4->title; ?></h3>
                    </div>
                </div>
            </div>
            <div class="faq-box">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="faq-accordion">
                            <div class="accrodion-grp" data-grp-name="faq-accrodion">
                                <?php $data_1 = json_decode($faq->data_1, TRUE); ?>
                                <?php foreach($data_1['title'] as $key => $faq_title) { ?>
                                <div class="accrodion <?php if ($key == 0) { echo 'active'; } ?>  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4><?php echo $faq_title; ?></h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p><?php echo $data_1['description'][$key] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center">
                        <a href="<?php echo site_url('frequently-asked-question') ?>" class="btn main-btn main-btn-2" title="<?= $dict_btn_to_faq ? $dict_btn_to_faq : 'tombol list faq' ?>"><?php echo $dict_btn_to_faq ?> <i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="faq-shape"></div>
    </section>

    <section id="feedbacks" class="testimonial-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h3 class="title"><?php echo $home_sesi_6->title ?></h3>
                    </div>
                </div>
            </div>
            <div class="row testimonial-active" data-count-testimonial="<?= count($testimonial); ?>">
                <?php foreach($testimonial as $testi) { ?>
                <div class="col-lg-4">
                    <div class="testimonial-item mt-30">
                        <img src="<?php echo $this->main->image_preview_url($testi->thumbnail) ?>" alt="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>" title="<?= $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>">
                        <p>“<?php echo $testi->description ?>”</p>
                        <h5 class="title"><?php echo $testi->title; ?></h5>
                        <span><?php echo $testi->title_sub; ?></span>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>

    <div class="download-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="section-title">
<!--                        <h3 class="title">--><?php //echo $home_sesi_7->title; ?><!--</h3>-->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="download-btns">
                        <ul class="nav">
                            <li><a class="main-btn" href="<?= site_url('agen'); ?>" title="<?= $dict_btn_to_agen ? $dict_btn_to_agen : 'tombol list agen' ?>"><?php echo $dict_btn_to_agen ?> <i class="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="blog" class="news-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-9">
                    <div class="section-title text-center">
                        <h3 class="title"><?php echo $home_sesi_8->title; ?></h3>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php foreach ($list_blog as $blog) { ?>
                <div class="col-lg-4 col-md-7 col-sm-9">
                    <div class="news-item mt-30">
                        <div class="news-thumb">
                            <img src="<?php echo $this->main->image_preview_url($blog->thumbnail) ?>" alt="<?php echo $blog->thumbnail_alt ? $blog->thumbnail_alt : $blog->title ?>" title="<?= $blog->thumbnail_alt ? $blog->thumbnail_alt : $blog->title ?>">
                        </div>
                        <div class="news-content">
                            <a href="<?php echo site_url('blog-and-news/'.$blog->category_slug.'/'.$blog->slug) ?>" title="<?= $blog->title ? $blog->title : 'judul blog' ?>">
                                <h4 class="title mb-2"><?php echo $blog->title ?></h4>
                            </a>
                            <span class="mb-1"><?php echo date('d M Y', $blog->created_at) ?></span>
                            <p><?php echo $blog->description; ?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-lg-12">
                    <div class="news-btn text-center mt-60">
                        <a class="main-btn main-btn-2" href="<?= site_url('blog-and-news') ?>" title="<?= $dict_btn_list_blog ? $dict_btn_list_blog : 'tombol list blog' ?>"><?php echo $dict_btn_list_blog ?> <i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>