<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $page->meta_title .' | '. $this->main->web_name() ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta content="telephone=no" name="format-detection">
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="reply-to" content="info@redsystem.id">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/fav.png" type="image/png">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/support88.min.css">
</head>

<body>
    <div id="base_url" class="hide"><?php echo base_url(); ?></div>
    <div id="site_url" class="hide"><?php echo site_url(); ?></div>

    <div class="preloader hide">
        <div class="lds-ellipsis">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="off_canvars_overlay">

    </div>

    <div class="offcanvas_menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="offcanvas_menu_wrapper">
                        <div class="canvas_close">
                            <a href="javascript:void(0)" title="Close menu"><i class="fal fa-times"></i></a>
                        </div>
                        <div class="offcanvas-social">
                            <ul class="text-center">
                                <li><a href="<?php echo $this->main->share_link('facebook', $this->main->web_name(), site_url()) ?>" title="link share facebook"><i class="fab fa-facebook-square"></i></a></li>
                                <li><a href="<?php echo $this->main->share_link('twitter', $this->main->web_name(), site_url()) ?>" title="link share twitter"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div id="menu" class="text-left ">
                            <ul class="offcanvas_main_menu">
                                <li class="menu-item-has-children <?php echo $page->type == "home" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url();?>" title="<?= $home->title_menu ? $home->title_menu : 'link beranda' ?>"><?php echo $home->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "agent" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('agen');?>" title="<?= $agent->title_menu ? $agent->title_menu : 'link agent' ?>"><?php echo $agent->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "blog" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('blog-and-news');?>" title="<?= $blog->title_menu ? $blog->title_menu : 'link blog' ?>"><?php echo $blog->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "event" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('event');?>" title="<?= $event->title_menu ? $event->title_menu : 'link event' ?>"><?php echo $event->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "faq" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('faq');?>" title="<?= $faq->title_menu ? $faq->title_menu : 'link faq' ?>"><?php echo $faq->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "about_us" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('tentang-kami');?>" title="<?= $about_us->title_menu ? $about_us->title_menu : 'link tentang kami' ?>"><?php echo $about_us->title_menu ?></a>
                                </li>
                                <li class="menu-item-has-children <?php echo $page->type == "contact_us" ? "active" : "" ?>">
                                    <a class="page-scroll" href="<?php echo site_url('kontak-kami');?>" title="<?= $contact_us->title_menu ? $contact_us->title_menu : 'link kontak kami' ?>"><?php echo $contact_us->title_menu ?></a>
                                </li>
                            </ul>
                        </div>
                        <div class="offcanvas_footer">
                            <span><a href="mailto:no-reply@redsystem.id" title="Email redsystem"><i class="fa fa-envelope-o"></i>no-reply@redsystem.id</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <header id="home" class="header-area header-v1-area">
        <div class="header-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="navigation">
                            <nav class="navbar navbar-expand-lg navbar-light ">
                                <a class="navbar-brand" href="<?php echo site_url() ?>" title="Kembali ke beranda"><img src="<?php echo base_url();?>assets/images/logo-white.png" alt="Logo Kutus-Kutus" title="Logo Kutus Kutus"></a>

                                <span class="side-menu__toggler canvas_open"><i class="fa fa-bars"></i></span>

                                <div class="collapse navbar-collapse sub-menu-bar main-nav__main-navigation" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto main-nav__navigation-box">
                                        <li class="nav-item <?php echo $page->type == "home" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url();?>" title="<?= $home->title_menu ? $home->title_menu : 'link beranda' ?>"><?php  echo $home->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "agent" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('agen');?>" title="<?= $agent->title_menu ? $agent->title_menu : 'link agent' ?>"><?php echo $agent->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "blog" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('blog-and-news');?>" title="<?= $blog->title_menu ? $blog->title_menu : 'link blog' ?>"><?php echo $blog->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "event" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('event');?>" title="<?= $event->title_menu ? $event->title_menu : 'link event' ?>"><?php echo $event->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "faq" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('faq');?>" title="<?= $faq->title_menu ? $faq->title_menu : 'link faq' ?>"><?php echo $faq->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "about_us" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('tentang-kami');?>" title="<?= $about_us->title_menu ? $about_us->title_menu : 'link tentang kami' ?>"><?php echo $about_us->title_menu ?></a>
                                        </li>
                                        <li class="nav-item <?php echo $page->type == "contact_us" ? "active" : "" ?>">
                                            <a class="nav-link page-scroll" href="<?php echo base_url('kontak-kami');?>" title="<?= $contact_us->title_menu ? $contact_us->title_menu : 'link kontak kami' ?>"><?php echo $contact_us->title_menu ?></a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php echo $content; ?>

    <section class="footer-area bg_cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-about mt-30">
                        <a href="<?= site_url() ?>" class="mid-logo-footer" title="Kembali ke beranda"><img src="<?php echo base_url();?>/assets/images/logo-white.png" alt="Support88 Logo" title="Support88 Logo"></a>
                        <?php echo $home_sesi_footer->description; ?>
                        <ul>
                            <li><a href="<?php echo $this->main->share_link('facebook', $this->main->web_name(), site_url()) ?>" title="link share facebook"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo $this->main->share_link('twitter', $this->main->web_name(), site_url()) ?>" title="link share twitter"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                    <div class="footer-list mt-40">
                        <h5 class="title"><?php echo $dict_footer_explore ?></h5>
                        <ul>
                            <li><a href="<?php echo site_url('agen') ?>" title="<?= $agent->title_menu ? $agent->title_menu : 'link agent' ?>"><?php echo $agent->title_menu ?></a></li>
                            <li><a href="<?php echo site_url('blog-and-news') ?>" title="<?= $blog->title_menu ? $blog->title_menu : 'link blog' ?>"><?php echo $blog->title_menu ?></a></li>
                            <li><a href="<?php echo site_url('event') ?>" title="<?= $event->title_menu ? $event->title_menu : 'link event' ?>"><?php echo $event->title_menu ?></a></li>
                            <li><a href="<?php echo site_url('tentang-kami') ?>" title="<?= $about_us->title_menu ? $about_us->title_menu : 'link tentang kami' ?>"><?php echo $about_us->title_menu ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                    <div class="footer-list mt-40 pl-55">
                        <h5 class="title"><?php echo $dict_footer_help ?></h5>
                        <ul>
                            <li><a href="<?php echo site_url('frequently-asked-question') ?>" title="<?= $faq->title_menu ? $faq->title_menu : 'link faq' ?>"><?php echo $faq->title_menu ?></a></li>
                            <li><a href="<?php echo site_url('kontak-kami') ?>" title="<?= $contact_us->title_menu ? $contact_us->title_menu : 'link kontak kami' ?>"><?php echo $contact_us->title_menu ?></a></li>
                            <li><a href="<?php echo site_url('sitemap') ?>" title="<?= $sitemap->title_menu ? $sitemap->title_menu : 'link sitemap' ?>"><?php echo $sitemap->title_menu ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <span class="copyright"> <a class="text-white" href="https://redsystem.id" title="Redsystem">© REDSYSTEM</a></span>
                </div>
            </div>
        </div>
    </section>

    <a class="back-to-top" title="Back to top">
        <i class="fal fa-angle-up"></i>
    </a>

    <script src="<?= base_url() ?>assets/js/support88.min.js"></script>

</body>

</html>
