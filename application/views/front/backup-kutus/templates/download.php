<section class="download-app-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="download-app-content">
                    <h3 class="title"><?php echo $download->title; ?></h3>
                    <ul class="nav">
                        <li><a class="main-btn main-btn-3" href="<?php echo base_url();?>agen"><?php echo $dict_btn_to_agen ?> <i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="download-app-case">
        <img src="assets/images/download-app-case.png" alt="<?php echo $download->thumbnail_alt ? $download->thumbnail_alt : $download->title ?>">
    </div>
</section>