<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?= $home->title_menu ? $home->title_menu : 'beranda' ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="intro" class="features-area intro-2">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="features-box-2">
                    <div class="features-title">
                        <h3 class="title"><?php echo $page->title ?></h3>
                    </div>
                    <div class="row justify-content-center contact-us-content">
                        <div class="row">
                            <?php if (!empty($contact_info['address'])) { ?>
                                <div class="col-lg-4">
                                    <div class="features-item mt-30">
                                        <i class="fal fa-map"></i>
                                        <h4 class="title"><?= $dict_address ?></h4>
                                        <p><a href="<?php echo $contact_info['address_link'] ?>" title="<?= $dict_address ? $dict_address : 'alamat' ?>"><?php echo $contact_info['address']; ?></a></p>
                                    </div>
                                </div>
                            <?php } ?>


                            <?php if (!empty($contact_info['phone'])) { ?>
                                <div class="col-lg-4">
                                    <div class="features-item mt-30">
                                        <i class="fal fa-address-book"></i>
                                        <h4 class="title"><?= $dict_telephone ?></h4>
                                        <p><a href="<?php echo $contact_info['phone_link'] ?>" title="<?= $dict_telephone ? $dict_telephone : 'telephone' ?>"><?php echo $contact_info['phone']; ?></a></p>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (!empty($contact_info['email'])) { ?>
                                <div class="col-lg-4">
                                    <div class="features-item mt-30">
                                        <i class="fal fa-envelope"></i>
                                        <h4 class="title"><?= $dict_email ?></h4>
                                        <p><a href="<?php echo $contact_info['email_link'] ?>" title="<?= $dict_email ? $dict_email : 'email' ?>"><?php echo $contact_info['email']; ?></a></p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-8">
                            <div class="single-blog-top-content">
                                <form action="<?php echo site_url('kontak-kami/send') ?>" method="post" class="form-send">
                                    <div class="row input-box">
                                        <div class="form-group col-md-6">
                                            <input type="text" class="form-control" name="name" placeholder="Nama *" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" class="form-control" name="phone" placeholder="Telephone *" required>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <input type="text" class="form-control" name="email" placeholder="Email *" required>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <textarea type="text" class="form-control" name="message" placeholder="Message *" required></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php echo $captcha ?>
                                            <input type="text" class="form-control mt-10" name="captcha" placeholder="Captcha *" required>
                                        </div>
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-primary btn-lg"><?= $dict_message_send ?> <i class="fas fa-arrow-right"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>