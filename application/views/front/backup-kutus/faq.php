<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?= $home->title_menu ? $home->title-menu : 'beranda' ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="faqs" class="faq-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title mb-45">
                    <h3 class="title"><?php echo $page->title ?></h3>
                </div>
            </div>
        </div>
        <div class="faq-box">
            <div class="row">
                <div class="col-lg-8">
                    <div class="faq-accordion">
                        <div class="accrodion-grp" data-grp-name="faq-accrodion">
                            <?php $data_1 = json_decode($page->data_1, TRUE); ?>
                            <?php foreach ($data_1['title'] as $key => $title) { ?>
                                <div class="accrodion <?php if ($key == 0) { echo 'active'; } ?>  animated wow fadeInRight" data-wow-duration="1500ms" data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4><?php echo $title ?></h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p><?php echo $data_1['description'][$key] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== FAQ PART ENDS ======-->
