<section class="page-title-area bg_cover bg-cover-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-item d-flex align-items-end">
                    <div class="page-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url() ?>" title="<?php echo $home->title_menu ?>"><?php echo $home->title_menu ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $page->title_menu ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="intro" class="features-area intro-2">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="features-box-2">
                    <div class="features-title">
                        <h3 class="title"><?php echo $page->title ?></h3>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-xs-12">
                            <div class="single-blog-top-content">
                                <?php echo $page->description; ?>
                                <ul>
                                    <li><a href="<?php echo $this->main->share_link('facebook', $page->title, site_url('tentang-kami')) ?>" title="share facebook"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="<?php echo $this->main->share_link('twitter', $page->title, site_url('tentang-kami')) ?>" title="share twitter"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php $data_1 = json_decode($page->data_1, TRUE); ?>
                        <?php foreach($data_1['title'] as $key => $title) { ?>
                            <div class="col-lg-3">
                                <div class="features-item about-item mt-30">
                                    <i class="fal fa-<?php echo $data_1['icons'][$key] ?>"></i>
                                    <h4 class="title"><?php echo $title ?></h4>
                                    <p><?php echo $data_1['description'][$key]; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="feedbacks" class="testimonial-area mb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title">
                    <h3 class="title"><?php echo $testimonial_page->title; ?></h3>
                </div>
            </div>
        </div>
        <div class="row testimonial-active">
            <?php foreach($testimonial as $testi) { ?>
            <div class="col-lg-4">
                <div class="testimonial-item mt-30">
                    <img src="<?php echo $this->main->image_preview_url($testi->thumbnail) ?>" title="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title; ?>" alt="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>">
                    <p>“<?php echo $testi->description ?>”</p>
                    <h5 class="title"><?php echo $testi->title; ?></h5>
                    <span><?php echo $testi->title_sub; ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>