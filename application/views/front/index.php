<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>

	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $page->meta_title .' | '. $this->main->web_name() ?></title>
	<meta name="keywords" content="<?php echo $page->meta_keywords ?>" />
	<meta name="description" content="<?php echo $page->meta_description ?>">
	<meta name="author" content="<?php echo $author ?>">
	<meta name="revisit-after" content="2 days" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="General" />
	<meta http-equiv="charset" content="ISO-8859-1" />
	<meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
	<meta http-equiv="content-language" content="<?php echo $lang_active->code ?>" />
	<meta content="telephone=no" name="format-detection">
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="reply-to" content="info@redsystem.id">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

	<!-- include the site Google Fonts stylesheet -->
	<link
		href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CRoboto:300,400,500,700,900&amp;display=swap"
		rel="stylesheet">
	<!-- include the site bootstrap stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<!-- include the site fontawesome stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome.css">
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
	<!-- include theme plugins setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/plugins.css">
	<!-- include theme color setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color.css">
	<!-- include theme responsive setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/swal.min.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/css/select2.css">
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/select2-bootstrap4-theme/select2-bootstrap4.min.css"> -->

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/loading.css">


	<!-- <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/png"> -->

	<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/support88.min.css"> -->
</head>

<body>
	<!-- <div id="base_url" class="hide">https://api.mykindofbeauty.co.id/public/v1/</div> -->
	<!-- <div id="site_url" class="hide">https://api.mykindofbeauty.co.id/public/v1/</div> -->
	<div id="base_url" class="hide"><?php echo base_url(); ?></div>
	<div id="site_url" class="hide"><?php echo site_url(); ?></div>

	<!-- pageWrapper -->
	<div id="pageWrapper">

		<header id="header" class="pt-lg-4 pt-md-3 pt-2 position-absolute w-100">
			<div class="container-fluid px-xl-17 px-lg-5 px-md-3 px-0 d-flex flex-wrap">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  static-block">
					<!-- mainHolder -->
					<div class="mainHolder justify-content-center">
						<!-- pageNav1 -->
						<nav class="navbar navbar-expand-lg navbar-light p-0 pageNav1 position-static">
							<button type="button" class="navbar-toggle collapsed position-relative mt-md-2"
								data-toggle="collapse" data-target="#navbarNav" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<div class="collapse navbar-collapse" id="navbarNav">
								<ul class="navbar-nav mx-auto text-uppercase d-inline-block">
									<!-- <li class="nav-item">
										<a class="d-block" href="<?php echo site_url();?>"><?php echo $home->title_menu ?></a>
                                    </li> -->
									<li class="nav-item">
										<a class="d-block"
											href="<?php echo site_url('agen');?>"><?php echo $agent->title_menu ?></a>
									</li>
									<li class="nav-item <?php echo $page->type == "blog" ? "active" : "" ?>">
										<a class="d-block"
											href="<?php echo site_url('blog-and-news');?>"><?php echo $blog->title_menu ?></a>
									</li>
									<li class="nav-item <?php echo $page->type == "event" ? "active" : "" ?>">
										<a class="d-block"
											href="<?php echo site_url('event');?>"><?php echo $event->title_menu ?></a>
									</li>
									<li class="nav-item <?php echo $page->type == "home" ? "active" : "" ?>">
										<a class="nLogo" href="<?php echo site_url();?>"><img
												src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
												alt="Minyak Kemiri" class="img-fluid pt-4"></a>
									</li>
									<li class="nav-item <?php echo $page->type == "faq" ? "active" : "" ?>">
										<a class="d-block"
											href="<?php echo site_url('faq');?>"><?php echo $faq->title_menu ?></a>
									</li>
									<li class="nav-item <?php echo $page->type == "about_us" ? "active" : "" ?>">
										<a class="d-block"
											href="<?php echo site_url('tentang-kami');?>"><?php echo $about_us->title_menu ?></a>
									</li>
									<li class="nav-item <?php echo $page->type == "contact_us" ? "active" : "" ?>">
										<a class="d-block"
											href="<?php echo site_url('kontak-kami');?>"><?php echo $contact_us->title_menu ?></a>
									</li>

								</ul>
							</div>
						</nav>
						<div class="logo pt-md-1 pt-sm-1">
							<a href="<?php echo site_url();?>"><img
									src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
									alt="Minyak Kemiri" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>
		</header>

		<main>

			<?php echo $content; ?>

			<!-- footerHolder -->
			<aside
				class="footerHolder container-fluid overflow-hidden px-xl-20 px-lg-14 pt-xl-12 pb-xl-8 pt-lg-12 pt-md-8 pt-10 pb-lg-8">
				<div class="d-flex flex-wrap flex-lg-nowrap">
					<div class="coll-1 pr-3 mb-sm-4 mb-3 mb-lg-0">
						<h3 class="headingVI fwEbold text-uppercase mb-7">Contact Us</h3>
						<ul class="list-unstyled footerContactList mb-3">
							<li class="mb-3 d-flex flex-nowrap"><span class="icon icon-place mr-3"></span>
								<address class="fwEbold m-0">Address: Ratna 68G, Gatsu Timur, Denpasar, Bali </address>
							</li>
							<li class="mb-3 d-flex flex-nowrap"><span class="icon icon-phone mr-3"></span> <span
									class="leftAlign">Phone : <a href="javascript:void(0);">(+032) 3456 7890</a></span>
							</li>
							<li class="email d-flex flex-nowrap"><span class="icon icon-email mr-2"></span> <span
									class="leftAlign">Email: <a href="javascript:void(0);">Minyak
										Kemiristore@gmail.com</a></span></li>
						</ul>
						<ul class="list-unstyled followSocailNetwork d-flex flex-nowrap">
							<li class="fwEbold mr-xl-11 mr-sm-6 mr-4">Share us:</li>
							<li class="mr-xl-6 mr-sm-4 mr-2"><a
									href="<?php echo $this->main->share_link('facebook', $this->main->web_name(), site_url()) ?>"
									class="fab fa-facebook-f" title="link share facebook"></a></li>
							<li class="mr-xl-6 mr-sm-4 mr-2"><a
									href="<?php echo $this->main->share_link('twitter', $this->main->web_name(), site_url()) ?>"
									class="fab fa-twitter" title="link share twitter"></a>
							</li>
						</ul>
					</div>
					<div class="coll-1 mb-sm-4 mb-3 mb-lg-0">
						<h3 class="headingVI fwEbold text-uppercase mb-6">Explore</h3>
						<ul class="list-unstyled footerNavList">
							<li class="mb-1"><a
									href="<?php echo site_url('agen');?>"><?php echo $agent->title_menu ?></a></li>
							<li class="mb-2"><a
									href="<?php echo site_url('blog-and-news');?>"><?php echo $blog->title_menu ?></a>
							</li>
							<li class="mb-2"><a
									href="<?php echo site_url('event');?>"><?php echo $event->title_menu ?></a></li>
							<li class="mb-2"><a
									href="<?php echo site_url('tentang-kami');?>"><?php echo $about_us->title_menu ?></a>
							</li>
						</ul>
					</div>
					<div class="coll-1 mb-sm-4 mb-3 mb-lg-0">
						<h3 class="headingVI fwEbold text-uppercase mb-6">Help</h3>
						<ul class="list-unstyled footerNavList">
							<li class="mb-2"><a href="<?php echo site_url('faq');?>"><?php echo $faq->title_menu ?></a>
							</li>
							<li class="mb-2"><a
									href="<?php echo site_url('kontak-kami');?>"><?php echo $contact_us->title_menu ?></a>
							</li>
							<li class="mb-2"><a href="<?php echo site_url('sitemap') ?>"
									title="<?= $sitemap->title_menu ? $sitemap->title_menu : 'link sitemap' ?>"><?php echo $sitemap->title_menu ?></a>
							</li>
						</ul>
					</div>
				</div>
			</aside>
		</main>
		<!-- footer -->
		<footer id="footer" class="container-fluid overflow-hidden px-lg-20">
			<div class="copyRightHolder text-center pt-lg-5 pb-lg-4 py-3">
				<p class="mb-0">Coppyright 2020 by <a href="https://redsystem.id">Red System</a> - All right
					reserved</p>
			</div>
		</footer>
	</div>

	<div class='container-loading d-none'>
        <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
            <div class='loader--desc'></div>
        </div>
    </div>

	<!-- include jQuery library -->
	<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
	<!-- include bootstrap popper JavaScript -->
	<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
	<!-- include bootstrap JavaScript -->
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- include custom JavaScript -->
	<script src="<?php echo base_url();?>assets/js/jqueryCustome.js"></script>
	<!-- Select2 -->
	<script src="<?php echo base_url();?>assets/select2/js/select2.full.min.js"></script>

	<script src="<?php echo base_url();?>assets/js/masonry.js"></script>
	
	<script src="<?php echo base_url();?>assets/js/swal.min.js"></script>

	<script src="<?php echo base_url();?>assets/js/main.js"></script>


</body>

</html>