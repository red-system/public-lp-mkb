<!DOCTYPE html>
<html lang="en">
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the Compatible of your site -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- set the page title -->
	<title>Minyak Kemiri</title>
	<!-- include the site Google Fonts stylesheet -->
	<link href="<?php echo base_url();?>assets/https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CRoboto:300,400,500,700,900&amp;display=swap" rel="stylesheet">
	<!-- include the site bootstrap stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<!-- include the site fontawesome stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome.css">
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
	<!-- include theme plugins setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/plugins.css">
	<!-- include theme color setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color.css">
	<!-- include theme responsive setting stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
</head>
<body>
	<!-- pageWrapper -->
	<div id="pageWrapper">
		<!-- pageHeader -->
		<header id="header" class="pt-lg-5 pt-md-3 pt-2 position-absolute w-100">
			<div class="container-fluid px-xl-17 px-lg-5 px-md-3 px-0 d-flex flex-wrap">
				<div class="col-6 col-sm-3 col-lg-2 order-sm-2 order-md-0 dis-none">
					<!-- langList -->
					<!-- <ul class="nav nav-tabs langList pt-xl-6 pt-lg-4 pt-3 border-bottom-0">
						<li>
							<a class="icon-menu" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false"></a>
							<div class="dropdown-menu pl-4 pr-4">
								<a class="dropdown-item" href="javascript:void(0);">Login</a>
								<a class="dropdown-item" href="javascript:void(0);">Registration</a>
								<a class="dropdown-item" href="javascript:void(0);">Logout</a>
							</div>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle text-uppercase" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">ENG</a>
							<div class="dropdown-menu pl-4 pr-4">
								<a class="dropdown-item" href="javascript:void(0);">English</a>
								<a class="dropdown-item" href="javascript:void(0);">Vietnamese</a>
								<a class="dropdown-item" href="javascript:void(0);">French</a>
							</div>
						</li>
					</ul> -->
				</div>
				<div class="col-12 col-sm-6 col-lg-8 static-block">
					<!-- mainHolder -->
					<div class="mainHolder justify-content-center">
						<!-- pageNav1 -->
						<nav class="navbar navbar-expand-lg navbar-light p-0 pageNav1 position-static">
							<button type="button" class="navbar-toggle collapsed position-relative mt-md-2" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<div class="collapse navbar-collapse" id="navbarNav">
								<ul class="navbar-nav mx-auto text-uppercase d-inline-block">
									<li class="nav-item">
										<a class="d-block" href="<?php echo base_url();?>">Home</a>
									</li>
									<li class="nav-item">
										<a class="d-block" href="<?php echo base_url();?>home/about">About</a>
									</li>
									<li class="nav-item">
										<a class="nLogo" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo_MKB1.png" alt="Minyak Kemiri" class="img-fluid"></a>
									</li>
									<li class="nav-item">
										<a class="d-block" href="<?php echo base_url();?>home/blog">Blog</a>
									</li>
									<li class="nav-item">
										<a class="d-block" href="<?php echo base_url();?>home/contact">Contact</a>
									</li>
								</ul>
							</div>
						</nav>
						<div class="logo">
							<a href="<?php echo base_url();?>assets/home.html"><img src="<?php echo base_url();?>assets/images/logo.png" alt="Minyak Kemiri" class="img-fluid"></a>
						</div>
					</div>
				</div>
				<div class="col-6 col-sm-3 col-lg-2 order-sm-3 order-md-0 dis-none">
					<!-- wishList -->
					<!-- <ul class="nav nav-tabs wishList pt-xl-5 pt-lg-4 pt-3 mr-xl-3 mr-0 justify-content-end border-bottom-0">
						<li class="nav-item"><a class="nav-link position-relative icon-cart" href="javascript:void(0);"><span class="num rounded d-block">2</span></a></li>
					</ul> -->
				</div>
			</div>
        </header>
        	<!-- main -->
		<main>