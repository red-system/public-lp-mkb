<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="<?php echo site_url() ?>"
                            title="beranda"><?php echo $home->title_menu ?></a></li>
                    <li class="mr-2">/</li>
                    <li class="active"><?php echo $page->title_menu ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-10 pb-xl-17 pt-lg-10 pb-lg-4 pt-md-10 pb-md-2 pt-10">
    <div class="twoColumns container">
        <div class="row mb-6">
            <div class="col-12 col-lg-5 order-lg-1">
                <!-- productSliderImage -->
                <div class="productSliderImage mb-lg-0 mb-4">
                    <div>
                        <img src="http://dev-storage.redsystem.id/redpos/<?= $agen->avatar ?>" alt="<?= $agen->nama ?>"
                            alt="image description" class="agent-image img-fluid w-100" title="<?= $agen->nama ?>">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-7 order-lg-3">
                <!-- productTextHolder -->
                <div class="productTextHolder overflow-hidden">
                    <h2 class="agent-name fwEbold mb-2"><?= ucwords($agen->nama) ?></h2>
                    <strong class="agent-type price d-block mb-5 text-green"><?= ucwords($agen->type) ?></strong>
                    <p class="agent-descriptionmb-5"><?= $agen->deskripsi; ?></p>

                    <div class="row">
                        <div class="holder col-12  overflow-hidden d-flex flex-wrap mb-6">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="<?= $agen->referal_url ?>" target="_blank" title="link registrasi referral"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fas fa-map"></i> Lokasi
                                </a>
                            </div>
                            
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="mailto:<?= $agen->email ?>" title="link email" target="_blank"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fas fa-envelope"></i> Email
                                </a>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="tel:<?= $this->main->trim_first_char($agen->phone) ?>" title="link telephone"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fas fa-phone"></i> Telepon
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="holder col-12 overflow-hidden d-flex flex-wrap mb-6">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="https://wa.me/:<?= $this->main->trim_first_char($agen->phone) ?>"
                                    title="link whatsapp"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fab fa-whatsapp"></i> WhatsApp
                                </a>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="<?= $agen->tokopedia ?>" title="link tokopedia"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fas fa-whatsapp mr-2"></i> Tokopedia
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                <a href="<?= $agen->shopee ?>" title="link shopee"
                                    class="btn btn-block btnTheme fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4">
                                    <i class="fas fa-whatsapp mr-2"></i> Shopee
                                </a>
                            </div>
                        </div>
                    </div>

                    <ul class="list-unstyled socialNetwork d-flex flex-wrap mb-sm-4 mb-4">
                        <li class="text-uppercase mr-5">Bagikan halaman ini :</li>
                        <li class="mr-4"><a
                                href="<?php echo $this->main->share_link('facebook', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?>"
                                title="link share facebook" class="fab fa-facebook-f"></a></li>
                        <li class="mr-4"><a
                                href="<?php echo $this->main->share_link('twitter', $agen->title, site_url('agen/'.$this->main->slug($agen->province).'/'.$this->main->slug($agen->city_name).'/'.$this->main->slug($agen->subdistrict_name).'/'.$this->main->slug($agen->username))) ?>"
                                title="link share twitter" class="fab fa-twitter"></a></li>
                    </ul>

                    <div class="holder overflow-hidden d-flex flex-wrap mb-6">
                        <a href="<?= $agen->referal_url ?>" target="_blank" title="link registrasi referral"
                            class="btn btnTheme btnShop fwEbold text-white md-round py-3 px-4 py-md-3 px-md-4"><?php echo $dict_btn_register_agen ?>
                            <i class="fas fa-arrow-right ml-2"></i></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>