<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a></li>
                    <li class="mr-2">/</li>
                    <li class="active"><?php echo $page->title_menu ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- latestSec -->
<section class="discoverSecHolder container pt-xl-19 pb-xl-12 pt-lg-20 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-5">
    <div class="row rightAlign">
        <div class="col-12 col-lg-12 mb-lg-0 mb-4">
            <div class="imgHolder position-relative">
                <img src="<?php echo base_url();?>assets/images/b-bdr.png" alt="image description"
                    class="img-fluid bdr position-absolute">
                <div class="container-fluid pl-0 pr-0">
                    <section
                        class="subscribeSecBlock filter-agen bgCover pt-xl-1 pb-xl-1 pt-lg-1 pb-lg-1 pt-md-1 pb-md-1 py-10 px-3"
                        style="background-image: url('<?php echo base_url();?>assets/images/n-bg.jpg')">
                        <header class="col-12 mainHeader mb-0 text-center">
                            <h1 class="headingIV playfair fwEblod mb-4"><?php echo $page->title ?></h1>
                            <span class="headerBorder d-block mb-5"><img
                                    src="<?php echo base_url();?>assets/images/hbdr.png" alt="Header Border"
                                    class="img-fluid"></span>
                        </header>

                        <div class="row justify-content-center sitemap">
                            <div class="sitemap-item col-xs-12 col-md-4 col-lg-4">
                                <h4>About Support88</h4>
                                <p></p>
                                <p><a href="<?= site_url('agen') ?>" class="text-green"><?= $agent->title_menu ?></a></p>
                                <p><a href="<?= site_url('blog') ?>" class="text-green"><?= $blog->title_menu ?></a></p>
                                <p><a href="<?= site_url('event') ?>" class="text-green"><?= $event->title_menu ?></a></p>
                                <p><a href="<?= site_url('faq') ?>" class="text-green"><?= $faq->title_menu ?></a></p>
                                <p><a href="<?= site_url('tentang-kami') ?>" class="text-green"><?= $about_us->title_menu ?></a>
                                </p>
                                <p><a href="<?= site_url('kontak-kami') ?>" class="text-green"><?= $contact_us->title_menu ?></a>
                                </p>
                            </div>
                            <div class="sitemap-item col-xs-12 col-md-4 col-lg-4">
                                <h4>Agen Support88</h4>
                                <p></p>
                                <?php foreach ($sitemap as $site) { ?>
                                <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province)) ?>" class="text-green"><?= $site->province ?></a>
                                </p>
                                <?php } ?>
                            </div>
                            <?php foreach ($sitemap as $site) { ?>
                            <div class="sitemap-item col-xs-12 col-md-4 col-lg-4">
                                <h4>Agen Wilayah <?= $site->province ?></h4>
                                <p></p>
                                <?php foreach ($site->kota_nya as $kota) { ?>
                                <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province).'/'.$this->main->slug_url($kota->city_name)) ?>" class="text-green"><?= $kota->city_name ?></a>
                                </p>
                                <?php } ?>
                            </div>

                            <?php } ?>

                            <?php foreach ($sitemap as $site) { ?>
                            <?php foreach ($site->kota_nya as $kota) { ?>
                            <div class="sitemap-item col-xs-12 col-md-4 col-lg-4">
                                <h4>Agen Wilayah <?= $kota->city_name ?></h4>
                                <p></p>
                                <?php foreach ($kota->kecamatannya as $kecamatan) { ?>
                                <p><a href="<?= site_url('agen/'.$this->main->slug_url($site->province).'/'.$this->main->slug_url($kota->city_name).'/'.$this->main->slug_url($kecamatan->subdistrict_name)) ?>" class="text-green"><?= $kecamatan->subdistrict_name ?></a>
                                </p>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>