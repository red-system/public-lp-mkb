<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
	style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
				<h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title_menu ?></h1>
				<ul class="list-unstyled breadCrumbs d-flex justify-content-center">
					<li class="mr-2"><a href="<?php echo site_url() ?>"
							title="<?= $home->title_menu ? $home->title_menu : 'beranda' ?>"><?php echo $home->title_menu ?></a>
					</li>
					<li class="mr-2">/</li>
					<li class="active"><?php echo $page->title_menu ?></li>
				</ul>
			</div>
		</div>
	</div>
</section>


<section class="contactSecBlock container pb-xl-24 pb-lg-10 pb-md-8 py-10">
	<div class="row">
		<header class="col-12 mainHeader text-center">
			<h1 class="headingIV playfair fwEblod mb-7">Get In Touch</h1>
			<p>Lorem ipsum dolor consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore
				magna<br class="d-block"> aliquam erat volutpatcommodo consequat.</p>
			<ul class="list-unstyled contactListHolder mt-3 mb-0 d-flex flex-wrap text-center">

				<?php if (!empty($contact_info['address'])) { ?>
				<li class="mb-lg-0 mb-6">
					<span class="icon d-block mx-auto bg-lightGray py-4 mb-4"><i
							class="fas fa-map-marker-alt"></i></span>
					<strong class="title text-uppercase playfair mb-5 d-block"><?= $dict_address ?></strong>
					<address class="mb-0"><a href="<?php echo $contact_info['address_link'] ?>"
							title="<?= $dict_address ? $dict_address : 'alamat' ?>"><?php echo $contact_info['address']; ?></a>
					</address>
				</li>
				<?php } ?>

				<?php if (!empty($contact_info['phone'])) { ?>
				<li class="mb-lg-0 mb-6">
					<span class="icon d-block mx-auto bg-lightGray py-4 mb-3"><i class="fas fa-phone"></i></span>
					<strong class="title text-uppercase playfair mb-5 d-block"><?= $dict_telephone ?></strong>
					<a href="<?php echo $contact_info['phone_link'] ?>"
						title="<?= $dict_telephone ? $dict_telephone : 'telephone' ?>"
						class="d-block"><?php echo $contact_info['phone']; ?></a>
				</li>
				<?php } ?>

				<?php if (!empty($contact_info['email'])) { ?>
				<li class="mb-lg-0 mb-6">
					<span class="icon d-block mx-auto bg-lightGray py-5 mb-3"><i class="fas fa-envelope"></i></span>
					<strong class="title text-uppercase playfair mb-5 d-block"><?= $dict_email ?></strong>
					<a href="<?php echo $contact_info['email_link'] ?>"
						title="<?= $dict_email ? $dict_email : 'email' ?>"
						class="d-block"><?php echo $contact_info['email']; ?></a>
				</li>
				<?php } ?>
			</ul>
		</header>
	</div>
	<div class="row mt-15">
		<div class="col-12">
			<form action="<?php echo site_url('kontak-kami/send') ?>" method="post" class="contactForm" id="contact-form">
				<div class="d-flex flex-wrap row1 mb-md-1">
					<div class="form-group coll mb-5">
						<input type="text" id="name" class="form-control" name="name" placeholder="Nama anda ...  *" required>
					</div>
					<div class="form-group coll mb-5">
						<input type="email" class="form-control" id="email" name="email"
							placeholder="Email anda ...  *" required>
					</div>
					<div class="form-group coll mb-5">
						<input type="tel" class="form-control" id="tel" name="phone" placeholder="Nomor telepon anda ... *" required>
					</div>
				</div>
				<div class="form-group w-100 mb-6">
					<textarea class="form-control" name="message" placeholder="Pesan anda ...  *" required></textarea>
				</div>
				<div class="d-flex flex-wrap row1 mb-md-1">
				<div class="form-group coll mb-5">
					<?php echo $captcha ?>
					<input type="text" class="form-control mt-3" name="captcha" placeholder="Captcha *" required>
				</div>
				</div>
				<div class="text-center">
					<button type="submit"
						class="btn btnTheme btnShop md-round fwEbold text-white py-3 px-4 py-md-3 px-md-4"><?= $dict_message_send ?></button>
				</div>
			</form>
		</div>
	</div>
</section>

<!-- mapHolder -->
<div class="mapHolder">
	<iframe
		src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.91477127143!2d-74.11976341808828!3d40.697403441901386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2s!4v1573223498837!5m2!1sen!2s"
		style="border:0;" allowfullscreen="">
	</iframe>
</div>