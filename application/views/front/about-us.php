<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
	style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
				<h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
				<ul class="list-unstyled breadCrumbs d-flex justify-content-center">
					<li class="mr-2"><a href="<?php echo site_url() ?>"
							title="<?php echo $home->title_menu ?>"><?php echo $home->title_menu ?></a></li>
					<li class="mr-2">/</li>
					<li class="active"><?php echo $page->title_menu ?></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="abtSecHolder container pt-xl-24 pb-xl-12 pt-lg-20 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-5">
	<div class="row">
		<div class="col-12 col-lg-6 pt-xl-0 pt-lg-0">
			<h2 class="playfair fwEbold position-relative mb-7 pb-5">
				<!-- <strong class="d-block">A Minimal Team</strong> -->
				<strong class="d-block"><?php echo $page->title ?></strong>
			</h2>
			<p class="pr-xl-16 pr-lg-10 mb-lg-0 mb-6"><?php echo $page->description; ?></p>
		</div>
		<div class="col-12 col-lg-6">
			<img src="<?php echo base_url();?>assets/images/img80.jpg" alt="image description" class="img-fluid">
		</div>
	</div>
</section>
<section class="counterSec container pt-xl-12 pb-xl-10 pt-lg-10 pb-lg-10 pt-md-8 pb-md-10 pt-5 pb-10">
	<div class="row">
		<div class="col-12">
			<!-- progressCounter -->
			<ul class="progressCounter list-unstyled mb-2 d-flex flex-wrap text-capitalize text-center">
				<li class="mb-md-0 mb-3">
					<strong class="d-block fwEbold counter mb-2">229</strong>
					<strong class="d-block text-uppercase txtWrap">Happy Clients</strong>
				</li>
				<li class="mb-md-0 mb-3">
					<strong class="d-block fwEbold counter mb-2">109</strong>
					<strong class="d-block text-uppercase txtWrap">completed project</strong>
				</li>
				<!-- <li class="mb-md-0 mb-3">
					<strong class="d-block fwEbold counter mb-2">22</strong>
					<strong class="d-block text-uppercase txtWrap">awesome staff</strong>
				</li>
				<li class="mb-md-0 mb-3">
					<strong class="d-block fwEbold counter mb-2">11</strong>
					<strong class="d-block text-uppercase txtWrap">winning awards</strong>
				</li> -->
			</ul>
		</div>
	</div>
</section>

<section class="processStepSec container pt-xl-10 pb-xl-10 pt-lg-10 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-0">
	<div class="row">
		<header class="col-12 mainHeader mb-3 text-center">
			<h1 class="headingIV playfair fwEblod mb-4">Delivery Process</h1>
			<span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
					alt="Header Border" class="img-fluid img-bdr"></span>
		</header>
	</div>
	<div class="row">
		<div class="col-12 pl-xl-23 mb-lg-3 mb-10">
			<div class="stepCol position-relative bg-lightGray py-6 px-6">
				<strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 01 </strong>
				<h2 class="headingV fwEblod text-uppercase mb-3">Choose your products</h2>
				<p class="mb-5">There are many variations of passages of lorem ipsum available, but the majority have
					suffered alteration in some form, by injected humour. Both betanin</p>
			</div>
		</div>
		<div class="col-12 pr-xl-23 mb-lg-3 mb-10">
			<div class="stepCol rightArrow position-relative bg-lightGray py-6 px-6 float-right">
				<strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 02</strong>
				<h2 class="headingV fwEblod text-uppercase mb-3">Connect nearest stored</h2>
				<p class="mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
					has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
		</div>
		<div class="col-12 pl-xl-23 mb-lg-3 mb-10">
			<div class="stepCol position-relative bg-lightGray py-6 px-6">
				<strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 03</strong>
				<h2 class="headingV fwEblod text-uppercase mb-3">Share your location</h2>
				<p class="mb-5">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
					laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore</p>
			</div>
		</div>
		<div class="col-12 pr-xl-23 mb-lg-3 mb-10">
			<div class="stepCol rightArrow position-relative bg-lightGray py-6 px-6 float-right">
				<strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 04</strong>
				<h2 class="headingV fwEblod text-uppercase mb-3">Get delivered fast</h2>
				<p class="mb-5">On the other hand, we denounce with righteous indignation and dislike men who are so
					beguiled and demoralized by the charms of pleasure of the moment.</p>
			</div>
		</div>
	</div>
</section>

<section class="teamSec pt-xl-12 pb-xl-0 pt-lg-10 pb-lg-0 pt-md-8 pb-md-16 pt-0 pb-4">
	<div class="container">
		<div class="row">
			<header class="col-12 mainHeader mb-9 text-center">
				<h1 class="headingIV playfair fwEblod mb-4"><?php echo $testimonial_page->title; ?></h1>
				<span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
						alt="Header Border" class="img-fluid img-bdr"></span>
			</header>
		</div>
		<div class="row">
			<?php foreach($testimonial as $testi) { ?>
			<div class="col-12 col-sm-6 col-lg-4 mb-lg-0 mb-6">
				<article class="teamBlock overflow-hidden">
					<span class="imgWrap position-relative d-block w-100 mb-4">
						<img src="<?php echo base_url();?>assets/images/img82.jpg" class="img-fluid"
							alt="image description">
						<!-- <img src="<?php echo $this->main->image_preview_url($testi->thumbnail) ?>" title="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title; ?>" alt="<?php echo $testi->thumbnail_alt ? $testi->thumbnail_alt : $testi->title ?>"> -->
					</span>
					<div class="textDetail w-100 text-center">
						<h3>
							<strong class="text-uppercase d-block fwEbold name mb-2"><a
									href="javascript:void(0);"><?php echo $testi->title; ?></a></strong>
							<strong class="text-capitalize d-block desination"><?php echo $testi->title_sub; ?></strong>
						</h3>

						<p class="mb-xl-14 mb-lg-10">“<?php echo $testi->description ?>”</p>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</div>
</section>