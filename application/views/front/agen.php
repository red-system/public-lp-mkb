<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title_menu ?></h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="<?php echo site_url() ?>"
                            title="<?php echo $home->title_menu; ?>"><?php echo $home->title_menu ?></a></li>
                    <li class="mr-2">/</li>
                    <li class="active"><?php echo $page->title_menu ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- latestSec -->
<section class="discoverSecHolder container pt-xl-19 pb-xl-12 pt-lg-20 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-5">
    <div class="row rightAlign">
        <div class="col-12 col-lg-12 mb-lg-0 mb-4">
            <div class="imgHolder position-relative">
                <img src="<?php echo base_url();?>assets/images/b-bdr.png" alt="image description"
                    class="img-fluid bdr position-absolute">
                <div class="container-fluid pl-0 pr-0">
                    <section
                        class="subscribeSecBlock bgCover pt-xl-1 pb-xl-1 pt-lg-1 pb-lg-1 pt-md-1 pb-md-1 py-10 px-3"
                        style="background-image: url('<?php echo base_url();?>assets/images/n-bg.jpg')">

                        <header class="col-12 mainHeader mb-0 text-center">
                            <h1 class="headingIV playfair fwEblod mb-4"><?php echo $dict_filter_agen; ?></h1>
                            <span class="headerBorder d-block mb-5"><img
                                    src="<?php echo base_url();?>assets/images/hbdr.png" alt="Header Border"
                                    class="img-fluid"></span>
                        </header>

                        <form>
                            <div class="row filter-agen">
                                <input type="text" class="hide" id="pre-define-province"
                                    value="<?= $province_name ? $province_name : null; ?>">
                                <input type="text" class="hide" id="pre-define-city"
                                    value="<?= $city_name ? $city_name : null; ?>">
                                <input type="text" class="hide" id="pre-define-subdistrict"
                                    value="<?= $subdistrict_name ? $subdistrict_name : null; ?>">

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label>Provinsi</label>
                                        <select name="filter-agent-province" id="filter-agent-province"
                                            class="form-control select2" data-placeholder="Pilih Provinsi"></select>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group hide">
                                        <label>Kabupaten</label>
                                        <select name="filter-agent-city" id="filter-agent-city"
                                            class="form-control select2" data-placeholder="Pilih Kabupaten"></select>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group hide">
                                        <label>Kecamatan</label>
                                        <select name="filter-agent-area" id="filter-agent-area"
                                            class="form-control select2" data-placeholder="Pilih Kecamatan"></select>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="faq-accordion agent-accordion">
                            <div id="zero-data" class="hide">
                                <h4 class="text-center"><?= $dict_agent_report_none ?></h4>
                            </div>
                            <div class="accrodion-grp hide" id="template-agent-nonactive" data-grp-name="faq-accrodion">
                                <div class="accrodion card mb-2">
                                    <div class="accrodion-inner card-header p-0 m-0">
                                        <div class="accrodion-title p-3" style="cursor:pointer">
                                            <h4 class="agent-name d-inline"></h4><i
                                                class="fas fa-angle-double-down d-inline float-right p-2"></i>
                                        </div>
                                        <div class="accrodion-content bg-white ml-0">
                                            <div class="card-body">
                                                <span><?= $dict_agent_list_address ?> </span><br>
                                                <p class="agent-address"></p><br>
                                                <p><?= $dict_agent_list_whatsapp ?> <a href="" class="agent-whatsapp"
                                                        title="Whatsapp agent"></a></p>
                                                <p><?= $dict_agent_list_telephone ?> <a href="" class="agent-phone"
                                                        title="Phone agent"></a></p>
                                                <p><?= $dict_agent_list_email ?> <a href="" class="agent-email"
                                                        title="Email agent"></a></p>
                                                <br>
                                                <strong><a href="https://goo.gl/maps/yw8U92kcvQ4ER7PEA" target="_blank"
                                                        class="mr-3 agent-maps bold" title="Link Google Maps">Google
                                                        Maps</a></strong>
                                                <strong><a href="" target="_blank" class="mr-3 agent-tokopedia"
                                                        title="Link Tokopedia">Tokopedia</a></strong>
                                                <strong><a href="" target="_blank" class="agent-shopee"
                                                        title="Link Shopee">Shopee</a></strong>
                                                <strong><a href="" target="_blank"
                                                        class="agent-detail main-btn main-btn-2 float-xl-right mt-xs-3 mt-sm-3 mt-lg-3 mt-xl-0"
                                                        title="Link Detail"><?= $dict_agent_list_detail ?> <i
                                                            class="fas fa-angle-right"></i> </a></strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>