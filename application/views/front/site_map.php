<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.dv3.css">
    <style>
        @font-face {
            font-family: "BrownStd";
            font-style: normal;
            font-weight: normal;
            src: url('<?php echo BASEPATH."assets/fonts/brownstd/BrownStdRegular.otf"?>') format('truetype');

        }
    </style>
</head>

<body>
<div class="container p-0" style="overflow-x:hidden">
    <div class="row m-0 d-flex justify-content-center">
        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 bg-white p-0">

            <!-- section banner -->
            <section class=" pt-5">
                <div class="px-4">
                    <img class="d-block mx-auto" width="130"
                         src="<?php echo base_url();?>assets/images/logo_mkb_hb.png"
                         alt="Logo">
                </div>
            </section>

            <!-- section description -->
            <section class="sec-desc pt-5 px-5" >
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center py-4" id="sec-desc">
                        <h5><b>My Kind of Beauty</b></h5>
                        <p class="desc-p pt-3">
                            Distributor Resmi Minyak Kemiri Bakar Bali
                        </p>
                        <img class="desc-accent"
                             src="<?php echo base_url("assets/images/assetsv2/subtraction_3_crop.png") ?>">
                        <div class="link-container">
                            <a class="btn btn-link-mkb d-block mx-auto" href="https://wa.me/6281337663109?text=Halo%20My%20Kind%20of%20Beauty.%20Saya%20tertarik%20dengan%20produk%20MKB%E2%80%A6"><i class="fa fa-whatsapp"></i> WhatsApp</a>
                            <a class="btn btn-link-mkb d-block mx-auto" href="https://t.me/MKB_HealthandBeauty"><i class="fa fa-telegram"></i> Telegram</a>
                            <a class="btn btn-link-mkb d-block mx-auto" href="https://mykindofbeauty.co.id"><i class="fa fa-globe"></i> Website</a>
                            <a class="btn btn-link-mkb d-block mx-auto" href="https://app.mykindofbeauty.co.id/registrasi-superreseller"><i class="fa fa-user-circle"></i> Daftar Super Reseller</a>
                            <a class="btn btn-link-mkb d-block mx-auto" href="https://mykindofbeauty.co.id/gabung"><i class="fa fa-user"></i> Daftar Reseller</a>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>



</body>

</html>