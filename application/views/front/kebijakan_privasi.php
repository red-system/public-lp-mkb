<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.dv2.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.theme.default.min.css">

    <style>
        @font-face {
            font-family: "BrownStd";
            font-style: normal;
            font-weight: normal;
            src: url('<?php echo BASEPATH."assets/fonts/brownstd/BrownStdRegular.otf"?>') format('truetype');

        }
    </style>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1371133559966403');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1371133559966403&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

</head>

<body>
<div class="container p-0" style="overflow-x:hidden">
    <div class="row m-0 d-flex justify-content-center">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 bg-white p-0">
            <section class=" pt-5">
                <div class="px-4">
                    <img class="d-block mx-auto" width="130"
                         src="<?php echo base_url();?>assets/images/logo_mkb_hb.png"
                         alt="Logo">
                </div>
            </section>
            <section class=" pt-5">
                <div class="px-4">

                    <h4>Kebijakan Privasi</h4>
                    <p>Website My Kind Of Beauty dimiliki oleh CV. AMERTHA SEDANA SARI, yang akan menjadi pengontrol atas data pribadi Anda.</p>
                    <p>Kami telah mengadopsi Kebijakan Privasi ini untuk menjelaskan bagaimana kami memproses informasi yang dikumpulkan oleh My Kind Of Beauty, yang juga menjelaskan alasan mengapa kami perlu mengumpulkan data pribadi tertentu tentang Anda. Oleh karena itu, Anda harus membaca Kebijakan Privasi ini sebelum menggunakan website My Kind Of Beauty.</p>
                    <p>Kami menjaga data pribadi Anda dan berjanji untuk menjamin kerahasiaan dan keamanannya.</p>
                    <h4>Informasi pribadi yang kami kumpulkan:</h4>
                    <p>Saat Anda mengunjungi My Kind Of Beauty, kami secara otomatis mengumpulkan informasi tertentu mengenai perangkat Anda, termasuk informasi tentang browser web, alamat IP, zona waktu, dan sejumlah cookie yang terinstal di perangkat Anda. Selain itu, selama Anda menjelajahi Website, kami mengumpulkan informasi tentang setiap halaman web atau produk yang Anda lihat, website atau istilah pencarian apa yang mengarahkan Anda ke Website, dan cara Anda berinteraksi dengan Website. Kami menyebut informasi yang dikumpulkan secara otomatis ini sebagai "Informasi Perangkat". Kemudian, kami mungkin akan mengumpulkan data pribadi yang Anda berikan kepada kami (termasuk tetapi tidak terbatas pada, Nama, Nama belakang, Alamat, informasi pembayaran, dll.) selama pendaftaran untuk dapat memenuhi perjanjian.</p>
                    <h4>Mengapa kami memproses data Anda?</h4>
                    <p>Menjaga data pelanggan agar tetap aman adalah prioritas utama kami. Oleh karena itu, kami hanya dapat memproses sejumlah kecil data pengguna, sebanyak yang benar-benar diperlukan untuk menjalankan website. Informasi yang dikumpulkan secara otomatis hanya digunakan untuk mengidentifikasi kemungkinan kasus penyalahgunaan dan menyusun informasi statistik terkait penggunaan website. Informasi statistik ini tidak digabungkan sedemikian rupa hingga dapat mengidentifikasi pengguna tertentu dari sistem.</p>
                    <p>Anda dapat mengunjungi website tanpa memberi tahu kami identitas Anda atau mengungkapkan informasi apa pun, yang dapat digunakan oleh seseorang untuk mengidentifikasi Anda sebagai individu tertentu yang dapat dikenali. Namun, jika Anda ingin menggunakan beberapa fitur website, atau Anda ingin menerima newsletter kami atau memberikan detail lainnya dengan mengisi formulir, Anda dapat memberikan data pribadi kepada kami, seperti email, nama depan, nama belakang, kota tempat tinggal, organisasi, dan nomor telepon Anda. Anda dapat memilih untuk tidak memberikan data pribadi Anda kepada kami, tetapi Anda mungkin tidak dapat memanfaatkan beberapa fitur website. Contohnya, Anda tidak akan dapat menerima Newsletter kami atau menghubungi kami secara langsung dari website. Pengguna yang tidak yakin tentang informasi yang wajib diberikan dapat menghubungi kami melalui info@mykindofbeauty.co.id.</p>
                    <h4>Hak-hak Anda:</h4>
                    <p>Jika Anda seorang warga Eropa, Anda memiliki hak-hak berikut terkait data pribadi Anda:</p>
                    <ul>
                        <li>Hak untuk mendapatkan penjelasan.</li>
                        <li>Hak atas akses.</li>
                        <li>Hak untuk memperbaiki.</li>
                        <li>Hak untuk menghapus.</li>
                        <li>Hak untuk membatasi pemrosesan.</li>
                        <li>Hak atas portabilitas data.</li>
                        <li>Hak untuk menolak.</li>
                        <li>Hak-hak terkait pengambilan keputusan dan pembuatan profil otomatis.</li>
                    </ul>
                    <p>Jika Anda ingin menggunakan hak ini, silakan hubungi kami melalui informasi kontak di bawah ini.</p>
                    <p>Selain itu, jika Anda seorang warga Eropa, perlu diketahui bahwa kami akan memproses informasi Anda untuk memenuhi kontrak yang mungkin kami miliki dengan Anda (misalnya, jika Anda melakukan pemesanan melalui Website), atau untuk memenuhi kepentingan bisnis sah kami seperti yang tercantum di atas. Di samping itu, harap diperhatikan bahwa informasi Anda mungkin dapat dikirim ke luar Eropa, termasuk Kanada dan Amerika Serikat.</p>
                    <h4>Link ke website lain:</h4>
                    <p>Website kami mungkin berisi tautan ke website lain yang tidak dimiliki atau dikendalikan oleh kami. Perlu diketahui bahwa kami tidak bertanggung jawab atas praktik privasi website lain atau pihak ketiga. Kami menyarankan Anda untuk selalu waspada ketika meninggalkan website kami dan membaca pernyataan privasi setiap website yang mungkin mengumpulkan informasi pribadi.</p>
                    <h4>Keamanan informasi:</h4>
                    <p>Kami menjaga keamanan informasi yang Anda berikan pada server komputer dalam lingkungan yang terkendali, aman, dan terlindungi dari akses, penggunaan, atau pengungkapan yang tidak sah. Kami menjaga pengamanan administratif, teknis, dan fisik yang wajar untuk perlindungan terhadap akses, penggunaan, modifikasi, dan pengungkapan tidak sah atas data pribadi dalam kendali dan pengawasannya. Namun, kami tidak menjamin tidak akan ada transmisi data melalui Internet atau jaringan nirkabel.</p>
                    <h4>Pengungkapan hukum:</h4>
                    <p>Kami akan mengungkapkan informasi apa pun yang kami kumpulkan, gunakan, atau terima jika diwajibkan atau diizinkan oleh hukum, misalnya untuk mematuhi panggilan pengadilan atau proses hukum serupa, dan jika kami percaya dengan itikad baik bahwa pengungkapan diperlukan untuk melindungi hak kami, melindungi keselamatan Anda atau keselamatan orang lain, menyelidiki penipuan, atau menanggapi permintaan dari pemerintah.</p>
                    <h4>Informasi kontak:</h4>
                    <p>Jika Anda ingin menghubungi kami untuk mempelajari Kebijakan ini lebih lanjut atau menanyakan masalah apa pun yang berkaitan dengan hak perorangan dan Informasi pribadi Anda, Anda dapat mengirim email ke info@mykindofbeauty.co.id.</p>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/owlcarousel/js/owl.carousel.min.js"></script>
<script>
    var myCollapsible = document.getElementById('myCollapsible')
    myCollapsible.addEventListener('hidden.bs.collapse', function () {
        // do something...
    })
</script>
</body>

</html>