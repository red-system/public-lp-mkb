<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="<?php echo site_url() ?>"
                            title="<?= $home->title_menu ? $home->title-menu : 'beranda' ?>"><?php echo $home->title_menu ?></a>
                    </li>
                    <li class="mr-2">/</li>
                    <li class="active"><?php echo $page->title_menu ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="introSec pt-xl-12 pb-xl-7 pt-10 pb-10">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 mb-lg-0 mb-6">
                <img src="<?php echo base_url();?>assets/images/img81.png" alt="image description" class="img-fluid">
            </div>
            <div class="col-12 col-lg-6">
                <div id="accordion" class="accordionList pt-lg-12">
                    <?php $data_1 = json_decode($page->data_1, TRUE); ?>
                    <?php foreach ($data_1['title'] as $key => $title) { ?>
                    <div class="card mb-2">
                        <div class="card-header px-xl-5 py-xl-3" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link fwEbold text-uppercase text-left w-100 p-0"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                    <?php echo $title ?> <i class="fas fa-sort-down float-right"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse <?php if ($key == 0) { echo 'show'; } ?>"
                            aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body px-xl-5 py-0">
                                <p class="mb-7"><?php echo $data_1['description'][$key] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section class="processStepSec container pt-xl-23 pb-xl-10 pt-lg-20 pb-lg-10 pt-md-16 pb-md-8 pt-10 pb-0">
    <div class="row">
        <header class="col-12 mainHeader mb-3 text-center">
            <h1 class="headingIV playfair fwEblod mb-4">Delivery Process</h1>
            <span class="headerBorder d-block mb-5"><img src="<?php echo base_url();?>assets/images/hbdr.png"
                    alt="Header Border" class="img-fluid img-bdr"></span>
        </header>
    </div>
    <div class="row">
        <div class="col-12 pl-xl-23 mb-lg-3 mb-10">
            <div class="stepCol position-relative bg-lightGray py-6 px-6">
                <strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 01</strong>
                <h2 class="headingV fwEblod text-uppercase mb-3">Choose your products</h2>
                <p class="mb-5">There are many variations of passages of lorem ipsum available, but the majority have
                    suffered alteration in some form, by injected humour. Both betanin</p>
            </div>
        </div>
        <div class="col-12 pr-xl-23 mb-lg-3 mb-10">
            <div class="stepCol rightArrow position-relative bg-lightGray py-6 px-6 float-right">
                <strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 02</strong>
                <h2 class="headingV fwEblod text-uppercase mb-3">Connect nearest stored</h2>
                <p class="mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
        </div>
        <div class="col-12 pl-xl-23 mb-lg-3 mb-10">
            <div class="stepCol position-relative bg-lightGray py-6 px-6">
                <strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 03</strong>
                <h2 class="headingV fwEblod text-uppercase mb-3">Share your location</h2>
                <p class="mb-5">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore</p>
            </div>
        </div>
        <div class="col-12 pr-xl-23 mb-lg-3 mb-10">
            <div class="stepCol rightArrow position-relative bg-lightGray py-6 px-6 float-right">
                <strong class="mainTitle text-uppercase mt-n8 mb-5 d-block text-center py-1 px-3">step 04</strong>
                <h2 class="headingV fwEblod text-uppercase mb-3">Get delivered fast</h2>
                <p class="mb-5">On the other hand, we denounce with righteous indignation and dislike men who are so
                    beguiled and demoralized by the charms of pleasure of the moment.</p>
            </div>
        </div>
    </div>
</section> -->