<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.dv2.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.theme.default.min.css">

    <style>
        @font-face {
            font-family: "BrownStd";
            font-style: normal;
            font-weight: normal;
            src: url('<?php echo BASEPATH."assets/fonts/brownstd/BrownStdRegular.otf"?>') format('truetype');

        }
    </style>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1371133559966403');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1371133559966403&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

</head>

<body>
<div class="container p-0" style="overflow-x:hidden">
    <div class="row m-0 d-flex justify-content-center">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 bg-white p-0">
            <section class=" pt-5">
                <div class="px-4">
                    <img class="d-block mx-auto" width="130"
                         src="<?php echo base_url();?>assets/images/logo_mkb_hb.png"
                         alt="Logo">
                </div>
            </section>
            <section class=" pt-5">
                <div class="px-4">
                    <h4>Syarat dan Ketentuan</h4>
                    <p>Selamat datang di My Kind Of Beauty!</p>
                    <p>Syarat dan ketentuan berikut menjelaskan peraturan dan ketentuan penggunaan Website CV. AMERTHA SEDANA SARI dengan alamat https://mykindofbeauty.co.id/.</p>
                    <p>Dengan mengakses website ini, kami menganggap Anda telah menyetujui syarat dan ketentuan ini. Jangan lanjutkan penggunaan My Kind Of Beauty jika Anda menolak untuk menyetujui semua syarat dan ketentuan yang tercantum di halaman ini.</p>
                    <h4>Cookie:</h4>
                    <p>Website ini menggunakan cookie untuk mempersonalisasi pengalaman online Anda. Dengan mengakses My Kind Of Beauty, Anda menyetujui penggunaan cookie yang diperlukan.</p>
                    <p>Cookie merupakan file teks yang ditempatkan pada hard disk Anda oleh server halaman web. Cookie tidak dapat digunakan untuk menjalankan program atau mengirimkan virus ke komputer Anda. Cookie yang diberikan telah disesuaikan untuk Anda dan hanya dapat dibaca oleh web server pada domain yang mengirimkan cookie tersebut kepada Anda.</p>
                    <p>Kami dapat menggunakan cookie untuk mengumpulkan, menyimpan, dan melacak informasi untuk keperluan statistik dan pemasaran untuk mengoperasikan website kami. Ada beberapa Cookie wajib yang diperlukan untuk pengoperasian website kami. Cookie ini tidak memerlukan persetujuan Anda karena akan selalu aktif. Perlu diketahui bahwa dengan menyetujui Cookie wajib, Anda juga menyetujui Cookie pihak ketiga, yang mungkin digunakan melalui layanan yang disediakan oleh pihak ketiga jika Anda menggunakan layanan tersebut di website kami, misalnya, jendela tampilan video yang disediakan oleh pihak ketiga dan terintegrasi dengan website kami.</p>
                    <h4>Lisensi:</h4>
                    <p>Kecuali dinyatakan lain, CV. AMERTHA SEDANA SARI dan/atau pemberi lisensinya memiliki hak kekayaan intelektual atas semua materi di My Kind Of Beauty. Semua hak kekayaan intelektual dilindungi undang-undang. Anda dapat mengaksesnya dari My Kind Of Beauty untuk penggunaan pribadi Anda sendiri dengan batasan yang diatur dalam syarat dan ketentuan ini.</p>
                    <p>Anda dilarang untuk:</p>
                    <ul>
                        <li>Menyalin atau menerbitkan ulang materi dari My Kind Of Beauty</li>
                        <li>Menjual, menyewakan, atau mensublisensikan materi dari My Kind Of Beauty</li>
                        <li>Memproduksi ulang, menggandakan, atau menyalin materi dari My Kind Of Beauty</li>
                        <li>Mendistribusikan ulang konten dari My Kind Of Beauty</li>
                    </ul>
                    <p>Perjanjian ini akan mulai berlaku pada tanggal perjanjian ini.</p>
                    <p>Beberapa bagian website ini menawarkan kesempatan bagi pengguna untuk memposting serta bertukar pendapat dan informasi di area website tertentu. CV. AMERTHA SEDANA SARI tidak akan memfilter, mengedit, memublikasikan, atau meninjau Komentar di hadapan pengguna di website. Komentar tidak mencerminkan pandangan dan pendapat CV. AMERTHA SEDANA SARI, agennya, dan/atau afiliasinya. Komentar mencerminkan pandangan dan pendapat individu yang memposting pandangan dan pendapatnya. Selama diizinkan oleh undang-undang yang berlaku, CV. AMERTHA SEDANA SARI tidak akan bertanggung jawab atas Komentar atau kewajiban, kerugian, atau pengeluaran yang disebabkan dan/atau ditanggung sebagai akibat dari penggunaan dan/atau penempatan dan/atau penayangan Komentar di website ini.</p>
                    <p>CV. AMERTHA SEDANA SARI berhak memantau semua Komentar dan menghapus Komentar apa pun yang dianggap tidak pantas, menyinggung, atau menyebabkan pelanggaran terhadap Syarat dan Ketentuan ini.</p>
                    <p>Anda menjamin dan menyatakan bahwa:</p>
                    <ul>
                        <li>Anda berhak memposting Komentar di website kami serta memiliki semua lisensi dan persetujuan yang diperlukan untuk melakukannya;</li>
                        <li>Komentar tidak melanggar hak kekayaan intelektual apa pun, termasuk tetapi tidak terbatas pada, hak cipta, paten, atau merek dagang pihak ketiga mana pun;</li>
                        <li>Komentar tidak mengandung materi yang bersifat memfitnah, mencemarkan nama baik, menyinggung, tidak senonoh, atau melanggar hukum, yang merupakan pelanggaran privasi.</li>
                        <li>Komentar tidak akan digunakan untuk membujuk atau mempromosikan bisnis atau kebiasaan atau memperkenalkan kegiatan komersial atau kegiatan yang melanggar hukum.</li>
                    </ul>
                    <p>Dengan ini Anda memberikan lisensi non-eksklusif kepada CV. AMERTHA SEDANA SARI untuk menggunakan, memproduksi ulang, mengedit, dan mengizinkan orang lain untuk menggunakan, memproduksi ulang, dan mengedit Komentar Anda dalam segala bentuk, format, atau media.</p>
                    <h4>Membuat hyperlink yang mengarah ke Konten kami:</h4>
                    <p>Organisasi berikut dapat membuat tautan menuju Website kami tanpa persetujuan tertulis sebelumnya:</p>
                    <ul>
                        <li>Lembaga pemerintah;</li>
                        <li>Mesin pencari;</li>
                        <li>Kantor berita;</li>
                        <li>Distributor direktori online dapat membuat tautan menuju Website kami dengan cara yang sama seperti membuat tautan ke Website bisnis terdaftar lainnya; dan</li>
                        <li>Bisnis Terakreditasi di Seluruh Sistem kecuali meminta organisasi nirlaba, pusat perbelanjaan amal, dan grup penggalangan dana amal yang mungkin tidak membuat hyperlink menuju Website kami.</li>
                    </ul>
                    <p>Organisasi-organisasi ini dapat menautkan ke halaman beranda, ke publikasi, atau ke informasi Website kami lainnya selama tautan tersebut: (a) tidak menipu dengan cara apa pun; (b) tidak menyiratkan secara keliru adanya hubungan sponsor, rekomendasi, atau persetujuan dari pihak yang menautkan beserta produk dan/atau layanannya; serta (c) sesuai dengan konteks website pihak yang menautkan.</p>
                    <p>Kami dapat mempertimbangkan dan menyetujui permintaan penautan lain dari jenis organisasi berikut:</p>
                    <ul>
                        <li>sumber informasi bisnis dan/atau konsumen yang sudah umum dikenal;</li>
                        <li>website komunitas dot.com;</li>
                        <li>asosiasi atau kelompok lain yang mewakili badan amal;</li>
                        <li>distributor direktori online;</li>
                        <li>portal internet;</li>
                        <li>firma akuntansi, hukum, dan konsultan; dan</li>
                        <li>lembaga pendidikan dan asosiasi dagang.</li>
                    </ul>
                    <p>Kami akan menyetujui permintaan penautan dari organisasi-organisasi tersebut jika kami memutuskan bahwa: (a) tautan tersebut tidak akan membuat kami terlihat merugikan kami sendiri atau bisnis terakreditasi kami; (b) organisasi tidak memiliki catatan negatif apa pun dengan kami; (c) keuntungan bagi kami dari keberadaan hyperlink mengkompensasi tidak adanya CV. AMERTHA SEDANA SARI; dan (d) tautan tersebut dalam konteks informasi sumber daya umum.</p>
                    <p>Organisasi-organisasi ini dapat menautkan ke halaman beranda kami selama tautan tersebut: (a) tidak menipu dengan cara apa pun; (b) tidak menyiratkan secara keliru adanya hubungan sponsor, rekomendasi, atau persetujuan dari pihak yang menautkan beserta produk dan/atau layanannya; dan (c) sesuai dengan konteks website pihak yang menautkan.</p>
                    <p>Jika Anda merupakan salah satu organisasi yang tercantum dalam paragraf 2 di atas dan tertarik untuk membuat tautan ke website kami, Anda harus memberitahu kami dengan mengirimkan email ke CV. AMERTHA SEDANA SARI. Harap cantumkan nama Anda, nama organisasi Anda, informasi kontak dan URL website Anda, daftar URL apa pun yang akan memuat tautan ke Website kami, serta daftar URL di website kami yang ingin Anda tautkan. Silakan tunggu tanggapan dari kami selama 2-3 minggu.</p>
                    <p>Organisasi yang telah disetujui dapat membuat hyperlink menuju Website kami seperti berikut:</p>
                    <ul>
                        <li>Dengan menggunakan nama perusahaan kami; atau</li>
                        <li>Dengan menggunakan Uniform Resource Locator yang ditautkan; atau</li>
                        <li>Dengan menggunakan deskripsi lain dari Website kami yang ditautkan yang masuk akal dalam konteks dan format konten di website pihak yang menautkan.</li>
                    </ul>
                    <p>Tidak ada penggunaan logo CV. AMERTHA SEDANA SARI atau karya seni lainnya yang diizinkan untuk menautkan perjanjian lisensi merek dagang.</p>
                    <h4>Tanggung jawab atas Konten:</h4>
                    <p>Kami tidak akan bertanggung jawab atas konten yang muncul di Website Anda. Anda setuju untuk melindungi dan membela kami terhadap semua klaim yang diajukan atas Website Anda. Tidak ada tautan yang muncul di Website mana pun yang dapat dianggap sebagai memfitnah, cabul, atau kriminal, atau yang menyalahi, atau melanggar, atau mendukung pelanggaran lain terhadap hak pihak ketiga.</p>
                    <h4>Pernyataan Kepemilikan Hak:</h4>
                    <p>Kami berhak meminta Anda menghapus semua tautan atau tautan tertentu yang menuju ke Website kami. Anda setuju untuk segera menghapus semua tautan ke Website kami sesuai permintaan. Kami juga berhak mengubah syarat ketentuan ini dan kebijakan penautannya kapan saja. Dengan terus menautkan ke Website kami, Anda setuju untuk terikat dan mematuhi syarat dan ketentuan penautan ini.</p>
                    <h4>Penghapusan tautan dari website kami:</h4>
                    <p>Jika Anda menemukan tautan di Website kami yang bersifat menyinggung karena alasan apa pun, Anda bebas menghubungi dan memberi tahu kami kapan saja. Kami akan mempertimbangkan permintaan untuk menghapus tautan, tetapi kami tidak berkewajiban untuk menanggapi Anda secara langsung.</p>
                    <p>Kami tidak memastikan bahwa informasi di website ini benar. Kami tidak menjamin kelengkapan atau keakuratannya, dan kami juga tidak berjanji untuk memastikan bahwa website tetap tersedia atau materi di website selalu diperbarui.</p>
                    <h4>Penolakan:</h4>
                    <p>Sejauh diizinkan oleh undang-undang yang berlaku, kami mengecualikan semua representasi, jaminan, dan ketentuan yang berkaitan dengan website kami dan penggunaan website ini. Tidak ada bagian dari penolakan ini yang akan:</p>
                    <ul>
                        <li>membatasi atau mengecualikan tanggung jawab kami atau Anda terhadap kematian atau cedera pribadi;</li>
                        <li>membatasi atau mengecualikan tanggung jawab kami atau Anda terhadap penipuan atau pemberian keterangan yang tidak benar;</li>
                        <li>membatasi tanggung jawab kami atau Anda dengan cara apa pun yang tidak diizinkan oleh undang-undang yang berlaku; atau</li>
                        <li>mengecualikan tanggung jawab kami atau Anda yang tidak dapat dikecualikan berdasarkan undang-undang yang berlaku.</li>
                    </ul>
                    <p>Batasan dan pengecualian tanggung jawab yang diatur dalam Bagian ini dan bagian lain dalam penolakan ini: (a) tunduk pada paragraf sebelumnya; dan (b) mengatur semua kewajiban yang timbul di bawah penolakan, termasuk kewajiban yang timbul dalam kontrak, dalam gugatan, dan untuk pelanggaran kewajiban hukum.</p>
                    <p>Selama website dan informasi serta layanan di website disediakan secara gratis, kami tidak akan bertanggung jawab atas kerugian atau kerusakan apa pun.</p>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/owlcarousel/js/owl.carousel.min.js"></script>
<script>
    var myCollapsible = document.getElementById('myCollapsible')
    myCollapsible.addEventListener('hidden.bs.collapse', function () {
        // do something...
    })
</script>
</body>

</html>