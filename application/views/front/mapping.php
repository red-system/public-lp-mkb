<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MKB Health and Beauty</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.d.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/css/select2.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/select2-bootstrap4-theme/select2-bootstrap4.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/loading.css">

<style>
    .text-sosmed{
        color: #22433e;
        font-size:18px;
    }

    a:hover {
        color: #f0f037;
    }

    @media only screen and (max-width: 600px) {

    .text-sosmed {
        color: #22433e;
        font-size: 14px;
        text-decoration: none;
    }

    }
</style>
</head>

<body style="overflow-x:hidden">

    <div id="base_url" class="d-none"><?php echo base_url(); ?></div>
    <div id="site_url" class="d-none"><?php echo site_url(); ?></div>

    <div class="row">
        <div class="col-lg-12 mt-3 mb-3 d-flex justify-content-center">
            <img width="200" src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                alt="Logo">
        </div>

        <div class="col-lg-12">
            <input type="text" class="d-none" id="lat_val" value="-1.035721">
            <input type="text" class="d-none" id="long_val" value="118.436931">
            <input type="text" class="d-none" id="zoom_val" value="5">
        </div>
    </div>

    <!-- <div class="row p-4">
        <div class="col-lg-4">
            <select name="provinces" id="province" class="select2bs4 form-control">
                <option value="">Pilih Provinsi</option>
                <?php foreach ($provinces as $province) { ?>
                <option value="<?= $province->province_id ?>"><?= $province->province ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-lg-4 py-lg-0 py-1">
            <select name="citys" id="city" class="select2bs4 form-control">
                <option value="">Pilih Kabupaten</option>
            </select>
        </div>
        <div class="col-lg-4">
            <select name="subdistricts" id="subdistrict" class="select2bs4 form-control">
                <option value="">Pilih Kecamatan</option>
            </select>
        </div>
    </div> -->


    <div id="show_maps" style="width:100%;height:70%;"></div>


    <div class="row pt-lg-4 p-3 mt-lg-0">
    <!-- <div class="row p-4"> -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
            <a href="https://api.whatsapp.com/send?phone=6281337663109&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB"
                target="_blank" class="text-sosmed mr-4"><i class="fa fa-whatsapp"></i> 081 337 663 109</a>
            <a href="https://www.facebook.com/Mkbhealthandbeauty-101394608499775" class="text-sosmed"><i
                    class="fa fa-facebook pt-1"></i></a>
            &nbsp;
            <a href="https://www.instagram.com/mkb.healthandbeauty" class="text-sosmed"><i
                    class="fa fa-instagram pt-1"></i></a>
            <a href="https://www.instagram.com/mkb.healthandbeauty" class="text-sosmed"><span
                    class="pl-1">mkb.healthandbeauty</span></a>
        </div>

    </div>

    <div class='container-loading d-none'>
        <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
            <div class='loader--desc'></div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/select2/js/select2.full.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/maps.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdIEp0TwCHgZBZWFqZ7NTjvPLrsobEKUU&callback=initMap"
        async defer></script>

    <script type="text/javascript">
        var base_url = $('#base_url').val();
        var locations = <?php echo $marker ?> ;
        var latitude, longitude, zoom;

        function initMap() {

            latitude = $('#lat_val').val();
            longitude = $('#long_val').val();
            zoom = $('#zoom_val').val();

            var maps = new google.maps.Map(document.getElementById('show_maps'), {
                center: {
                    lat: parseFloat(latitude),
                    lng: parseFloat(longitude),
                },
                zoom: parseInt(zoom)
            });

            var infowindow = new google.maps.InfoWindow(),
                marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: maps
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(maps, marker);
                        maps.setZoom(17);
                        maps.panTo(marker.getPosition());
                    }
                })(marker, i));
            }
        }
    </script>

</body>

</html>