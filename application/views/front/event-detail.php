<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
	style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
				<h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
				<ul class="list-unstyled breadCrumbs d-flex justify-content-center">
					<li class="mr-2"><a href="<?php echo site_url() ?>"
							title="beranda"><?php echo $home->title_menu ?></a></li>
					<li class="mr-2">/</li>
					<li class="active"><?php echo $page->title_menu ?></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-10 pb-xl-17 pt-lg-10 pb-lg-4 pt-md-10 pb-md-2 pt-10">
	<div class="twoColumns container pt-xl-23 pb-xl-20 py-lg-20 py-md-16 py-10">
		<div class="row border-bottom mb-9">
			<div class="col-12 col-lg-12 order-lg-12">
				<!-- newsBlogColumn -->
				<div class="newsBlogColumn mb-9">
					<div class="imgHolder mb-6">
					<img src="<?php echo $this->main->image_preview_url($detail_event->thumbnail) ?>" alt="<?php echo $detail_event->thumbnail_alt ? $detail_event->thumbnail_alt : $detail_event->title ?>" title="<?= $detail_event->thumbnail_alt ? $detail_event->thumbnail_alt : $detail_event->title ?>" class="img-fluid w-100">
					</div>
					<div class="textHolder d-flex align-items-start mb-1">
						<time class="time text-center text-uppercase py-sm-3 py-1 px-1" datetime="2019-02-03 20:00">
							<span class="fwEbold d-block mb-1"><?php echo date('d M Y', strtotime($detail_event->created_at)) ?></span></time>
						<div class="alignLeft pl-6 w-100">
							<h2 class="headingV fwEbold mb-2"><?php echo $detail_event->title ?></h2>
							<!-- <span class="postBy d-block pb-6 mb-3">Post by: Jane doe</span> -->
						</div>
					</div>
					<p class="mb-5"><?php echo $detail_event->description; ?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<!-- socialNetworkList -->
				<ul class="list-unstyled socialNetworkList d-flex flex-nowrap mb-5">
					<li class="text-uppercase mr-12">SHARE THIS POST:</li>								
					<li class="mr-4"><a href="<?php echo $this->main->share_link('facebook', $detail_event->title, site_url('event/'.$detail_event->category_slug.'/'.$detail_event->slug)) ?>" title="link share facebook" class="fab fa-facebook-f"></a></li>
					<li class="mr-4"><a href="<?php echo $this->main->share_link('twitter', $detail_event->title, site_url('event/'.$detail_event->category_slug.'/'.$detail_event->slug)) ?>" title="link share twitter" class="fab fa-twitter"></a></li>
				</ul>
			</div>
		</div>
		
	</div>
</section>