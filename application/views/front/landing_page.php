<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MKB Health and Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.d.css">

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1371133559966403');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1371133559966403&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

</head>

<body>
    <div class="container p-0" style="overflow-x:hidden">
        <div class="row m-0 justify-content-center">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 bg-white p-0">
                <div class="banner2 pt-5">
                    <div class="px-4">
                        <div class="row pt-5">
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-6">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="banner-text2 pt-5">
                                            Solusi Perawatan Rambut Alami.
                                        </div>
                                        <span class="banner-text3 pt-2">
                                            Aman untuk Anak-anak dan Bayi.
                                        </span>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pt-5">
                                        <!-- <a href="https://api.whatsapp.com/send?phone=6281337663109&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB" target="_blank" class="banner-btn">Beli Sekarang</a> -->

                                        <?php if ($reseller->tokopedia == null && $reseller->shopee == null) { ?>
                                            <a href="<?php echo $url ?>" class="banner-btn" id="btnBuyUp">Beli Sekarang</a>
                                        <?php }else{ ?>
                                            <span class="banner-btn" data-toggle="modal" data-target="#modalBuy" style="cursor:pointer">Beli Sekarang</span>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-6">
                                <img class="w-100 banner-produk"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new.png") ?>">
                            </div>
                            <div class="banner-accent2">
                                <img width="140"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page-new/half1.png") ?>">
                            </div>
                        </div>

                        <div class="row py-4 text-center">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <span class="banner-cta2 p-0">lihat lebih detail</span>
                                <br>
                                <a href="" class="">
                                    <img width="20"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/arrow.png") ?>">
                                </a>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                            </div>
                        </div>

                    </div>
                </div>

                <!-- section deskripsi -->
                <div class="row py-5 px-4">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-5 pr-3">
                        <img class="w-100 banner-produk"
                            src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_18.png") ?>">
                        <div class="banner-accent3">
                            <img width="140"
                                src="<?php echo base_url("assets/images/kemiri/landing-page-new/half2.png") ?>">
                        </div>
                    </div>

                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-7 pl-0" style="vertical-align: text-bottom">
                        <div class="text-desc pb-3">
                            <span class="">Terbuat dari bahan alami pilihan dan tanpa bahan kimia</span> melalui proses
                            produksi
                            dengan metode tradisional modern yang dijaga ketat dan higienis sehingga kualitas produksi
                            MKB
                            tetap terjaga.
                        </div>

                    </div>
                </div>

                <!-- section komposisi -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-komposisi pb-4">
                            <span class="komposisi-title">
                                Komposisi
                            </span>
                            <div class="row px-5 pt-4">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/kemiri.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Biji Kemiri <span>(<i>Aleurites moluccana</i>)</span>
                                        <li>Menumbuhkan rambut</li>
                                        <li>Mengatasi rambut rontok</li>
                                        <li>Mencegah timbulnya uban</li>
                                    </div>
                                </div>
                            </div>

                            <div class="row px-5">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/minyak_kelapa.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Minyak Kelapa <span>(<i>Cocos nucifera</i>)</span>
                                        <li>Melembabkan rambut</li>
                                        <li>Memperbaiki rambut rusak</li>
                                        <li>Menangani rambut ketombe</li>
                                    </div>
                                </div>
                            </div>

                            <div class="row px-5">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/seledri.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Daun Seledri <span>(<i>Apium graveolens</i>)</span>
                                        <li>Membuat rambut berkilau</li>
                                        <li>Meredakan ketombe</li>
                                        <li>Kaya kandungan vitamin & mineral</li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- section manfaat -->
                <!-- <div class="row pt-5">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <img class="w-100" src="<?php echo base_url("assets/images/kemiri/landing-page-new/manfaat.png") ?>">
                    </div>
                </div> -->

                <div class="row pt-5 px-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                        <span class="howto-title d-flex justify-content-center mb-4">
                            Manfaat Minyak Kemiri Bakar
                        </span>
                        <!-- <span class="manfaat-title my-3">
                            Manfaat
                        </span> -->
                    </div>
                    <div
                        class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 d-flex justify-content-center align-items-center pr-0">
                        <img class="w-100"
                            src="<?php echo base_url("assets/images/kemiri/landing-page-new/botol-aksen2.png") ?>">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 d-flex align-items-center">
                        <div class="manfaat-desc">
                            <li>Melembabkan rambut dan kulit kepala</li>
                            <li>Mencegah rambut jadi kusut</li>
                            <li>Mencegah kerusakan rambut</li>
                            <li>Menebalkan dan memanjangkan rambut</li>
                            <li>Melembabkan rambut dan kulit kepala</li>
                            <li>Menumbuhkan rambut</li>
                            <li>Mengatasi rambut rontok</li>
                            <li>Memperkuat akar rambut</li>
                            <li>Menghitamkan rambut</li>
                            <li>Mengurangi peradangan akibat ketombe</li>
                            <li>Mencegah timbulnya uban</li>
                            <li>Menjaga kesehatan rambut</li>
                        </div>
                    </div>
                </div>

                <!-- section howto -->
                <div class="row py-5">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <span class="howto-title d-flex justify-content-center">
                            Bagaimana Cara Pakainya?
                        </span>
                        <div class="row px-5 pt-4">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                <img class="w-100"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_13.png") ?>">
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0 d-flex align-items-center">
                                <div class="howto-desc">
                                    <span>Dipakai sebelum keramas secara merata</span> ke seluruh bagian rambut sambil
                                    memijat.
                                </div>
                            </div>
                        </div>

                        <div class="row px-5">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0 d-flex align-items-center">
                                <div class="howto-desc pl-4">
                                    <span>Diamkan selama 30 menit - 2 jam.</span> Bisa menggunakan shower cap agar tidak
                                    menetes.
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                <img class="w-100"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_14.png") ?>">
                            </div>
                        </div>

                        <div class="row px-5">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                <img class="w-100"
                                    src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_15.png") ?>">
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0 d-flex align-items-center">
                                <div class="howto-desc">
                                    <span>Bilas 2x dengan shampoo </span>dan gunakan air hangat sampai bersih dan tidak
                                    berminyak.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- section barcode -->
                <div class="row mb-5">
                    <div class="col-12">
                        <div class="section-barcode">
                            <span class="barcode-title float-right">
                                MKB Aman!
                            </span>
                            <div class="pt-3">
                                <div class="barcode-desc px-5 py-5">
                                    Produk Minyak Kemiri Bakar dari MKB Health & Beauty sudah tersertifikasi dan
                                    terdaftar di <span>Badan Pengawas Obat dan Makanan (BPOM) Republik Indonesia.</span>
                                    Pabrik kami pun sudah berbadan hukum legal yaitu <span>UD. Amertha Sedana
                                        Sakti</span> dan didistribusikan oleh <span>CV Amertha Sedana Sari (MKB Health &
                                        Beauty).</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/barcode2.png") ?>">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <!-- CTA beli -->
                <div class="row pb-4">
                    <div class="col-12 d-flex justify-content-center">
                        <!-- <a href="https://api.whatsapp.com/send?phone=6281337663109&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB" target="_blank" class="buy-btn">Beli Sekarang</a> -->
                        
                        <?php if ($reseller->tokopedia == null && $reseller->shopee == null) { ?>
                            <a href="<?php echo $url ?>" class="buy-btn" id="btnBuyBottom">Beli Sekarang</a>
                        <?php }else{ ?>
                            <span class="buy-btn" data-toggle="modal" data-target="#modalBuy" style="cursor:pointer">Beli Sekarang</span>
                        <?php } ?>

                    </div>
                </div>

                <!-- section footer -->
                <footer class="d-flex mt-5">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                            <img class="w-100"
                                src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new.png") ?>"
                                style="bottom:0">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-7 pr-0 pl-0 pt-4 footer-description">
                            <img width="110"
                                src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                                alt="Logo">
                            <br>
                            <span class="company-name">CV. AMERTHA SEDANA SARI</span>
                            <div class="row m-0 pr-1 footer-contact">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-0">
                                    <div>
                                        <b>Office</b>
                                        <br>
                                        <a
                                            href="https://www.google.com/maps/place/MKB+Health+and+Beauty/@-8.6404531,115.2283771,15z/data=!4m5!3m4!1s0x0:0x28f52169c1dbb072!8m2!3d-8.6405313!4d115.228046">Jalan
                                            Ratna No.80 Tonja, Denpasar Utara, Kota Denpasar, Bali</a>
                                    </div>
                                    <div class="mt-3">
                                        <b>Office Hour</b>
                                        <br>
                                        <span>08.00 - 17.00 WITA</span>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12 pl-1 pr-0 pb-5">
                                    <a href="https://wa.me/"><i class="fa fa-whatsapp mr-1"></i>+62 813 376
                                        63109</a>
                                    <br>
                                    <a href="https://www.instagram.com/mkb.healthandbeauty"><i
                                            class="fa fa-instagram"></i><span
                                            class="pl-1">mkb.healthandbeauty</span></a>
                                    <br>
                                    <a href="https://www.facebook.com/Mkbhealthandbeauty-101394608499775"><i
                                            class="fa fa-facebook-square"></i><span
                                            class="pl-1">Mkbhealthandbeauty</span></a>
                                    <br>
                                    <a href=""><i class="fa fa-globe mr-0"></i> www.mykindofbeauty.co.id</a>
                                </div>
                                <div class="col-12 pl-0 mt-4 text-white">
                                    <span>
                                        <i class="fa fa-copyright"></i> mkbhealthandbeauty. 2021
                                    </span>
                                    <div class="py-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>


                <!-- Modal Buy Here -->
                <div class="modal fade" id="modalBuy" tabindex="-1" role="dialog" aria-labelledby="modalBuyLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content" style="border: 2px solid #22433e; border-radius:20px;">
                            <div class="modal-header"
                                style="border-bottom: 2px solid #22433e !important; background-color: #22433e !important; border-top-left-radius:15px !important; border-top-right-radius:15px !important; color:#FFF !important;">
                                <h5 class="modal-title" id="modalBuyLabel">Beli MKB disini!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="color:#FFF">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row px-2 py-2">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-1">
                                        <a href="<?= $url ?>" class="btn-cta-wa"><i class="fa fa-whatsapp"></i>
                                            Whatsapp</a>
                                    </div>

                                    <?php if ($reseller->shopee != null) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 my-1">
                                        <a href="<?= $reseller->shopee ?>" class="btn-cta-shopee">
                                            <img class="img-icon"
                                                src="<?php echo base_url("assets/images/kemiri/landing-page-new/icon-shopee.png") ?>">
                                            Shopee</a>
                                    </div>
                                    <?php } ?>

                                    <?php if ($reseller->tokopedia != null) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-1">
                                        <a href="<?= $reseller->tokopedia ?>" class="btn-cta-tokped">
                                            <img class="img-icon"
                                                src="<?php echo base_url("assets/images/kemiri/landing-page-new/icon-tokped.png") ?>">
                                            Tokopedia
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">

    $('#btnBuyUp').click(function() {
        fbq('trackCustom', 'CTA Beli Sekarang (atas)', {content_name: "Link to Reseller Page (from CTA upper)"});
    });

    $('#btnBuyBottom').click(function() {
        fbq('trackCustom', 'CTA Beli Sekarang (bawah)', {content_name: "Link to Reseller Page (from CTA bottom)"});
    });
    
    </script>

</body>

</html>