<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-25 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4">Not Found</h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="home.html">Beranda</a></li>
                    <li class="mr-2">/</li>
                    <li class="active">Not Found</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-23 pb-xl-17 pt-lg-20 pb-lg-4 pt-md-16 pb-md-2 pt-10">
    <div class="row">

        <h3 class="title">404</h3>
        <h5 class="mb-2 mb-sm-3"><?php echo $dict_404_title_1 ?></h5>
        <p><?php echo $dict_404_title_2 ?></p>
        <a href="<?php echo site_url() ?>" class="main-btn"
            title="<?php echo $dict_404_button_1 ?>"><?php echo $dict_404_button_1 ?><i
                class="fas fa-angle-right"></i></a>
    </div>
</section>