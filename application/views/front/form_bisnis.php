<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.dv4.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/loading.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/swal.min.css">
    <style>
        @font-face {
            font-family: "BrownStd";
            font-style: normal;
            font-weight: normal;
            src: url('<?php echo BASEPATH."assets/fonts/brownstd/BrownStdRegular.otf"?>') format('truetype');

        }
    </style>
</head>

<body>
<div class="container p-0" style="overflow-x:hidden">
    <input type="hidden" id="base_url" value="<?=base_url()?>">
    <div class="row m-0 d-flex justify-content-center">
        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 bg-white p-0">

            <!-- section banner -->
            <section class=" pt-5">
                <div class="px-4">
                    <img class="d-block mx-auto"
                         src="<?php echo base_url();?>assets/images/banner-3-.jpg"
                         alt="Logo" style="width: 100%">
                </div>
            </section>

            <!-- section description -->
            <section class="sec-desc pt-5 px-5" >
                <div class="container">
                    <form action="<?=base_url()?>form-bisnis/save" method="post" id="form-bisnis-submit">
                        <div class="mb-3">
                            <label for="inputForName" class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" id="inputForName" aria-describedby="nameHelp" required>
                        </div>
                        <div class="mb-3">
                            <label for="inputForEmail" class="form-label">Email</label>
                            <input type="email" class="form-control" name="email" id="inputForEmail" required>
                        </div>
                        <div class="mb-3">
                            <label for="inputForPhone" class="form-label">No HP</label>
                            <input type="text" class="form-control" name="phone" id="inputForPhone" required>
                        </div>
                        <div class="mb-3">
                            <label for="inputForProvince" class="form-label">Domisili</label>
                            <select class="form-control" name="province_id" id="inputForProvince" aria-label="Pilih Provinsi" required>
                                <option>Pilih Provinsi</option>
                                <?php
                                    foreach ($province as $key){
                                        ?>
                                        <option value="<?=$key->province_id?>"><?=$key->province?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="inputForKabupaten" class="form-label">Kabupaten/Kota</label>
                            <select class="form-control" name="city_id" id="inputForKabupaten" aria-label="Pilih Kabupaten/Kota" required>
                                <option>Pilih Kabupaten/Kota</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="inputForKecamatan" class="form-label">Kecamatan</label>
                            <select class="form-control" name="subdistrict_id" id="inputForKecamatan" aria-label="Pilih Kecamatan" required>
                                <option >Pilih Kecamatan</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mb-3" id="btn-submit">Submit</button>
                    </form>
                </div>
            </section>


        </div>
    </div>
</div>

</div>
<div class='container-loading d-none'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/swal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form_bisnis.js"></script>



</body>

</html>