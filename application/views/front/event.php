<!-- introBannerHolder -->
<section class="introBannerHolder d-flex w-100 bgCover mt-xl-26 mt-lg-21 mt-md-17 mt-15"
    style="background-image: url('<?php echo base_url();?>assets/images/b-bg7.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-10 pt-md-5 pt-sm-5 pt-6 text-center">
                <h1 class="headingIV fwEbold playfair mb-4"><?php echo $page->title ?></h1>
                <ul class="list-unstyled breadCrumbs d-flex justify-content-center">
                    <li class="mr-2"><a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a></li>
                    <li class="mr-2">/</li>
                    <li class="active"><?php echo $page->title_menu ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- latestSec -->
<section class="latestSec container overflow-hidden pt-xl-10 pb-xl-17 pt-lg-10 pb-lg-4 pt-md-10 pb-md-2 pt-10">
    <div class="row">
        <div class="col-12 mr-0 pr-0 ml-0 pl-0">
            <form action="<?php echo site_url('event') ?>" method="get">
                <div class="input-group mb-10 col-xl-4 col-lg-12 col-md-12 float-right">
                    <input class="form-control bg-white border" placeholder="Search ...">
                    <span class="input-group-append bg-white">
                        <button class="btn border" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
        </div>
        </form>
    </div>
    <div class="row">
        <?php foreach ($list as $event) { 
            $permalink = $this->main->permalink(array('event', $event->slug));
        ?>
        <div class="col-12 col-sm-6 col-lg-4">
            <!-- newsPostColumn -->
            <div class="newsPostColumn text-center px-2 pb-6 mb-6">
                <div class="imgHolder position-relative mb-6">
                    <a href="<?= $permalink ?>">
                        <img src="<?php echo $this->main->image_preview_url($event->thumbnail) ?>"
							alt="<?php echo $event->thumbnail_alt ? $event->thumbnail_alt : $event->title; ?>"
							title="<?= $event->thumbnail_alt ? $event->thumbnail_alt : $event->title ?>"
							class="img-fluid">
                        <time class="time text-uppercase position-absolute py-2 px-0" datetime="2019-02-03 20:00">
                            <span
                                class="fwEbold d-block"><?php echo date('d M Y', strtotime($event->created_at)) ?></span>
                        </time>
                    </a>
                </div>
                <h2 class="headingV fwEbold mb-2"><a href="<?= $permalink ?>"
                        title="<?= $event->title ? $event->title : 'link event' ?>"><?php echo $event->title ?></a></h2>
                <p class="mb-0">
                    <?php echo $this->main->short_desc($event->description); ?>
                </p>
            </div>
        </div>

        <?php } ?>

    </div>

    <div class="row">
		<div class="col-12 pt-3 mb-lg-0 mb-md-6 mb-3">
			<?php echo $this->pagination->create_links(); ?>
		</div>
	</div>
    
</section>