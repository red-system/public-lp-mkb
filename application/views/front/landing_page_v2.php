<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Kind Of Beauty</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.dv2.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/css/owl.theme.default.min.css">

    <style>
        @font-face {
        font-family: "BrownStd";
        font-style: normal;
        font-weight: normal;
        src: url('<?php echo BASEPATH."assets/fonts/brownstd/BrownStdRegular.otf"?>') format('truetype');

    }
    </style>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1371133559966403');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1371133559966403&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

</head>

<body>
    <div class="container p-0" style="overflow-x:hidden">
        <div class="row m-0 d-flex justify-content-center">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 bg-white p-0">

                <!-- section banner -->
                <section class="sec-banner pt-5">
                    <div class="px-4">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                <span class="banner-title">
                                    JAWABAN DARI RAHASIA <br>RAMBUT TEBAL TANPA RONTOK
                                </span>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                <p class="banner-subtitle mt-2">
                                    Untuk Meningkatkan Rasa Percaya <br>Diri & Tampil Awet Muda.
                                </p>
                            </div>
                            <div
                                class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center mt-2">
                                <img class="banner-pict"
                                    src="<?php echo base_url("assets/images/assetsv2/image_6_crop2.png") ?>">
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                <a href="#sec-desc" class="btn btn-block banner-cta col-xl-9 col-lg-9 col-md-9 col-sm-8 col-10">
                                    simak rahasianya disini
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- section description -->
                <section class="sec-desc pt-5 px-5" >
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center py-4" id="sec-desc">
                            <span class="desc-title">
                                Tahukah Kamu?
                            </span>
                            <p class="desc-p pt-3">
                                8 dari 10 orang di Indonesia mengalami botak keturunan yang disebabkan oleh kelebihan
                                hormon
                                DHT (penyebab utama rontok) dan siklus rambut yang tidak normal (penyebab utama rambut
                                tidak
                                tumbuh kembali).
                            </p>

                            <p class="desc-p pt-3">
                                kamu bisa menyelesaikan 2 masalah utama ini dengan menggunakan <span>Minyak Kemiri
                                    Bakar</span> untuk mengatasi rambut rontok tersebut. Kamu akan berhasil mengurangi
                                rontok dan menumbuhkan rambut baru seketika dan tentunya akan meningkatkan rasa percaya
                                dirimu di kemudian hari.
                            </p>
                            <img class="desc-accent"
                                src="<?php echo base_url("assets/images/assetsv2/subtraction_3_crop.png") ?>">
                        </div>
                    </div>
                </section>

                <!-- section solusi -->
                <section class="sec-solusi pt-2 px-5">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center py-4 ">
                            <span class="solusi-title">
                                Minyak Kemiri Bakar (MKB)
                            </span>
                            <br>
                            <span class="solusi-subtitle pt-2">
                                Solusi Rambut Rontok, <br>Rusak, Kusam, & Kusut.
                            </span>

                            <div class="row py-3">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img class="solusi-pict"
                                        src="<?php echo base_url("assets/images/assetsv2/group_100.png") ?>">
                                </div>
                            </div>

                            <p class="solusi-p pt-3">
                                Terbuat dari bahan alami pilihan dan tanpa bahan kimia melalui proses produksi dengan
                                metode
                                tradisional modern yang dijaga ketat dan higienis sehingga kualitas produksi Minyak
                                Kemiri
                                Bakar tetap terjaga.
                            </p>
                        </div>
                    </div>
                </section>

                <!-- section komposisi -->
                <section class="sec-komposisi pt-2">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4 ">
                            <div class="text-center">
                                <span class="komposisi-title">
                                    100% Komposisi Bahan Alami
                                </span>
                            </div>
                            <div class="mt-2 text-center px-3">
                                <span class="komposisi-subtitle">
                                    sebuah formula yang diwariskan turun-temurun oleh nenek moyang nusantara dan berfungsi secara alami meningkatkan kualitas dan menyehatkan rambut sehingga aman untuk siapa saja, termasuk bayi dan ibu hamil.
                                </span>
                            </div>

                            <div class="row pt-4 ml-4">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/kemiri.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Biji Kemiri <span>(<i>Aleurites moluccana</i>)</span>
                                        <li>Menumbuhkan rambut</li>
                                        <li>Mengatasi rambut rontok</li>
                                        <li>Mencegah timbulnya uban</li>
                                    </div>
                                </div>
                            </div>

                            <div class="row ml-4">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/minyak_kelapa.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Minyak Kelapa <span>(<i>Cocos nucifera</i>)</span>
                                        <li>Melembabkan rambut</li>
                                        <li>Memperbaiki rambut rusak</li>
                                        <li>Menangani rambut ketombe</li>
                                    </div>
                                </div>
                            </div>

                            <div class="row ml-4">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 px-0">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/seledri.png") ?>">
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pl-0">
                                    <div class="komposisi-desc pt-4">
                                        Daun Seledri <span>(<i>Apium graveolens</i>)</span>
                                        <li>Membuat rambut berkilau</li>
                                        <li>Meredakan ketombe</li>
                                        <li>Kaya kandungan vitamin & mineral</li>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- section keunggulan -->
                <section class="sec-keunggulan pt-5 pb-5">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="text-center">
                                <span class="keunggulan-title">
                                    Keunggulan <br>Minyak Kemiri Bakar (MKB) <br>dari My Kind of Beauty
                                </span>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <img class="w-100 pt-3"
                                        src="<?php echo base_url("assets/images/assetsv2/ss.png") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="180" class="pt-5"
                                        src="<?php echo base_url("assets/images/assetsv2/group_83.png") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="140" class="pt-4"
                                        src="<?php echo base_url("assets/images/assetsv2/group_86.png") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="140" class="pt-4"
                                        src="<?php echo base_url("assets/images/assetsv2/group_105.png") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="160" class="pt-4"
                                        src="<?php echo base_url("assets/images/assetsv2/group_90.png") ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- section howto -->
                <section class="sec-howto pt-4 px-3">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4 ">
                            <div class="text-center">
                                <span class="howto-title">
                                    Bagaimana Cara Pakainya?
                                </span>
                            </div>

                            <div class="row pt-4">
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_13.png") ?>">
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-7 pl-0 d-flex align-items-center">
                                    <div class="howto-desc">
                                        Dipakai sebelum keramas secara merata ke seluruh bagian rambut
                                        sambil memijat.
                                    </div>
                                </div>
                            </div>

                            <div class="row pt-4">
                                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-7 pl-0 d-flex align-items-center">
                                    <div class="howto-desc pl-4">
                                        Diamkan selama 30 menit - 2 jam. Bisa menggunakan shower cap agar
                                        tidak menetes.
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_14.png") ?>">
                                </div>
                            </div>

                            <div class="row pt-4">
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                                    <img class="w-100"
                                        src="<?php echo base_url("assets/images/kemiri/landing-page-new/group_15.png") ?>">
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-7 pl-0 d-flex align-items-center">
                                    <div class="howto-desc">
                                        Bilas 2x dengan shampoo dan gunakan air hangat sampai bersih dan
                                        tidak
                                        berminyak.
                                    </div>
                                </div>
                            </div>

                            <div class="text-center pt-4">
                                <span class="howto-title">
                                    Gampangkan cara pakainya? <br>Dapatkan Hasil Maksimal <br>Dengan Pemakaian Rutin
                                    <br>3x seminggu.
                                </span>
                            </div>

                            <div class="row pt-4 pb-5">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">

                                    <?php if ($reseller->tokopedia == null && $reseller->shopee == null) { ?>
                                    <a href="<?php echo $url ?>"
                                        class="btn btn-block cta-beli col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9"
                                        id="btnBuyUp">Beli Sekarang</a>
                                    <?php }else{ ?>
                                    <span class="btn btn-block cta-beli col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9"
                                        data-toggle="modal" data-target="#modalBuy" style="cursor:pointer">Beli
                                        Sekarang</span>
                                    <?php } ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- section testi -->
                <section class="sec-testi pt-4">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4 ">

                            <div class="text-center">
                                <span class="testi-title">
                                    REAL TESTIMONI <br>PEMAKAIAN MKB RUTIN
                                </span>
                            </div>

                            <div class="text-center">
                                <span class="testi-subtitle">
                                    sebelum - sesudah
                                </span>
                            </div>
                            <div>
                                <?php
                                    for ($i=1;$i<=8;$i++) {
                                        ?>

                                <div class="testi-content-scroll">
                                    <img src="<?php echo base_url("assets/images/assetsv2/testi/landingpage-0".$i.".jpg") ?>">
                                </div>


                                        <?php
                                 }
                                ?>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- section spesial -->
                <section class="sec-spesial pt-4">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4 ">

                            <div class="text-center">
                                <span class="spesial-title">
                                    SPESIAL BUAT KAMU! <br>PERAWATAN RAMBUT ALAMI,
                                </span>
                            </div>

                            <div class="text-center">
                                <span class="spesial-subtitle">
                                    TANPA KE SALON
                                </span>
                            </div>

                            <div class="text-center">
                                <span class="spesial-title">
                                    BISA KAMU DAPATKAN <br>DENGAN HARGA
                                </span>
                            </div>

                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="200" class="pt-5"
                                        src="<?php echo base_url("assets/images/assetsv2/group_115.png") ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="200" class="pt-5"
                                        src="<?php echo base_url("assets/images/assetsv2/group_116.png") ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img width="200" class="pt-5"
                                        src="<?php echo base_url("assets/images/assetsv2/group_117.png") ?>">
                                </div>
                            </div>

                            <div class="text-center mt-5 ">
                                <span class="spesial-title">
                                    Hanya
                                </span>
                            </div>

                            <div class="text-center">
                                <span class="spesial-price">
                                    Rp 69.000
                                </span>
                            </div>

                            <div class="text-center px-4">
                                <span class="spesial-price-desc">
                                    dapatkan manfaat yang maksimal tanpa harus merogoh kocek yang terlalu dalam.
                                </span>
                            </div>

                            <div class="row pt-4 pb-5">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <?php if ($reseller->tokopedia == null && $reseller->shopee == null) { ?>
                                    <a href="<?php echo $url ?>"
                                        class="btn btn-block cta-beli2 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"
                                        id="btnBuyUp">Beli Sekarang</a>
                                    <?php }else{ ?>
                                    <span class="btn btn-block cta-beli2 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"
                                        data-toggle="modal" data-target="#modalBuy" style="cursor:pointer">Beli
                                        Sekarang</span>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- section peta -->
                <section class="sec-peta pt-4">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4 ">

                            <div class="text-center">
                                <span class="peta-title">
                                    700+ RESELLER MINYAK <br>KEMIRI BAKAR
                                </span>
                            </div>

                            <div class="text-center">
                                <span class="peta-subtitle">
                                    TERSEBAR DI BERBAGAI <br>PROVINSI DI INDONESIA
                                </span>
                            </div>

                            <div class="row">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <img class="w-100 pt-5 pl-2"
                                        src="<?php echo base_url("assets/images/assetsv2/group_99.png") ?>">
                                </div>
                            </div>

                            <div class="row mt-5">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center align-items-center">
                                    <div class="peta-dot"></div><span class="peta-dot-text ml-2">Area Reseller</span>
                                </div>
                            </div>

                            <div class="text-center pt-4">
                                <p class="peta-list">
                                    Sumatera Utara <br>
                                    Sumatera Barat <br>
                                    Riau <br>
                                    Kepulauan Riau <br>
                                    Sumatera Selatan <br>
                                    Bangka Belitung <br>
                                    Bengkulu <br>
                                    Lampung <br>
                                    DKI Jakarta <br>
                                    Banten <br>
                                    Jawa Barat <br>
                                    Jawa Tengah <br>
                                    DI Yogyakarta <br>
                                    Jawa Timur <br>
                                    Bali <br>
                                    NTB <br>
                                    NTT <br>
                                    Kalimantan Selatan <br>
                                    Kalimantan Utara <br>
                                    Kalimantan Barat <br>
                                    Sulawesi Tengah <br>
                                    Sulawesi Selatan <br>
                                    Maluku Utara <br>
                                    Papua
                                </p>
                            </div>

                            <div class="row pt-4 pb-5">
                                <div
                                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center">
                                    <a href="<?php echo $url ?>"
                                        class="btn btn-block cta-beli2 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">Temukan
                                        Reseller di Kotamu</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- section footer -->
                <footer class="d-flex mt-5">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                            <img class="w-100"
                                src="<?php echo base_url("assets/images/kemiri/landing-page/mkb-botol-new.png") ?>"
                                style="bottom:0">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-7 pr-0 pl-0 pt-4 footer-description">
                            <img width="130"
                                src="<?php echo base_url();?>assets/images/kemiri/logo_mkb_my_kind_of_beauty.png"
                                alt="Logo">
                            <br>
                            <span class="company-name">CV. AMERTHA SEDANA SARI</span>
                            <div class="row m-0 pr-1 footer-contact">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-0">
                                    <div>
                                        <b>Office</b>
                                        <br>
                                        <a
                                            href="https://www.google.com/maps/place/MKB+Health+and+Beauty/@-8.6404531,115.2283771,15z/data=!4m5!3m4!1s0x0:0x28f52169c1dbb072!8m2!3d-8.6405313!4d115.228046">Jalan
                                            Ratna No.80 Tonja, Denpasar Utara, Kota Denpasar, Bali</a>
                                    </div>
                                    <div class="mt-3">
                                        <b>Office Hour</b>
                                        <br>
                                        <span>08.00 - 17.00 WITA</span>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12 pl-1 pr-0 pb-5">
                                    <a href="https://wa.me/"><i class="fa fa-whatsapp mr-1"></i>+62 813 376
                                        63109</a>
                                    <br>
                                    <a href="https://www.instagram.com/mkb.mykindofbeauty"><i
                                            class="fa fa-instagram"></i><span
                                            class="pl-1">My Kind Of Beauty</span></a>
                                    <br>
                                    <a href="https://www.facebook.com/mkb.mykindofbeauty"><i
                                            class="fa fa-facebook-square"></i><span
                                            class="pl-1">My Kind Of Beauty</span></a>
                                    <br>
                                    <a href="<?=base_url()?>"><i class="fa fa-globe mr-0"></i> www.mykindofbeauty.co.id</a>
                                </div>
                                <div class="col-12 pl-0 mt-4 text-white">
                                    <span>
                                        <i class="fa fa-copyright"></i> mykindofbeauty. 2021
                                    </span>
                                    <div class="py-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>


                <!-- Modal Buy Here -->
                <div class="modal fade" id="modalBuy" tabindex="-1" role="dialog" aria-labelledby="modalBuyLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content" style="border: 2px solid #22433e; border-radius:20px;">
                            <div class="modal-header"
                                style="border-bottom: 2px solid #22433e !important; background-color: #22433e !important; border-top-left-radius:15px !important; border-top-right-radius:15px !important; color:#FFF !important;">
                                <h5 class="modal-title" id="modalBuyLabel">Beli MKB disini!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="color:#FFF">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row px-2 py-2">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-1">
                                        <a href="<?= $url ?>" class="btn-cta-wa"><i class="fa fa-whatsapp"></i>
                                            Whatsapp</a>
                                    </div>

                                    <?php if ($reseller->shopee != null) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 my-1">
                                        <a href="<?= $reseller->shopee ?>" class="btn-cta-shopee">
                                            <img class="img-icon"
                                                src="<?php echo base_url("assets/images/kemiri/landing-page-new/icon-shopee.png") ?>">
                                            Shopee</a>
                                    </div>
                                    <?php } ?>

                                    <?php if ($reseller->tokopedia != null) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-1">
                                        <a href="<?= $reseller->tokopedia ?>" class="btn-cta-tokped">
                                            <img class="img-icon"
                                                src="<?php echo base_url("assets/images/kemiri/landing-page-new/icon-tokped.png") ?>">
                                            Tokopedia
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/owlcarousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript">
        $('#btnBuyUp').click(function () {
            fbq('trackCustom', 'CTA Beli Sekarang (atas)', {
                content_name: "Link to Reseller Page (from CTA upper)"
            });
        });

        $('#btnBuyBottom').click(function () {
            fbq('trackCustom', 'CTA Beli Sekarang (bawah)', {
                content_name: "Link to Reseller Page (from CTA bottom)"
            });
        });

        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            items: 1,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
        })
    </script>

</body>

</html>