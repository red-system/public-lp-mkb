<?php echo $tab_language ?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon2-image-file"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Management Page <?php echo $row->title; ?>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        <form method="post" action="<?php echo base_url('proweb/pages/update/' . $row->id); ?>" enctype="multipart/form-data" class="form-send">
            <?php
            foreach ($menu_list as $val){
                if ($row->type === 'home' || strpos($row->type, 'home_sesi_') !== FALSE) {
                    foreach ($val['home']['sub_menu'] as $key => $val1) {
                        if ($key === 'home_page' || 'home_'.$key === $row->type){
                            foreach ($val1['form_input'] as $key2 => $val2) {
                                $show[$key2] = $val2;
                            }
                        }
                    }
                } else {
                    foreach ($val[$row->type]['form_input'] as $key => $val1) {
                        $show[$key] = $val1;
                    }
                }
            }
            ?>

            <input type="hidden" name="type" value="<?php echo $type ?>">
            <?php if ($show['judul_top_menu']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">Title Top Menu</label>
                <textarea class="form-control" name="title_menu"><?php echo $row->title_menu ?></textarea>
            </div>
            <?php } ?>

            <?php if ($show['judul_halaman']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">Title Page</label>
                <textarea class="form-control" name="title"><?php echo $row->title ?></textarea>
            </div>
            <?php } ?>

            <?php if ($show['sub_judul']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">Sub Title</label>
                <textarea class="form-control" name="title_sub"><?php echo $row->title_sub ?></textarea>
            </div>
            <?php } ?>

            <?php if ($show['thumbnail']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">Thumbnail</label>
                <br/>
                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" class="img-thumbnail"
                     width="200">
                <br/><br/>
                <div class="custom-file">
                    <input type="file" class="custom-file-input browse-preview-img" accept="image/*" name="thumbnail"
                           id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <span class="form-text text-muted"><?php echo $this->main->file_info() ?></span>
            </div>
            <?php } ?>

            <?php if ($show['file']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">File</label>
                <br/>
                <div class="custom-file">
                    <input type="file" class="custom-file-input browse-preview-img" name="file" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                    <?php if ($row->file) { ?>
                        <br/>
                        <br/>
                        <a href="<?php echo $this->main->image_preview_url($row->file) ?>" target="_blank"
                           class="btn btn-success">Download File</a>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>

            <?php if ($show['description']) { ?>
            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                <label>Description</label>
                <textarea class="tinymce" id="exampleTextarea" rows="3"
                          name="description"><?php echo $row->description ?></textarea>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="exampleSelect1">Status SEO</label>
                <br/>
                <span class="kt-switch kt-switch--lg kt-switch--icon">
                    <label>
                        <input type="checkbox"
                               class="status-seo" <?php echo $row->status_seo == 'yes' ? 'checked="checked"' : ''; ?> value="yes"
                               name="status_seo">
                        <span></span>
                    </label>
                </span>
            </div>

            <div class="status-seo-wrapper">
                <div class="form-group">
                    <label for="exampleSelect1">Meta title</label>
                    <input type="text" class="form-control" value="<?php echo $row->meta_title ?>" name="meta_title">
                </div>
                <div class="form-group">
                    <label for="exampleSelect1">Meta Description</label>
                    <input type="text" class="form-control" value="<?php echo $row->meta_description ?>"
                           name="meta_description">
                </div>
                <div class="form-group">
                    <label for="exampleSelect1">Meta Keywords</label>
                    <input type="text" class="form-control" value="<?php echo $row->meta_keywords ?>"
                           name="meta_keywords">
                </div>
            </div>

            <?php if ($show['status_sub_konten']) { ?>
            <div class="form-group">
                <label for="exampleSelect1">Status Sub Konten Sederhana</label>
                <br/>
                <span class="kt-switch kt-switch--lg kt-switch--icon">
                    <label>
                        <input type="checkbox"
                               class="data-1-status" <?php echo $row->data_1_status == 'yes' ? 'checked="checked"' : ''; ?> value="yes"
                               name="data_1_status">
                        <span></span>
                    </label>
                </span>
            </div>
            <div class="data-1-wrapper">
                <h4>Adding Item Data</h4>
                <div class="data-1-wrapper">
                    <ol>
                        <?php $row_data_1 = json_decode($row->data_1, TRUE); ?>
                        <?php if ($show['status_sub_judul_section']) { ?>
                        <div class="form-group">
                            <label for="exampleSelect1">Title Section </label>
                            <textarea class="form-control" name="data_1[title_section]"><?php echo $row_data_1['title_section'] ?></textarea>
                        </div>
                        <?php } ?>

                        <?php if ($show['status_sub_deskripsi_section']) { ?>
                        <div class="form-group">
                            <label for="exampleSelect1">Description Section</label>
                            <textarea class="form-control" name="data_1[description_section]"><?php echo $row_data_1['description_section'] ?></textarea>
                        </div>
                        <?php } ?>

                        <?php
                        foreach ($row_data_1['title'] as $key => $title) { ?>
                            <li>
                                <?php if ($show['status_sub_judul']) { ?>
                                <div class="form-group">
                                    <label for="exampleSelect1">Title</label>
                                    <textarea class="form-control" name="data_1[title][]"><?php echo $title ?></textarea>
                                </div>
                                <?php } ?>

                                <?php if ($show['status_sub_deskripsi']) { ?>
                                <div class="form-group">
                                    <label for="exampleSelect1">Description</label>
                                    <textarea class="form-control" name="data_1[description][]"><?php echo $row_data_1['description'][$key] ?></textarea>
                                </div>
                                <?php } ?>

                                <?php if ($show['status_sub_gambar']) { ?>
                                <div class="form-group">
                                    <label for="exampleSelect1">Image Data</label>
                                    <br/>
                                    <img src="<?php echo $this->main->image_preview_url($row_data_1['images'][$key]) ?>" class="img-thumbnail" width="200">
                                    <br/><br/>
                                        <input type="hidden" name="images_old[]" value="<?php echo $row_data_1['images'][$key] ?>">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input browse-preview-img" accept="image/*" name="images[]" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <span class="form-text text-muted"><?php if (!empty($show['status_sub_gambar_note'])) { echo $show['status_sub_gambar_note']; } ?></span>
                                </div>
                                <?php } ?>

                                <?php if ($show['status_sub_gambar_deskripsi']) { ?>
                                <div class="form-group">
                                    <label for="exampleSelect1">Image Name</label>
                                    <textarea class="form-control" name="data_1[images_edit][]"><?php echo $row_data_1['images_edit'][$key] ?></textarea>
                                </div>
                                <?php } ?>

                                <?php if ($show['status_sub_icons_selection']) { ?>
                                <div class="form-group">
                                    <label for="exampleSelect1">Icon Selection</label>
                                    <select class="form-control input-select2 icons-selection" name="data_1[icons][]" data-value="<?php echo $row_data_1['icons'][$key] ?>">
                                        <option value=""></option>
                                        <option value="address-book">&#xf2b9; address-book</option>
                                        <option value="address-card">&#xf2bb; address-card</option>
                                        <option value="angry">&#xf556; angry</option>
                                        <option value="arrow-alt-circle-down">&#xf358; arrow-alt-circle-down</option>
                                        <option value="arrow-alt-circle-left">&#xf359; arrow-alt-circle-left</option>
                                        <option value="arrow-alt-circle-right">&#xf35a; arrow-alt-circle-right</option>
                                        <option value="arrow-alt-circle-up">&#xf35b; arrow-alt-circle-up</option>
                                        <option value="bell">&#xf0f3; bell</option>
                                        <option value="bell-slash">&#xf1f6; bell-slash</option>
                                        <option value="bookmark">&#xf02e; bookmark</option>
                                        <option value="building">&#xf1ad; building</option>
                                        <option value="calendar">&#xf133; calendar</option>
                                        <option value="calendar-alt">&#xf073; calendar-alt</option>
                                        <option value="calendar-check">&#xf274; calendar-check</option>
                                        <option value="calendar-minus">&#xf272; calendar-minus</option>
                                        <option value="calendar-plus">&#xf271; calendar-plus</option>
                                        <option value="calendar-times">&#xf273; calendar-times</option>
                                        <option value="caret-square-down">&#xf150; caret-square-down</option>
                                        <option value="caret-square-left">&#xf191; caret-square-left</option>
                                        <option value="caret-square-right">&#xf152; caret-square-right</option>
                                        <option value="caret-square-up">&#xf151; caret-square-up</option>
                                        <option value="chart-bar">&#xf080; chart-bar</option>
                                        <option value="check-circle">&#xf058; check-circle</option>
                                        <option value="check-square">&#xf14a; check-square</option>
                                        <option value="circle">&#xf111; circle</option>
                                        <option value="clipboard">&#xf328; clipboard</option>
                                        <option value="clock">&#xf017; clock</option>
                                        <option value="clone">&#xf24d; clone</option>
                                        <option value="closed-captioning">&#xf20a; closed-captioning</option>
                                        <option value="comment">&#xf075; comment</option>
                                        <option value="comment-alt">&#xf27a; comment-alt</option>
                                        <option value="comment-dots">&#xf4ad; comment-dots</option>
                                        <option value="comments">&#xf086; comments</option>
                                        <option value="compass">&#xf14e; compass</option>
                                        <option value="copy">&#xf0c5; copy</option>
                                        <option value="copyright">&#xf1f9; copyright</option>
                                        <option value="credit-card">&#xf09d; credit-card</option>
                                        <option value="dizzy">&#xf567; dizzy</option>
                                        <option value="dot-circle">&#xf192; dot-circle</option>
                                        <option value="edit">&#xf044; edit</option>
                                        <option value="envelope">&#xf40e0 envelope </option>
                                        <option value="envelope-open">&#xf2b6; envelope-open</option>
                                        <option value="eye">&#xf06e; eye</option>
                                        <option value="eye-slash">&#xf070; eye-slash</option>
                                        <option value="file">&#xf15b; file</option>
                                        <option value="file-alt">&#xf15c; file-alt</option>
                                        <option value="file-archive">&#xf1c6; file-archive</option>
                                        <option value="file-audio">&#xf1c7; file-audio</option>
                                        <option value="file-code">&#xf1c9; file-code</option>
                                        <option value="file-excel">&#xf1c3; file-excel </option>
                                        <option value="file-image">&#xf1c5; file-image</option>
                                        <option value="file-pdf">&#xf1c1; file-pdf</option>
                                        <option value="file-powerpoint">&#xf1c4; file-powerpoint</option>
                                        <option value="file-video">&#xf1c8; file-video</option>
                                        <option value="file-word">&#xf1c2; file-word</option>
                                        <option value="flag">&#xf024; flag</option>
                                        <option value="flushed">&#xf579; flushed</option>
                                        <option value="folder">&#xf07b; folder</option>
                                        <option value="folder-open">&#xf07c; folder-open </option>
                                        <option value="frown">&#xf119; frown</option>
                                        <option value="frown-open">&#xf57a; frown-open</option>
                                        <option value="futbol">&#xf1e3; futbol</option>
                                        <option value="gem">&#xf3a5; gem</option>
                                        <option value="grimace">&#xf57f; grimace</option>
                                        <option value="grin">&#xf580; grin</option>
                                        <option value="grin-alt">&#xf581; grin-alt</option>
                                        <option value="grin-beam">&#xf582; grin-beam</option>
                                        <option value="grin-beam-sweet">&#xf583; grin-beam-sweet </option>
                                        <option value="grin-hearts">&#xf584; grin-hearts</option>
                                        <option value="grin-squint">&#xf585; grin-squint</option>
                                        <option value="grin-squint-tears">&#xf586; grin-squint-tears</option>
                                        <option value="grin-stars">&#xf587; grin-stars</option>
                                        <option value="grin-tears">&#xf588; grin-tears</option>
                                        <option value="grin-tongue">&#xf589; grin-tongue</option>
                                        <option value="grin-tongue-squint">&#xf58a; grin-tongue-squint</option>
                                        <option value="grin-tongue-wink">&#xf58b; grin-tongue-wink</option>
                                        <option value="grin-wink">&#xf58c; grin-wink </option>
                                        <option value="hand-lizard">&#xf258; hand-lizard</option>
                                        <option value="hand-paper">&#xf256; hand-paper</option>
                                        <option value="hand-peace">&#xf25b; hand-peace</option>
                                        <option value="hand-point-down">&#xf0a7; hand-point-down</option>
                                        <option value="hand-point-left">&#xf0a5; hand-point-left</option>
                                        <option value="hand-point-right">&#xf0a4; hand-point-right</option>
                                        <option value="hand-point-up">&#xf0a6; hand-point-up</option>
                                        <option value="hand-pointer">&#xf25a; hand-pointer</option>
                                        <option value="hand-rock">&#xf255; hand-rock </option>
                                        <option value="hand-scissors">&#xf257; hand-scissors</option>
                                        <option value="hand-spock">&#xf259; hand-spock</option>
                                        <option value="handshake">&#xf2b5; handshake</option>
                                        <option value="hdd">&#xf0a0; hdd</option>
                                        <option value="heart">&#xf004; heart</option>
                                        <option value="home">&#xf015; home</option>
                                        <option value="hospital">&#xf0f8; hospital</option>
                                        <option value="hourglass">&#xf254; hourglass</option>
                                        <option value="id-badge">&#xf2c1; id-badge</option>
                                        <option value="id-card">&#xf2c2; id-card </option>
                                        <option value="image">&#xf03e; image</option>
                                        <option value="images">&#xf302; images</option>
                                        <option value="keyboard">&#xf11c; keyboard</option>
                                        <option value="kiss">&#xf596; kiss</option>
                                        <option value="kiss-beam">&#xf597; kiss-beam</option>
                                        <option value="kiss-wink-heart">&#xf598; kiss-wink-heart</option>
                                        <option value="laugh">&#xf599; laugh</option>
                                        <option value="laugh-beam">&#xf59a; laugh-beam</option>
                                        <option value="laugh-squint">&#xf59b; laugh-squint </option>
                                        <option value="laugh-wink">&#xf59c; laugh-wink</option>
                                        <option value="lemon">&#xf094; lemon</option>
                                        <option value="life-ring">&#xf1cd; life-ring</option>
                                        <option value="lightbulb">&#xf0eb; lightbulb</option>
                                        <option value="list-alt">&#xf022; list-alt</option>
                                        <option value="map">&#xf279; map</option>
                                        <option value="meh">&#xf11a; meh</option>
                                        <option value="meh-blank">&#xf5a4; meh-blank</option>
                                        <option value="meh-rolling-eyes">&#xf5a5; meh-rolling-eyes </option>
                                        <option value="minus-square">&#xf146; minus-square</option>
                                        <option value="money-bill-alt">&#xf3d1; money-bill-alt</option>
                                        <option value="moon">&#xf186; moon</option>
                                        <option value="newspaper">&#xf1ea; newspaper</option>
                                        <option value="object-group">&#xf247; object-group</option>
                                        <option value="object-upgroup">&#xf248; object-upgroup</option>
                                        <option value="paper-plane">&#xf1d8; paper-plane</option>
                                        <option value="pause-circle">&#xf28b; pause-circle</option>
                                        <option value="play-circle">&#xf144; play-circle </option>
                                        <option value="plus-square">&#xf0fe; plus-square</option>
                                        <option value="question-circle">&#xf059; question-circle</option>
                                        <option value="registered">&#xf25d; registered</option>
                                        <option value="sad-cry">&#xf5b3; sad-cry</option>
                                        <option value="sad-tear">&#xf5b4; sad-tear</option>
                                        <option value="save">&#xf0c7; save</option>
                                        <option value="share-square">&#xf14d; share-square</option>
                                        <option value="smile">&#xf118; smile</option>
                                        <option value="smile-beam">&#xf5b8; smile-beam </option>
                                        <option value="smile-wink">&#xf4da; smile-wink</option>
                                        <option value="snowflake">&#xf2dc; snowflake</option>
                                        <option value="square">&#xf0c8; square</option>
                                        <option value="star">&#xf005; star</option>
                                        <option value="star-half">&#xf089; star-half</option>
                                        <option value="sticky-note">&#xf249; sticky-note</option>
                                        <option value="stop-circle">&#xf28d; stop-circle</option>
                                        <option value="sun">&#xf185; sun</option>
                                        <option value="surprise">&#xf5c2; surprise </option>
                                        <option value="thumbs-down">&#xf165; thumbs-down</option>
                                        <option value="thumbs-up">&#xf1164; thumbs-up</option>
                                        <option value="times-circle">&#xf057; times-circle</option>
                                        <option value="tired">&#xf5c8; tired</option>
                                        <option value="trash-alt">&#xf2ed; trash-alt</option>
                                        <option value="user">&#xf007; user</option>
                                        <option value="user-circle">&#xf2bd; user-circle</option>
                                        <option value="window-close">&#xf410; window-close</option>
                                        <option value="window-maximize">&#xf2d0; window-maximize </option>
                                        <option value="window-minimize">&#xf2d1; window-minimize</option>
                                        <option value="window-restore">&#xf2d2; window-restore</option>
                                    </select>
                                </div>
                                <?php } ?>

                                <button type="button" class="btn btn-danger btn-data-1-hapus">Remove</button>
                                <br/>
                                <br/>
                                <br/>
                            </li>
                        <?php } ?>
                    </ol>
                </div>
                <button type="button" class="btn btn-success btn-data-1-tambah">Add Data</button>
            </div>
            <?php } ?>

            <br/><br/>
            <br/><br/>
            <br/><br/>
            <button type="submit" class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-lg btn-floating">
                Simpan
            </button>
        </form>
    </div>
</div>

<div class="data-1-data hide">
    <li>
        <?php if ($show['status_sub_judul']) { ?>
        <div class="form-group">
            <label for="exampleSelect1">Title</label>
            <textarea class="form-control" name="data_1[title][]"></textarea>
        </div>
        <?php } ?>

        <?php if ($show['status_sub_deskripsi']) { ?>
        <div class="form-group">
            <label for="exampleSelect1">Description</label>
            <textarea class="form-control" name="data_1[description][]"></textarea>
        </div>
        <?php } ?>

        <?php if ($show['status_sub_gambar']) { ?>
        <div class="form-group">
            <label for="exampleSelect1">Image Data</label>
            <br/>
            <img src="" class="img-thumbnail" width="200">
            <br/><br/>
                <input type="hidden" name="images_old[]" value="">
            <div class="custom-file">
                <input type="file" class="custom-file-input browse-preview-img" accept="image/*" name="images[]" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            <span class="form-text text-muted"><?php echo $this->main->file_info() ?></span>
        </div>
        <?php } ?>

        <?php if ($show['status_sub_gambar_deskripsi']) { ?>
        <div class="form-group">
            <label for="exampleSelect1">Image Name</label>
            <textarea class="form-control" name="data_1[images_edit][]"></textarea>
        </div>
        <?php } ?>

        <?php if ($show['status_sub_icons_selection']) { ?>
        <div class="form-group">
            <label for="exampleSelect1">Icon Selection</label>
            <select class="form-control input-select2 icons-selection new-select2" name="data_1[icons][]">
                <option value=""></option>
                <option value="address-book">&#xf2b9; address-book</option>
                <option value="address-card">&#xf2bb; address-card</option>
                <option value="angry">&#xf556; angry</option>
                <option value="arrow-alt-circle-down">&#xf358; arrow-alt-circle-down</option>
                <option value="arrow-alt-circle-left">&#xf359; arrow-alt-circle-left</option>
                <option value="arrow-alt-circle-right">&#xf35a; arrow-alt-circle-right</option>
                <option value="arrow-alt-circle-up">&#xf35b; arrow-alt-circle-up</option>
                <option value="bell">&#xf0f3; bell</option>
                <option value="bell-slash">&#xf1f6; bell-slash</option>
                <option value="bookmark">&#xf02e; bookmark</option>
                <option value="building">&#xf1ad; building</option>
                <option value="calendar">&#xf133; calendar</option>
                <option value="calendar-alt">&#xf073; calendar-alt</option>
                <option value="calendar-check">&#xf274; calendar-check</option>
                <option value="calendar-minus">&#xf272; calendar-minus</option>
                <option value="calendar-plus">&#xf271; calendar-plus</option>
                <option value="calendar-times">&#xf273; calendar-times</option>
                <option value="caret-square-down">&#xf150; caret-square-down</option>
                <option value="caret-square-left">&#xf191; caret-square-left</option>
                <option value="caret-square-right">&#xf152; caret-square-right</option>
                <option value="caret-square-up">&#xf151; caret-square-up</option>
                <option value="chart-bar">&#xf080; chart-bar</option>
                <option value="check-circle">&#xf058; check-circle</option>
                <option value="check-square">&#xf14a; check-square</option>
                <option value="circle">&#xf111; circle</option>
                <option value="clipboard">&#xf328; clipboard</option>
                <option value="clock">&#xf017; clock</option>
                <option value="clone">&#xf24d; clone</option>
                <option value="closed-captioning">&#xf20a; closed-captioning</option>
                <option value="comment">&#xf075; comment</option>
                <option value="comment-alt">&#xf27a; comment-alt</option>
                <option value="comment-dots">&#xf4ad; comment-dots</option>
                <option value="comments">&#xf086; comments</option>
                <option value="compass">&#xf14e; compass</option>
                <option value="copy">&#xf0c5; copy</option>
                <option value="copyright">&#xf1f9; copyright</option>
                <option value="credit-card">&#xf09d; credit-card</option>
                <option value="dizzy">&#xf567; dizzy</option>
                <option value="dot-circle">&#xf192; dot-circle</option>
                <option value="edit">&#xf044; edit</option>
                <option value="envelope">&#xf40e0 envelope </option>
                <option value="envelope-open">&#xf2b6; envelope-open</option>
                <option value="eye">&#xf06e; eye</option>
                <option value="eye-slash">&#xf070; eye-slash</option>
                <option value="file">&#xf15b; file</option>
                <option value="file-alt">&#xf15c; file-alt</option>
                <option value="file-archive">&#xf1c6; file-archive</option>
                <option value="file-audio">&#xf1c7; file-audio</option>
                <option value="file-code">&#xf1c9; file-code</option>
                <option value="file-excel">&#xf1c3; file-excel </option>
                <option value="file-image">&#xf1c5; file-image</option>
                <option value="file-pdf">&#xf1c1; file-pdf</option>
                <option value="file-powerpoint">&#xf1c4; file-powerpoint</option>
                <option value="file-video">&#xf1c8; file-video</option>
                <option value="file-word">&#xf1c2; file-word</option>
                <option value="flag">&#xf024; flag</option>
                <option value="flushed">&#xf579; flushed</option>
                <option value="folder">&#xf07b; folder</option>
                <option value="folder-open">&#xf07c; folder-open </option>
                <option value="frown">&#xf119; frown</option>
                <option value="frown-open">&#xf57a; frown-open</option>
                <option value="futbol">&#xf1e3; futbol</option>
                <option value="gem">&#xf3a5; gem</option>
                <option value="grimace">&#xf57f; grimace</option>
                <option value="grin">&#xf580; grin</option>
                <option value="grin-alt">&#xf581; grin-alt</option>
                <option value="grin-beam">&#xf582; grin-beam</option>
                <option value="grin-beam-sweet">&#xf583; grin-beam-sweet </option>
                <option value="grin-hearts">&#xf584; grin-hearts</option>
                <option value="grin-squint">&#xf585; grin-squint</option>
                <option value="grin-squint-tears">&#xf586; grin-squint-tears</option>
                <option value="grin-stars">&#xf587; grin-stars</option>
                <option value="grin-tears">&#xf588; grin-tears</option>
                <option value="grin-tongue">&#xf589; grin-tongue</option>
                <option value="grin-tongue-squint">&#xf58a; grin-tongue-squint</option>
                <option value="grin-tongue-wink">&#xf58b; grin-tongue-wink</option>
                <option value="grin-wink">&#xf58c; grin-wink </option>
                <option value="hand-lizard">&#xf258; hand-lizard</option>
                <option value="hand-paper">&#xf256; hand-paper</option>
                <option value="hand-peace">&#xf25b; hand-peace</option>
                <option value="hand-point-down">&#xf0a7; hand-point-down</option>
                <option value="hand-point-left">&#xf0a5; hand-point-left</option>
                <option value="hand-point-right">&#xf0a4; hand-point-right</option>
                <option value="hand-point-up">&#xf0a6; hand-point-up</option>
                <option value="hand-pointer">&#xf25a; hand-pointer</option>
                <option value="hand-rock">&#xf255; hand-rock </option>
                <option value="hand-scissors">&#xf257; hand-scissors</option>
                <option value="hand-spock">&#xf259; hand-spock</option>
                <option value="handshake">&#xf2b5; handshake</option>
                <option value="hdd">&#xf0a0; hdd</option>
                <option value="heart">&#xf004; heart</option>
                <option value="home">&#xf015; home</option>
                <option value="hospital">&#xf0f8; hospital</option>
                <option value="hourglass">&#xf254; hourglass</option>
                <option value="id-badge">&#xf2c1; id-badge</option>
                <option value="id-card">&#xf2c2; id-card </option>
                <option value="image">&#xf03e; image</option>
                <option value="images">&#xf302; images</option>
                <option value="keyboard">&#xf11c; keyboard</option>
                <option value="kiss">&#xf596; kiss</option>
                <option value="kiss-beam">&#xf597; kiss-beam</option>
                <option value="kiss-wink-heart">&#xf598; kiss-wink-heart</option>
                <option value="laugh">&#xf599; laugh</option>
                <option value="laugh-beam">&#xf59a; laugh-beam</option>
                <option value="laugh-squint">&#xf59b; laugh-squint </option>
                <option value="laugh-wink">&#xf59c; laugh-wink</option>
                <option value="lemon">&#xf094; lemon</option>
                <option value="life-ring">&#xf1cd; life-ring</option>
                <option value="lightbulb">&#xf0eb; lightbulb</option>
                <option value="list-alt">&#xf022; list-alt</option>
                <option value="map">&#xf279; map</option>
                <option value="meh">&#xf11a; meh</option>
                <option value="meh-blank">&#xf5a4; meh-blank</option>
                <option value="meh-rolling-eyes">&#xf5a5; meh-rolling-eyes </option>
                <option value="minus-square">&#xf146; minus-square</option>
                <option value="money-bill-alt">&#xf3d1; money-bill-alt</option>
                <option value="moon">&#xf186; moon</option>
                <option value="newspaper">&#xf1ea; newspaper</option>
                <option value="object-group">&#xf247; object-group</option>
                <option value="object-upgroup">&#xf248; object-upgroup</option>
                <option value="paper-plane">&#xf1d8; paper-plane</option>
                <option value="pause-circle">&#xf28b; pause-circle</option>
                <option value="play-circle">&#xf144; play-circle </option>
                <option value="plus-square">&#xf0fe; plus-square</option>
                <option value="question-circle">&#xf059; question-circle</option>
                <option value="registered">&#xf25d; registered</option>
                <option value="sad-cry">&#xf5b3; sad-cry</option>
                <option value="sad-tear">&#xf5b4; sad-tear</option>
                <option value="save">&#xf0c7; save</option>
                <option value="share-square">&#xf14d; share-square</option>
                <option value="smile">&#xf118; smile</option>
                <option value="smile-beam">&#xf5b8; smile-beam </option>
                <option value="smile-wink">&#xf4da; smile-wink</option>
                <option value="snowflake">&#xf2dc; snowflake</option>
                <option value="square">&#xf0c8; square</option>
                <option value="star">&#xf005; star</option>
                <option value="star-half">&#xf089; star-half</option>
                <option value="sticky-note">&#xf249; sticky-note</option>
                <option value="stop-circle">&#xf28d; stop-circle</option>
                <option value="sun">&#xf185; sun</option>
                <option value="surprise">&#xf5c2; surprise </option>
                <option value="thumbs-down">&#xf165; thumbs-down</option>
                <option value="thumbs-up">&#xf1164; thumbs-up</option>
                <option value="times-circle">&#xf057; times-circle</option>
                <option value="tired">&#xf5c8; tired</option>
                <option value="trash-alt">&#xf2ed; trash-alt</option>
                <option value="user">&#xf007; user</option>
                <option value="user-circle">&#xf2bd; user-circle</option>
                <option value="window-close">&#xf410; window-close</option>
                <option value="window-maximize">&#xf2d0; window-maximize </option>
                <option value="window-minimize">&#xf2d1; window-minimize</option>
                <option value="window-restore">&#xf2d2; window-restore</option>
            </select>
        </div>
        <?php } ?>

        <button type="button" class="btn btn-danger btn-data-1-hapus">Remove</button>
        <br/>
        <br/>
        <br/>
    </li>
<!--    <li>-->
<!--        <div class="form-group">-->
<!--            <label for="exampleSelect1">Judul</label>-->
<!--            <textarea class="form-control" name="data_1[title][]"></textarea>-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            <label for="exampleSelect1">Deskripsi</label>-->
<!--            <textarea class="form-control" name="data_1[description][]"></textarea>-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            <label for="exampleSelect1">Gambar Data</label>-->
<!--            <br/>-->
<!--            <img src="" class="img-thumbnail" width="200">-->
<!--            <br/><br/>-->
<!--            <div class="custom-file">-->
<!--                <input type="file" class="custom-file-input browse-preview-img" accept="image/*" name="data_1[images][]" id="customFile">-->
<!--                <label class="custom-file-label" for="customFile">Choose file</label>-->
<!--            </div>-->
<!--            <span class="form-text text-muted">--><?php //echo $this->main->file_info() ?><!--</span>-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            <label for="exampleSelect1">Nama Gambar</label>-->
<!--            <input class="form-control" name="data_1[images_edit][]">-->
<!--        </div>-->
<!--        <button type="button" class="btn btn-danger btn-data-1-hapus">Hapus</button>-->
<!--        <br/>-->
<!--        <br/>-->
<!--        <br/>-->
<!--    </li>-->
</div>
