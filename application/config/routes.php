<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang_code_list = array('id','jp','en','fr','zh');

// $connection = mysqli_connect('159.65.132.116', 'ngurah', 'Ngu4Ra3hhh!@#', 'kemiri_web');
// $connection = mysqli_connect('localhost', 'support88_front_user', 'G$o?@)v%f6Nk', 'support88_front_db');
// $blog_category = mysqli_query($connection,
//     "SELECT blog.id, blog.title, blog.slug, blog_category.slug AS category_slug
//             FROM blog 
//             INNER JOIN blog_category ON blog_category.id = blog.id_blog_category");

// $event_category = mysqli_query($connection,
//     "SELECT event.id, event.title, event.slug FROM event");

$route['default_controller'] = 'landing_page/landing_page';
$route['site-map'] = 'landing_page/site_map';
$route['proweb'] = 'proweb/login';
$route['proweb/login'] = 'proweb/login/process';
$route['form-webinar'] = "landing_page/form_bisnis";
$route['form-bisnis/kabupaten'] = "landing_page/kabupaten";
$route['form-bisnis/kecamatan'] = "landing_page/kecamatan";
$route['form-bisnis/save'] = "landing_page/save";
$route['kebijakan-privasi'] = "kebijakan_privasi/index";
$route['syarat-ketentuan'] = "syarat_ketentuan/index";

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$route['tidak-ditemukan'] = 'not_found';



// $route['home'] = 'home/index';
// $route['tentang-kami'] = 'about_us';
// $route['frequently-asked-question'] = 'faq';
// $route['syarat-ketentuan'] = 'terms_condition';
// $route['agen'] = 'agen';
// $route['kontak-kami'] = 'contact_us';
// $route['kontak-kami/send'] = 'contact_us/send_message';

// $route['sitemap'] = 'sitemap';

// $route['blog-and-news'] = 'blog/index';
// $route['blog-and-news/(:num)'] = 'blog/index';
// $route['event'] = 'event/index';
// $route['event/(:num)'] = 'event/index';

// $route['agen/(:any)'] = 'agen/index/$1';
// $route['agen/(:any)/(:any)'] = 'agen/index/$1/$2';
// $route['agen/(:any)/(:any)/(:any)'] = 'agen/index/$1/$2/$3';
// $route['agen/(:any)/(:any)/(:any)/(:any)'] = 'agen/detail/$1/$2/$3/$4';
// $route['agen-dummy/detail-dummy'] = 'agen/detail_dummy';

// $route['agen-find/province'] = 'agen_find/province';
// $route['agen-find/search/province'] = 'agen_find/province_search';
// $route['agen-find/city'] = 'agen_find/city';
// $route['agen-find/search/city'] = 'agen_find/city_search';
// $route['agen-find/subdistrict'] = 'agen_find/subdistrict';
// $route['agen-find/search/subdistrict'] = 'agen_find/subdistrict_search';
// $route['agen-find/list-agent'] = 'agen_find/list_agent';

$route['agen-find/provinces'] = 'agen_find/provinces';
$route['agen-find/search/provinces'] = 'agen_find/provinces_search';
$route['agen-find/citys'] = 'agen_find/citys';
$route['agen-find/search/citys'] = 'agen_find/citys_search';
$route['agen-find/subdistricts'] = 'agen_find/subdistricts';
$route['agen-find/search/subdistricts'] = 'agen_find/subdistricts_search';
$route['agen-find/lists-agent'] = 'agen_find/lists_agent';

$route['agen-find/lists-super-agent'] = 'agen_find/lists_super_agent';

// $route['landing-page'] = 'landing_page/landing_page';
// $route['landing-page'] = 'landing_page/landing_page';
$route['reseller/:any'] = 'landing_page/landing_page';
$route['reseller-page'] = 'landing_page/reseller_page';
$route['super-reseller-page'] = 'landing_page/super_reseller_page';
$route['gabung'] = 'landing_page/gabung';
$route['gabung-super'] = 'landing_page/gabung_super';
$route['gabung/:any'] = 'landing_page/gabung';
// $route['landing-page-v2'] = 'landing_page/landing_page_v2';
// $route['reseller-v2/:any'] = 'landing_page/landing_page_v2';

$route['mapping'] = 'Mapping/index';
$route['mapping/province'] = 'Mapping/getProvinceLatLong';
$route['mapping/city'] = 'Mapping/getCityLatLong';
$route['mapping/subdistrict'] = 'Mapping/getSubdistrictLatLong';


// $route['gabung_sample'] = 'landing_page/gabung_sample';

// while ($data = mysqli_fetch_array($blog_category)) {
//     $route['blog-and-news/' . $data['category_slug']] = 'blog/index/'.$data['category_slug'];
//     $route['blog-and-news/' . $data['category_slug'].'/(:num)'] = 'blog/index/'.$data['category_slug'];
//     $route['blog-and-news/' . $data['category_slug'].'/'.$data['slug']] = 'blog/detail/'.$data['category_slug'].'/'.$data['slug'];
// }

// while ($data = mysqli_fetch_array($event_category)) {
//     $route['event/' .$data['slug']] = 'event/detail/'.$data['slug'];
// }

//foreach($lang_code_list as $code) {
//
//    $route[$code.'/tidak-ditemukan'] = 'not_found';
//
//    $route[$code.'/tentang-kami'] = 'about_us';
//    $route[$code.'/frequently-asked-question'] = 'faq';
//    $route[$code.'/syarat-ketentuan'] = 'terms_condition';
//    $route[$code.'/agen'] = 'agen';
//    $route[$code.'/contact-us'] = 'contact_us';
//    $route[$code.'/contact-us/send'] = 'contact_us/send';
//
//    while ($data = mysqli_fetch_array($blog_category)) {
//        $route[$code.'/blog/' . $data['category_slug']] = 'blog/index/'.$data['category_slug'];
//        $route[$code.'/blog/' . $data['category_slug'].'/(:num)'] = 'blog/index/'.$data['category_slug'];
//        $route[$code.'/blog/' . $data['category_slug'].'/'.$data['slug']] = 'blog/detail/'.$data['slug'];
//    }
//
//    while ($data = mysqli_fetch_array($event_category)) {
//        $route[$code.'/event/' . $data['category_slug']] = 'event/index/'.$data['category_slug'];
//        $route[$code.'/event/' . $data['category_slug'].'/(:num)'] = 'event/index/'.$data['category_slug'];
//        $route[$code.'/event/' . $data['category_slug'].'/'.$data['slug']] = 'event/detail/'.$data['slug'];
//    }
//
//
//    /**
//     * Blog and Event
//     */
//    $route[$code.'/blog'] = 'blog/index';
//    $route[$code.'/blog/(:num)'] = 'blog/index';
//    $route[$code.'/event'] = 'blog/index';
//    $route[$code.'/event/(:num)'] = 'blog/index';
//
//    $route[$code.'/404_override'] = 'not_found';
//
//    $route['^'.$code.'/(.+)$'] = "$1";
//    $route['^'.$code.'$'] = $route['default_controller'];
//}

function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',','(',')');
    $replace = array('-', '-', 'and', '-', '-', '-','','');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
