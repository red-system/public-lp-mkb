<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapping extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        // $data['provinces'] = $this->db
        //     ->select('*')
        //     ->get('province')
        //     ->result();

        $data['provinces'] = $this->db
            ->select('subdistrict.province, subdistrict.province_id')
            ->group_by('subdistrict.province_id')
            ->get('subdistrict')->result();

        $reseller = $this->db
            // ->select('reseller.nama, reseller.outlet_subdistrict_id, subdistrict.subdistrict_name, subdistrict.latitude, subdistrict.longitude')
            ->select('reseller.nama, reseller.outlet_subdistrict_id, subdistrict.subdistrict_name, reseller.latitude, reseller.longitude')
            ->join('subdistrict', 'subdistrict.subdistrict_id = reseller.outlet_subdistrict_id')
            ->where('reseller.status', 'active')
            ->group_start()
            ->where('reseller.type', 'super agen')
            ->or_where('reseller.type', 'agent')
            ->group_end()
            ->where('reseller.demo !=', '1')
            ->where('reseller.latitude is not null', null)
            ->where('reseller.latitude !=', '-')
            ->where('reseller.longitude is not null', null)
            ->where('reseller.longitude !=', '-')
            ->get('reseller')->result();

        $item_marker = array();

            foreach ($reseller as $key) {
                $arr_item[$key->nama] = array($key->nama, $key->latitude, $key->longitude);
                array_push($item_marker, $arr_item[$key->nama]);
            }

        $data['marker'] = json_encode($item_marker);

        $this->load->view('front/mapping', $data);
    }

    public function getProvinceLatLong()
    {

        $province_id = $this->input->post('province_id');

        $province = $this->db
            ->select('subdistrict.province, subdistrict.latitude, subdistrict.longitude')
            ->where('subdistrict.province_id', $province_id)
            ->where('subdistrict.type', 'Kota')
            ->where('subdistrict.latitude !=', null)
            ->limit(1)
            ->get('subdistrict')->row();

        $citys = $this->db
            ->select('subdistrict.city, subdistrict.city_id')
            ->where('subdistrict.province_id', $province_id)
            ->group_by('subdistrict.city_id')
            // ->order_by('rand()')
            ->get('subdistrict')->result();

            $select_city = array();
                array_push($select_city, "<option value=''>Pilih Kabupaten</option>");
            foreach($citys as $city){
                array_push($select_city, "<option value='".$city->city_id."'>".$city->city."</option>");
            }


        if ($province->latitude != null) {
            $data = [
                'latitude' => $province->latitude,
                'longitude' => $province->longitude,
                'zoom' => 9,
            ];
        }else{
            $data = [
                'latitude' => '-1.035721',
                'longitude' => '118.436931',
                'zoom' => 5,
            ];
        }

        echo json_encode(array(
            'status' => 'success',
            'latlong' => $data,
            'data' => $select_city,
         ));

    }

    public function getCityLatLong()
    {

        $city_id = $this->input->post('city_id');

        $city = $this->db
            ->select('subdistrict.city, subdistrict.latitude, subdistrict.longitude')
            ->where('subdistrict.city_id', $city_id)
            ->where('subdistrict.latitude !=', null)
            // ->order_by('rand()')
            ->limit(1)
            ->get('subdistrict')->row();

        $subdistricts = $this->db
            ->select('subdistrict.subdistrict_name, subdistrict.subdistrict_id')
            ->where('subdistrict.city_id', $city_id)
            ->group_by('subdistrict.subdistrict_id')
            ->get('subdistrict')->result();

            $select_subdistrict = array();
            array_push($select_subdistrict, "<option value=''>Pilih Kecamatan</option>");
            foreach($subdistricts as $subdistrict){
                array_push($select_subdistrict, "<option value='".$subdistrict->subdistrict_id."'>".$subdistrict->subdistrict_name."</option>");
            }

        if ($city->latitude != null) {
            $data = [
                'latitude' => $city->latitude,
                'longitude' => $city->longitude,
                'zoom' => 13,
            ];
        }else{
            $data = [
                'latitude' => '-1.035721',
                'longitude' => '118.436931',
                'zoom' => 5,
            ];
        }

        echo json_encode(array(
            'status' => 'success',
            'latlong' => $data,
            'data' => $select_subdistrict,
         ));

    }

    public function getSubdistrictLatLong()
    {

        $subdistrict_id = $this->input->post('subdistrict_id');

        $subdistrict = $this->db
            ->select('subdistrict.subdistrict_name, subdistrict.latitude, subdistrict.longitude')
            ->where('subdistrict.subdistrict_id', $subdistrict_id)
            ->where('subdistrict.latitude !=', null)
            // ->order_by('rand()')
            ->limit(1)
            ->get('subdistrict')->row();

        if ($subdistrict->latitude != null) {
            $data = [
                'latitude' => $subdistrict->latitude,
                'longitude' => $subdistrict->longitude,
                'zoom' => 15,
            ];
        }else{
            $data = [
                'latitude' => '-1.035721',
                'longitude' => '118.436931',
                'zoom' => 5,
            ];
        }

        echo json_encode(array(
            'status' => 'success',
            'latlong' => $data,
         ));

    }




    

}


