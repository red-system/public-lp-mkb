<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

	public function index()
	{
        $data = $this->main->data_front();

        $data['page'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'about_us'))->get('pages')->row();
        $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();
        $data['testimonial_page'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'testimonial'))->get('pages')->row();
        $data['testimonial'] = $this->db->where('use', 'yes')->get('comment')->result();
        $data['download'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_9'))->get('pages')->row();

	    $this->template->front('about-us', $data);
    }
}
