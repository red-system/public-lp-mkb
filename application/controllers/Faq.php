<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

	public function index()
	{
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'faq'))->get('pages')->row();
        $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();
	    $this->template->front('faq', $data);
    }
}
