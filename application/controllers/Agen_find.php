<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agen_find extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function provinces()
	{

        $search = $this->input->post('search');

	    $data = $this->db
            ->select('*')
            ->like('province',$search , 'both')
            ->get('province')
            ->result();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function provinces_search()
	{
        $search = $this->input->post('province_name');

	    $data = $this->db
            ->select('*')
            ->where('province', $search)
            ->get('province')
            ->row();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function citys()
	{

        $province_id = $this->input->post('province_id');
        $search = $this->input->post('search');

	    $data = $this->db
            ->select('*')
            ->where(array(
                'province_id' => $province_id,
            ))
            ->like('city_name',$search , 'both')
            ->get('city')
            ->result();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function citys_search()
	{
        $city_name = $this->input->post('city_name');
        $province_id = $this->input->post('province_id');

	    $data = $this->db
            ->select('*')
            ->where('city_name', $city_name)
            ->get('city')
            ->row();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function subdistricts()
	{
        $province_id = $this->input->post('province_id');
        $city_id = $this->input->post('city_id');
        $search = $this->input->post('search');

	    $data = $this->db
            ->select('*')
            ->where(array(
                'city_id' => $city_id,
            ))
            ->like('subdistrict_name', $search , 'both')
            ->get('subdistrict')
            ->result();
            
        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function subdistricts_search()
	{
        $subdistrict_name = $this->input->post('subdistrict_name');
        $province_id = $this->input->post('province_id');
        $city_id = $this->input->post('city_id');

	    $data = $this->db
            ->select('*')
            ->where('subdistrict_name', $subdistrict_name)
            ->get('subdistrict')
            ->row();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function lists_agent()
	{

        $province_id = $this->input->post('province_id');
        $city_id = $this->input->post('city_id');
        $subdistrict_id = $this->input->post('subdistrict_id');
        $username = $this->input->post('username');

        $data = $this->db
            ->select('reseller.*, province.province, city.city_name, subdistrict.subdistrict_name')
            ->join('mykindofbeauty_master.login as db2', 'reseller.reseller_id = db2.reseller_id')
            ->join('province', 'province.province_id = reseller.outlet_province_id')
            ->join('city', 'city.city_id = reseller.outlet_city_id')
            ->join('subdistrict', 'subdistrict.subdistrict_id = reseller.outlet_subdistrict_id')
            ->where('reseller.status', 'active')
            ->where('reseller.type !=', 'shareholder')
            ->where('reseller.demo !=', '1')
            ->where('reseller.reseller_id !=', '1239')
            ->not_like('reseller.email', '#', 'after')
            ->order_by('rand()');

            if($province_id!=""){
                // $data = $this->db->where('province.province_id',$province_id);
                $data = $this->db->where('reseller.outlet_province_id',$province_id);
            }
            if($city_id!=""){
                // $data = $this->db->where('city.city_id',$city_id);
                $data = $this->db->where('reseller.outlet_city_id',$city_id);
            }
            if($subdistrict_id!=""){
                // $data = $this->db->where('subdistrict.subdistrict_id',$subdistrict_id);
                $data = $this->db->where('reseller.outlet_subdistrict_id',$subdistrict_id);
            }
            if($username!=""){
                $data = $this->db->where('db2.username',$username);
            }

            $data = $this->db->get('reseller')->result();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function lists_super_agent()
	{

        $province_id = $this->input->post('province_id');
        $city_id = $this->input->post('city_id');
        $subdistrict_id = $this->input->post('subdistrict_id');
        $username = $this->input->post('username');

        $data = $this->db
            ->select('reseller.*, province.province, city.city_name, subdistrict.subdistrict_name')
            ->join('mykindofbeauty_master.login as db2', 'reseller.reseller_id = db2.reseller_id')
            ->join('province', 'province.province_id = reseller.outlet_province_id')
            ->join('city', 'city.city_id = reseller.outlet_city_id')
            ->join('subdistrict', 'subdistrict.subdistrict_id = reseller.outlet_subdistrict_id')
            ->where('reseller.status', 'active')
            ->where('reseller.demo !=', '1')
            ->where('reseller.type', 'super agen')
            ->order_by('rand()');

            if($province_id!=""){
                // $data = $this->db->where('province.province_id',$province_id);
                $data = $this->db->where('reseller.outlet_province_id',$province_id);
            }
            if($city_id!=""){
                // $data = $this->db->where('city.city_id',$city_id);
                $data = $this->db->where('reseller.outlet_city_id',$city_id);
            }
            if($subdistrict_id!=""){
                // $data = $this->db->where('subdistrict.subdistrict_id',$subdistrict_id);
                $data = $this->db->where('reseller.outlet_subdistrict_id',$subdistrict_id);
            }
            // if($username!=""){
            //     $data = $this->db->where('db2.username',$username);
            // }

            $data = $this->db->get('reseller')->result();

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    
    public function province()
	{
	    // $url = 'http://api-2.support88.id/v1/province';
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/province';
	    // $url = 'http://localhost/mkb-api/public/v1/province';

	    $ch = curl_init( $url );

        $json = json_encode( array( 'search'=> $this->input->post('search') ) );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            // 'Authorization: bearer 79aa765995b72f88349a017aac022c13fd76130a8d124dba32bc890164e3f9deebdb4b5e991ee08e2d2b226e48ea2770250d',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        // print_r($data);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function province_search()
	{
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/province-search?search='.$this->input->post('province_name');
	    // $url = 'http://localhost/mkb-api/public/v1/province-search?search='.$this->input->post('province_name');
//	    $url = 'http://api-2.support88.id/v1/kecamatan?search=denpasar+barat';

	    $ch = curl_init( $url );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

//        print_r($result);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function city()
	{
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/city';
	    // $url = 'http://localhost/mkb-api/public/v1/city';

	    $ch = curl_init( $url );

        $json = json_encode( array(
            'province_id' => $this->input->post('province_id'),
            'search'=> $this->input->post('search')
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function city_search()
	{
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/city-search?search='.$this->input->post('city_name');
	    // $url = 'http://localhost/mkb-api/public/v1/city-search?search='.$this->input->post('city_name');

	    $ch = curl_init( $url );

        $json = json_encode( array(
            'province_id' => $this->input->post('province_id'),
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_PUT, $json );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function subdistrict()
	{
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/subdistrict';
	    // $url = 'http://localhost/mkb-api/public/v1/subdistrict';

	    $ch = curl_init( $url );

        $json = json_encode( array(
            'province_id' => $this->input->post('province_id'),
            'city_id' => $this->input->post('city_id'),
            'search'=> $this->input->post('search')
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function subdistrict_search()
	{

	    $url = 'https://api.mykindofbeauty.co.id/public/v1/subdistrict-search?search='.$this->input->post('subdistrict_name');
	    // $url = 'http://localhost/mkb-api/public/v1/subdistrict-search?search='.$this->input->post('subdistrict_name');

	    $ch = curl_init( $url );

//        $json = json_encode( array(
//            'province_id' => $this->input->post('province_id'),
//        ));
//        curl_setopt( $ch, CURLOPT_PUT, $json );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));
//        curl_setopt($ch, CURLOPT_CAINFO, base_url().'cacert-2020-12-08.pem');

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}

    public function list_agent()
	{
	    $url = 'https://api.mykindofbeauty.co.id/public/v1/reseller-active';
	    // $url = 'http://localhost/mkb-api/public/v1/reseller-active';

	    $ch = curl_init( $url );

        $json = json_encode( array(
            'province_id' => $this->input->post('province_id'),
            'city_id' => $this->input->post('city_id'),
            'subdistrict_id' => $this->input->post('subdistrict_id'),
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);
        $data = json_decode(json_encode($decode_json['data']), FALSE);

        echo json_encode(array(
           'status' => 'success',
           'data' => $data
        ));
	}
}
