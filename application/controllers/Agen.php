<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agen extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

	public function index($province_name = null, $city_name = null, $subdistrict_name = null)
	{
	    $data = $this->main->data_front();
	    $province_name ? $data['province_name'] = $province_name : null;
	    $city_name ? $data['city_name'] = $city_name : null;
	    $subdistrict_name ? $data['subdistrict_name'] = $subdistrict_name : null;
	    $data['page'] = $this->db->where(array('type' => 'agent', 'id_language' => $data['id_language']))->get('pages')->row();
	    $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

	    $this->template->front('agen', $data);
    }
    
    public function detail($province_name, $city_name, $subdistrict_name, $username)
	{
	    $data = $this->main->data_front();

	    $data['page'] = $this->db->where(array('type' => 'agent', 'id_language' => $data['id_language']))->get('pages')->row();
	    $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

	    $url = 'https://api.mykindofbeauty.co.id/public/v1/reseller-active';
	    // $url = 'http://localhost/mkb-api/public/v1/reseller-active';

	    $ch = curl_init( $url );

        $json = json_encode( array( 'username'=> $username ) );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 8817cf1081a1ca964a243a9eea9d8f0ef9956a3f0969e98d36e171a461e2911bb3c340f083cd64c57c321be56d310e7a7d54',
        ));

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);

        $data_province_name = $this->main->slug($decode_json['data'][0]['province']);
        $data_city_name = $this->main->slug($decode_json['data'][0]['city_name']);
        $data_subdistrict_name = $this->main->slug($decode_json['data'][0]['subdistrict_name']);

        if ($data_province_name === $province_name && $data_city_name === $city_name && $data_subdistrict_name === $subdistrict_name) {
            $data['agen'] = json_decode(json_encode($decode_json['data'][0]), FALSE);
        } else {
            redirect('404_override');
        }

	    $this->template->front('agen-detail', $data);
	}

	public function detail_dummy()
	{
	    $data = $this->main->data_front();

	    $data['page'] = $this->db->where(array('type' => 'agent', 'id_language' => $data['id_language']))->get('pages')->row();
	    $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

	    $this->template->front('agen-detail-dummy', $data);
	}
}
