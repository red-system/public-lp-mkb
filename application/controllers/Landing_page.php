<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_page extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function join()
    {
        return $this->load->view('front/lp_join');
    }

    public function landing_page()
    {
        $segment = $this->uri->segment(2);
        $url = base_url()."reseller-page";
        if($segment!=""){
            $data['reseller'] = $this->db->select('phone, shopee, tokopedia')->where('referal_code', $segment)->get('reseller')->row();
            $url = "https://api.whatsapp.com/send?phone=". $this->main->trim_first_char_wa($data['reseller']->phone) ."&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB";
        }
        $data['url'] = $url;

        // $this->load->view('front/landing_page',$data);
        $this->load->view('front/landing_page_v2',$data);
    }

    public function landing_page_v2()
    {
        $segment = $this->uri->segment(2);
        $url = base_url()."reseller-page";
        if($segment!=""){
            $data['reseller'] = $this->db->select('phone, shopee, tokopedia')->where('referal_code', $segment)->get('reseller')->row();
            $url = "https://api.whatsapp.com/send?phone=". $this->main->trim_first_char_wa($data['reseller']->phone) ."&text=Hai,%20Saya%20ingin%20membeli%20produk%20MKB";
        }
        $data['url'] = $url;

        $this->load->view('front/landing_page_v2',$data);
    }

    public function reseller_page($province_name = null, $city_name = null, $subdistrict_name = null)
	{
	    // $data = $this->main->data_front();
	    $province_name ? $data['province_name'] = $province_name : null;
	    $city_name ? $data['city_name'] = $city_name : null;
	    $subdistrict_name ? $data['subdistrict_name'] = $subdistrict_name : null;
	    // $data['page'] = $this->db->where(array('type' => 'agent', 'id_language' => $data['id_language']))->get('pages')->row();
	    // $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();
	    // $this->template->front('reseller_page', $data);
        return $this->load->view('front/reseller_page',$data);
    }


    public function super_reseller_page($province_name = null, $city_name = null, $subdistrict_name = null)
	{
	    $province_name ? $data['province_name'] = $province_name : null;
	    $city_name ? $data['city_name'] = $city_name : null;
	    $subdistrict_name ? $data['subdistrict_name'] = $subdistrict_name : null;

        // if ($province_name == null && $city_name == null && $subdistrict_name = null) {
            $data['resellers'] = $this->db
            ->select('reseller.*, province.province, city.city_name, subdistrict.subdistrict_name')
            ->join('mykindofbeauty_master.login as db2', 'reseller.reseller_id = db2.reseller_id')
            ->join('province', 'province.province_id = reseller.outlet_province_id')
            ->join('city', 'city.city_id = reseller.outlet_city_id')
            ->join('subdistrict', 'subdistrict.subdistrict_id = reseller.outlet_subdistrict_id')
            ->where('reseller.status', 'active')
            ->where('reseller.type', 'super agen')
            ->where('reseller.demo !=', '1')
            ->order_by('rand()')
            ->get('reseller')->result();
        // }
        

	    return $this->load->view('front/super_reseller_page',$data);
    }


    // public function reseller_page()
    // {
    //     $this->load->view('front/reseller_page');
    // }

    public function gabung()
    {
        $segment = $this->uri->segment(2);
        // $url = "https://app.mykindofbeauty.co.id/registrasi";
        // if($segment!=""){
        //     $url = "https://app.mykindofbeauty.co.id/registrasi-v1/".$segment;
        // }
        $url = base_url()."super-reseller-page";
        if($segment!=""){
            $url = "https://app.mykindofbeauty.co.id/registrasi-v1/".$segment;
        }
        $data['url'] = $url;
        $data['count_res'] = $this->db
            ->select('*')
            ->where('reseller.status', 'active')
            ->where('reseller.type', 'agent')
            ->where('reseller.demo !=', '1')
            ->get('reseller')->num_rows();
        $data['count_superres'] = $this->db
            ->select('*')
            ->where('reseller.status', 'active')
            ->where('reseller.type', 'super agen')
            ->where('reseller.demo !=', '1')
            ->get('reseller')->num_rows();
        $this->load->view('front/landing_page_1',$data);
    }

    public function gabung_super()
    {
        
        $data['url'] = "https://app.mykindofbeauty.co.id/registrasi/";;
        $this->load->view('front/landing_page_1',$data);
    }

    public function site_map(){
        $this->load->view('front/site_map');
    }
    public function form_bisnis(){
        $data['province'] = $this->db->get('province')->result();
        $this->load->view('front/form_bisnis',$data);
    }
    public function kabupaten(){
        $province_id = $this->input->post('id');
        $this->db->where('province_id',$province_id);
        $data = $this->db->get('city')->result();
        echo json_encode($data);
    }
    public function kecamatan(){
        $city_id = $this->input->post('id');
        $this->db->where('city_id',$city_id);
        $data = $this->db->get('subdistrict')->result();
        echo json_encode($data);
    }
    public function save(){
        $result['success'] = false;
        $result['message'] = "Gagal Menyimpan Data";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[calon_reseller.email]',array(
            'is_unique'     => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('province_id', 'Domisili', 'required|numeric',
            array(
                'numeric'     => '%s harus diisi'
        ));
        $this->form_validation->set_rules('city_id', 'Kabupaten/Kota', 'required|numeric',
            array(
                'numeric'     => '%s harus diisi'
            ));
        $this->form_validation->set_rules('subdistrict_id', 'Kecamatan', 'required|numeric',
            array(
                'numeric'     => '%s harus diisi'
            ));
        if ($this->form_validation->run() === TRUE)
        {
            $data['nama'] = $this->input->post('nama');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $data['province_id'] = $this->input->post('province_id');
            $data['city_id'] = $this->input->post('city_id');
            $data['subdistrict_id'] = $this->input->post('subdistrict_id');
            $data['created_at'] = date("Y-m-d H:i:s");
            $this->db->insert('calon_reseller',$data);
            $result['success'] = true;
            $result['message'] = "Berhasil Menyimpan Data";
            echo json_encode($result);
        }else{
            echo json_encode(
                array(
                    'status' => 'error',
                    'message' => 'Email sudah terdaftar, silahkan coba kembali',
                    'errors' => array(
                        'email' => form_error('email'),
                        'province_id' => form_error('province_id'),
                        'city_id' => form_error('city_id'),
                        'subdistrict_id' => form_error('subdistrict_id'),
                    )
                )
            );
        }
    }

}


