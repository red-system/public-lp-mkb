<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

	public function index()
	{
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();

        $sesi_home = $this->db
            ->where_in('type', array(
                'home_sesi_1',
                'home_sesi_2',
                'home_sesi_3',
                'home_sesi_4',
                'home_sesi_5',
                'home_sesi_6',
                'home_sesi_7',
                'home_sesi_8',
                'home_sesi_9',
                'home_sesi_10',
                'home_sesi_footer'
            ))
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->result();

        foreach ($sesi_home as $row) {
            $data[$row->type] = $row;
        }

        $data['list_event'] = $this->db->select('title, description, slug, thumbnail, thumbnail_alt, created_at')->where(array('id_language' => $data['id_language'], 'use' => 'yes'))->order_by('event.id', 'DESC')->get('event', 3)->result();
        $data['list_blog'] = $this->db
            ->select('blog.title, blog.description, blog.created_at, blog.slug, blog_category.slug as category_slug, blog.thumbnail, blog.thumbnail_alt')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where(array('blog.id_language' => $data['id_language'], 'blog.use' => 'yes'))
            ->order_by('blog.id', 'DESC')
            ->get('blog', 3)
            ->result();
        $data['faq'] = $this->db->where(array('type' => 'faq', 'id_language' => $data['id_language']))->get('pages', 6)->row();

        $data['testimonial'] = $this->db->where('use', 'yes')->get('comment')->result();
	    $this->template->front('home', $data);
    }
}
