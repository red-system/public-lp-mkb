<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
		$this->load->library('pagination');
	}

	public function index()
	{
        $offset = $this->uri->segment(2);
        $keyword = $_GET['search'];

		$data =  $this->main->data_front();
		$data['page'] = $this->db->select('title, title_menu, type')->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
		$data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

        if($keyword) {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->like('title', $keyword)
                ->like('description', $keyword)
                ->get('blog')
                ->num_rows();
        } else {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->get('blog')
                ->num_rows();
        }

		$this->load->library('pagination');

		$config['base_url'] = base_url().'blog-and-news';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 9;

		$config['use_page_numbers'] = TRUE;
		$config['uri_segment'] = $offset;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '<i class="fas fa-chevron-right"></i>';
        $config['prev_link'] = '<i class="fas fa-chevron-left"></i>';
        $config['full_tag_open'] = '<ul class="list-unstyled pagination d-flex justify-content-center align-items-end">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();

		$page = $this->uri->segment(2) == '' ? 1 : $this->uri->segment(2);
		$start = ($page - 1) * $config['per_page'];

		if ($keyword) {
            $data['list'] = $this->db
                ->select('blog.title, blog.description, blog.created_at, blog.slug, blog.thumbnail, blog.thumbnail_alt, blog_category.slug as category_blog')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category')
                ->where(array(
                    'blog.id_language' => $data['id_language'],
                    'blog.use' => 'yes'
                ))
                ->like('blog.title', $keyword)
                ->or_like('blog.description', $keyword)
                ->get('blog',$config['per_page'],$start)->result();
        } else {
            $data['list'] = $this->db
                ->select('blog.title, blog.description, blog.created_at, blog.slug, blog.thumbnail, blog.thumbnail_alt, blog_category.slug as category_blog')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category')
                ->where(array(
                    'blog.id_language' => $data['id_language'],
                    'blog.use' => 'yes'
                ))
                ->get('blog', $config['per_page'], $start)->result();
        }

		$this->template->front('blog', $data);
	}

	public function detail($category_slug, $slug){

		$data =  $this->main->data_front();
		$data['page'] = $this->db->select('title, title_menu, type')->where(array('id_language' => $data['id_language'], 'type' => 'blog'))->get('pages')->row();
		$data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

		$data['detail_blog'] = $this->db
            ->select('blog.title, blog.description, blog.created_at, blog.thumbnail, blog.thumbnail, blog_category.slug as category_slug')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where(array(
                'blog.id_language' => $data['id_language'],
                'blog.slug' => $slug,
                'blog.use' => 'yes',
                'blog_category.slug' => $category_slug
            ))
            ->get('blog')
            ->row();

		$data['random_post'] = $this->db
            ->select('blog.title, blog.description, blog.created_at, blog.thumbnail, blog.thumbnail, blog.slug, blog_category.slug as category_slug')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'last')
            ->where(array(
                'blog.id_language' => $data['id_language'],
                'blog.use' => 'yes',
                'blog_category.slug' => $category_slug
            ))
            ->where_not_in('blog.slug', $slug)
            ->get('blog')
            ->result();

		$this->template->front('blog-detail', $data);
	}
}
