<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}
	public function index()
	{
        $offset = $this->uri->segment(2);
        $keyword = $_GET['search'];

		$data =  $this->main->data_front();
		$data['page'] = $this->db->select('title, title_menu, type')->where(array('type' => 'event', 'id_language' => $data['id_language']))->get('pages')->row();
		$data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

        if($keyword) {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->like('title', $keyword)
                ->like('description', $keyword)
                ->get('event')
                ->num_rows();
        } else {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->get('event')
                ->num_rows();
        }

		$this->load->library('pagination');

		$config['base_url'] = base_url().'event-and-news';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 9;

		$config['use_page_numbers'] = TRUE;
		$config['uri_segment'] = $offset;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '<i class="fas fa-chevron-right"></i>';
        $config['prev_link'] = '<i class="fas fa-chevron-left"></i>';
        $config['full_tag_open'] = '<ul class="list-unstyled pagination d-flex justify-content-center align-items-end">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();

		$page = $this->uri->segment(2) == '' ? 1 : $this->uri->segment(2);
		$start = ($page - 1) * $config['per_page'];

		if ($keyword) {
            $data['list'] = $this->db
                ->select('event.title, event.description, event.created_at, event.slug, event.thumbnail_alt, event.thumbnail')
                ->where(array(
                    'event.id_language' => $data['id_language'],
                    'event.use' => 'yes'
                ))
                ->like('event.title', $keyword)
                ->or_like('event.description', $keyword)
                ->get('event',$config['per_page'],$start)->result();
        } else {
            $data['list'] = $this->db
                ->select('event.title, event.description, event.created_at, event.slug, event.thumbnail_alt, event.thumbnail')
                ->where(array(
                    'event.id_language' => $data['id_language'],
                    'event.use' => 'yes'
                ))
                ->get('event', $config['per_page'], $start)->result();
        }

		$this->template->front('event', $data);
	}

	public function detail($slug){

		$data =  $this->main->data_front();
		$data['page'] = $this->db->select('title, title_menu, type')->where(array('id_language' => $data['id_language'], 'type' => 'event'))->get('pages')->row();
		$data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

		$data['detail_event'] = $this->db
            ->select('event.title, event.description, event.created_at, event.thumbnail, event.thumbnail_alt')
            ->where(array(
                'event.id_language' => $data['id_language'],
                'event.slug' => $slug,
                'event.use' => 'yes',
            ))
            ->get('event')
            ->row();

		$data['random_post'] = $this->db
            ->select('event.title, event.description, event.created_at, event.thumbnail, event.thumbnail_alt')
            ->where(array(
                'event.id_language' => $data['id_language'],
                'event.use' => 'yes',
            ))
            ->where_not_in('event.slug', $slug)
            ->get('event')
            ->result();

		$this->template->front('event-detail', $data);
	}
}
