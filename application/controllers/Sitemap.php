<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

	public function index()
	{
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'sitemap'))->get('pages')->row();
        $data['home_sesi_footer'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'home_sesi_footer'))->get('pages')->row();

        $data['blog'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'blog'))->get('pages')->row();
        $data['event'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'event'))->get('pages')->row();
        $data['faq'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'faq'))->get('pages')->row();
        $data['about_us'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'about_us'))->get('pages')->row();
        $data['contact_us'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'contact_us'))->get('pages')->row();
        $data['agent'] = $this->db->where(array('id_language' => $data['id_language'], 'type' => 'agent'))->get('pages')->row();

        $url = 'http://api-2.support88.id/v1/sitemap';

	    $ch = curl_init( $url );
        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: bearer 79aa765995b72f88349a017aac022c13fd76130a8d124dba32bc890164e3f9deebdb4b5e991ee08e2d2b226e48ea2770250d',
        ));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        $decode_json = json_decode($result, TRUE);

        $data['sitemap'] = json_decode(json_encode($decode_json['data']), FALSE);

//        print_r($data['sitemap']);

	    $this->template->front('sitemap', $data);
    }
}
